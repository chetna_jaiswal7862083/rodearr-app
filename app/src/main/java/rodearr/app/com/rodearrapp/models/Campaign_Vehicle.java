package rodearr.app.com.rodearrapp.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Campaign_Vehicle implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("owner")
    @Expose
    private Owner owner;
    @SerializedName("vehicle")
    @Expose
    private Vehicle_ vehicle;

    public Campaign_Vehicle(Integer id, Owner owner, Vehicle_ vehicle) {
        this.id = id;
        this.owner = owner;
        this.vehicle = vehicle;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Vehicle_ getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle_ vehicle) {
        this.vehicle = vehicle;
    }
}
