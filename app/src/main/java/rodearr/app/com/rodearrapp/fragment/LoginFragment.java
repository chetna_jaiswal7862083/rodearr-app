package rodearr.app.com.rodearrapp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.activity.ForgotPassword;
import rodearr.app.com.rodearrapp.activity.HomeActivity;
import rodearr.app.com.rodearrapp.activity.Login;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;


public class LoginFragment extends Fragment {


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @BindViews({R.id.uname, R.id.password})
    List<EditText> editText;

    @BindView(R.id.login_btn)
    Button login_btn;

    @BindView(R.id.forgot_pass_text)
    TextView forgot_pass_text;

    String uname, password;
    SharedPref _sharedPref;
    private View view;
    public LoginFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login, container, false);
        _sharedPref = new SharedPref(getActivity());
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.get(0).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Invalid Username",
                            Toast.LENGTH_SHORT).show();
                } else if (editText.get(1).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Invalid Password",
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (Global_Data.checkInternetConnection(getActivity())) {
                        if (Build.VERSION.SDK_INT > 11) {
                            new LoginTask().executeOnExecutor(Global_Data.sExecutor);
                        } else {
                            new LoginTask().execute();
                        }
                    }
                    else {

                        Toast.makeText(getActivity(),"Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        forgot_pass_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ForgotPassword.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return view;
    }

    private class LoginTask extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        JSONObject _loginJson;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = generateLoginjson().toString();
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _loginJson = jsonParser.makeHttpRequest(Global_Data._url + "login/", "POST", json2);
                System.out.println(_loginJson);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_loginJson.has("username") && _loginJson.has("token")) {
                    _sharedPref.setStringData(Global_Data.username, _loginJson.getString("username"));
                    _sharedPref.setStringData(Global_Data.token, _loginJson.getString("token"));
                    _sharedPref.setStringData(Global_Data.user_role,_loginJson.getString("role"));
                    _sharedPref.setStringData(Global_Data.id,_loginJson.getString("id"));
                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }else {
                    if(_loginJson.getJSONArray("non_field_errors").get(0).equals("Incorrect credentials. Please try again.")){

                        System.out.println("checl incorrection---   "+_loginJson.getJSONArray("non_field_errors").get(0));
                        Toast.makeText(getActivity(), ""+_loginJson.getJSONArray("non_field_errors").get(0),
                                Toast.LENGTH_SHORT).show();
                    }


                }


                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Error is-  "+e,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }


    public JSONObject generateLoginjson() {
        JSONObject mainJson = new JSONObject();
        try {
            mainJson.put("username", editText.get(0).getText().toString());
            mainJson.put("password", editText.get(1).getText().toString());

            System.out.println("string LoginModel  " + mainJson.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJson;
    }



}
