package rodearr.app.com.rodearrapp.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.adapter.LanguageAdapter;
import rodearr.app.com.rodearrapp.models.BrandListResponse;


public class LanguageFragment extends Fragment {
   private  RecyclerView rv;
private View view;
private LanguageAdapter languageAdapter;
ArrayList<String> namleList;
    public LanguageFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_language, container, false);
namleList= new ArrayList<>();
namleList.add("English");
        namleList.add("हिंदी");
        namleList.add("मराठी");
        namleList.add("తెలుగు");
        namleList.add("ಕನ್ನಡ");
        namleList.add("தமிழ்");
        namleList.add("ગુજરાતી");
        rv = (RecyclerView) view.findViewById(R.id.rv);
languageAdapter= new LanguageAdapter(namleList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(mLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setNestedScrollingEnabled(false);
        rv.setAdapter(languageAdapter);
        // Inflate the layout for this fragment
        return view;
    }



}
