package rodearr.app.com.rodearrapp.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.classes.WorkaroundMapFragment;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.models.IncentivisedLocations;

public class Incentivised_Location extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap googleMap;
    private ScrollView mScrollView;
    @BindViews({R.id.top_title})
    List<TextView> valueTVList;

    @BindViews({R.id.go_back, R.id.close})
    List<ImageView> imageViews;

    ArrayList<IncentivisedLocations> incentivised_locationArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incentivised__location);
        ButterKnife.bind(this);
        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).getMapAsync(this);
        mScrollView = (ScrollView) findViewById(R.id.sv_container);

        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                mScrollView.requestDisallowInterceptTouchEvent(true);
            }
        });
        valueTVList.get(0).setText(getIntent().getStringExtra("campaign"));
        imageViews.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imageViews.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        incentivised_locationArrayList= new ArrayList<IncentivisedLocations>();
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Indiranagar",12.973210,77.640143));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Koramangala",12.928842,77.624811));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","MG Road",12.975814,77.607079));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Brigade Road",12.969501,77.606419));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Commercial Street",12.982493,77.608359));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Whitefield",12.970788,77.747945));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Electronic City",12.845113,77.673169));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Hebbal",13.035960,77.597530));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Marathahalli",12.959469,77.695634));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Yeshwanthpur",13.029340,77.542188));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Seshadripuram",12.990625,77.578157));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Vittal Mallya Road",12.972216,77.595672));

        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Vidhana Soudha",12.979795,77.591013));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Airport",13.198927,77.706743));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Jayanagar",12.925530,77.591704));
        incentivised_locationArrayList.add(new IncentivisedLocations("Bangalore","Majestic",12.977837,77.571877));

    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                // Disallow ScrollView to intercept touch events.
                mScrollView.requestDisallowInterceptTouchEvent(true);
                break;

            case MotionEvent.ACTION_UP:
                // Allow ScrollView to intercept touch events.
                mScrollView.requestDisallowInterceptTouchEvent(false);
                break;
        }

        // Handle MapView's touch events.
        super.onTouchEvent(ev);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setUpMap();
    }

    public void setUpMap() {
        try {
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            if (ActivityCompat.checkSelfPermission(Incentivised_Location.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Incentivised_Location.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            // googleMap.setMyLocationEnabled(true);
            googleMap.setTrafficEnabled(true);

            googleMap.setIndoorEnabled(true);
            googleMap.setBuildingsEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            googleMap.clear();

            for (int i = 0; i < incentivised_locationArrayList.size(); i++) {


                        createMarker(incentivised_locationArrayList.get(i).getLatitude(), incentivised_locationArrayList.get(i).getLongitude(),incentivised_locationArrayList.get(i).getCity(),incentivised_locationArrayList.get(i).getLocation(), R.drawable.point_copy);
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(12.9716, 77.5946)).zoom(11).build();
                        googleMap.animateCamera(CameraUpdateFactory
                                .newCameraPosition(cameraPosition));






            }




        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected Marker createMarker(double latitude, double longitude, String title, String snippet, int iconResID) {

        return googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
               /* .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));*/
        // .icon(BitmapDescriptorFactory.fromResource(iconResID)));
    }
}
