package rodearr.app.com.rodearrapp.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.activity.AddNewVehicle;
import rodearr.app.com.rodearrapp.adapter.VehicleWithDriverListAdapter;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.models.Brand;
import rodearr.app.com.rodearrapp.models.BrandListResponse;
import rodearr.app.com.rodearrapp.models.Category;
import rodearr.app.com.rodearrapp.models.CategoryListResponse;
import rodearr.app.com.rodearrapp.models.Driver;
import rodearr.app.com.rodearrapp.models.Model;
import rodearr.app.com.rodearrapp.models.Stats;
import rodearr.app.com.rodearrapp.models.Vehicle;
import rodearr.app.com.rodearrapp.models.VehicleALL;
import rodearr.app.com.rodearrapp.models.VehicleListResponse;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;


public class ListOfVehiclesOnDashboard extends Fragment {
    @BindView(R.id.no_record_layout)
    LinearLayout no_record_layout;
    private RecyclerView rv;
    private View view;
    SharedPref _sharedPref;
    ArrayList<VehicleALL> vehicleListResponseList;
    List<Vehicle> vehicleList;
    String  vehicle_id = "0", driver_id = "";

    private VehicleWithDriverListAdapter vehicleListAdapter;

    private ImageView add_vehicle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_list_of_vehicles_dashboard, container, false);
        ButterKnife.bind(this, view);
        _sharedPref = new SharedPref(getActivity());
        add_vehicle = (ImageView) view.findViewById(R.id.add_vehicle);

        rv = (RecyclerView) view.findViewById(R.id.rv);
        vehicleListResponseList = new ArrayList<VehicleALL>();
        vehicleList = new ArrayList<Vehicle>();
        // vehicleListAdapter = new VehicleWithDriverListAdapter(vehicleListResponseList, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(mLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setNestedScrollingEnabled(false);
        //rv.setAdapter(vehicleListAdapter);
        no_record_layout.setVisibility(View.GONE);
        if (Global_Data.checkInternetConnection(getActivity())) {
            if (Build.VERSION.SDK_INT > 11) {

                new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
            } else {

                new GetVehicleListOfOwner().execute();
            }

        } else {

            Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }

        add_vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AddNewVehicle.class);
                getActivity().startActivity(i);
            }
        });
        return view;
    }

    //............................................................................................
    boolean isStatsAvailable;

    private class GetVehicleListOfOwner extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfOwnerJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AndroidNetworking.get(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id))
                        /*    .addBodyParameter("firstname", "Amit")
                            .addBodyParameter("lastname", "Shekhar")
                            .setTag("test")*/
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // do anything with response
                                _vehicleListOfOwnerJSON = response;


                                try {
                                    vehicleListResponseList.clear();
                                    if (_vehicleListOfOwnerJSON != null) {
                                        if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                                            System.out.println("Inside not null");
                                            jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                                            System.out.println("Check vehicle json array---   " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").length());


                                            for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {


                                                try {
                                                    System.out.println("value of i---->..   " + i);


                                                    if (_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").has("stats")) {

                                                        JSONObject dataObject = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").optJSONObject("stats");

                                                        if (dataObject != null) {

                                                            //Do things with object.
                                                            isStatsAvailable = true;
                                                        } else {

                                                            isStatsAvailable = false;
                                                            //Do things with array
                                                        }
                                                    }
                                                    if (isStatsAvailable) {
                                                     //   vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getJSONObject("location").getString("longitude"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getJSONObject("location").getString("lattitude")))));
                                                        JSONObject locationObject = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").optJSONObject("location");
                                                        if (locationObject != null) {
                                                            vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getJSONObject("location").getString("longitude"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getJSONObject("location").getString("lattitude")))));
                                                        } else {
                                                            vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), "", ""))));

                                                        }
                                                    } else {
                                                        vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                                    }
                                                } catch (Exception e) {
                                                    System.out.println("value of i---->..   " + i);

                                                    vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(111, "", "", "", "", "", "", "", new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                                    e.printStackTrace();
                                                }

                                            }
                                            vehicle_id = String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(0).getInt("id"));
                                            driver_id = vehicle_id;
                                            //   vehicleListResponseArrayAdapter.notifyDataSetChanged();
                                            rv.setVisibility(View.VISIBLE);

                                            vehicleListAdapter = new VehicleWithDriverListAdapter(vehicleListResponseList, getActivity());
                                            rv.setAdapter(vehicleListAdapter);
                                            //  vehicleListAdapter.notifyDataSetChanged();
                                        } else {

                                        }
                                    } else {

                                        Toast.makeText(getActivity(), _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                                    }
                                    progressDialog.dismiss();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    no_record_layout.setVisibility(View.VISIBLE);
                                    rv.setVisibility(View.GONE);

                                    // Toast.makeText(getActivity(), "Vehicles are not associated for this owner.", Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();

                                }

                            }

                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });


              /*  String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                   _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);

             */   //Uncomment it when code is running properly.       _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + 2, "GET", json2);

                System.out.println("Owner's vehicle list in listof vehicles------   " + _vehicleListOfOwnerJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

      /*      try {
                vehicleListResponseList.clear();
                if (_vehicleListOfOwnerJSON != null) {
                    // for(int i=0;i<_vehicleListOfOwnerJSON.getJSONArray("vehicles").length();i++) {
                    if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                        System.out.println("Inside not null");
                        jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                        // vehicleList= (List<Vehicle>) jsonArray;
                        // }
                        //   if (jsonArray.length() != 0 || jsonArray != null) {
                        System.out.println("Check vehicle json array---   " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").length());


                        for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {

                          *//*  System.out.println("---->>>    " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"));
                            System.out.println("---->>>    " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"));
                         *//*


                            try {
                                System.out.println("value of i---->..   "+i);
                               *//* if(i!=1){

                                    vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("longitude"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("lattitude")))));

                                }*//*
             *//*  if (_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").has("firstname")) {
                                    vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("longitude"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("lattitude")))));
                                } else {

                                    vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(111, "", "", "", "", "", "", "", new Stats(111, 111, 111, 111, 111, 111, "", ""))));

                                }*//*
                            //UC    vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("longitude"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("lattitude")))));


                                if (_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").has("stats")) {

                                    JSONObject dataObject = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").optJSONObject("stats");

                                    if (dataObject != null) {

                                        //Do things with object.
                                        isStatsAvailable = true;
                                    } else {

                                        isStatsAvailable = false;
                                        //Do things with array
                                    }
                                }
                                if (isStatsAvailable){
                                    vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("longitude"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("lattitude")))));

                                }else {
                                    vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"),new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                }
                            } catch (Exception e) {
                                System.out.println("value of i---->..   "+i);

                                vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(111,"","", "", "","","","", new Stats(0,0, 0, 0, 0, 0, "", ""))));

                                e.printStackTrace();
                            }

                        }
                        vehicle_id = String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(0).getInt("id"));
                        driver_id = vehicle_id;
                        //   vehicleListResponseArrayAdapter.notifyDataSetChanged();
                        rv.setVisibility(View.VISIBLE);

                               vehicleListAdapter = new VehicleWithDriverListAdapter(vehicleListResponseList, getActivity());
                        rv.setAdapter(vehicleListAdapter);
                      //  vehicleListAdapter.notifyDataSetChanged();
                    } else {
                        System.out.println("Inside null");
                    }
                } else {

                    Toast.makeText(getActivity(), _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                no_record_layout.setVisibility(View.VISIBLE);
                rv.setVisibility(View.GONE);

               // Toast.makeText(getActivity(), "Vehicles are not associated for this owner.", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
*/

        }
    }

}
