package rodearr.app.com.rodearrapp.global_supporting_items;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.view.LayoutInflater;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by hp on 18-05-2016.
 */
public class Global_Data {
    public static final int CORE_POOL_SIZE = 5;
    public static final int MAXIMUM_POOL_SIZE = 128;

    public static final BlockingQueue<Runnable> sWorkQueue = new LinkedBlockingQueue<Runnable>(10);

    public static final ThreadPoolExecutor sExecutor =
            new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, 200000, TimeUnit.SECONDS, sWorkQueue);
    public Context _mContext;
    public static LayoutInflater inflater = null;
    public ArrayList<String> _setFeedsByMountain = new ArrayList<>();
    public ArrayList<String> _setFeedsByRegion = new ArrayList<>();
    public Set<String> _alreadyFollowedmountainHS = new HashSet<>();
    public ArrayList<String> _alreadyFollowedmountainAL = new ArrayList<>();

   // public static String _url = "https://rdrprojectapi.herokuapp.com/api/v1/";
//..
  /*  public static String _url = "https://rodearrapi.herokuapp.com/api/v1/";
*/
   public static String _url = "http://142.93.212.45/api/v1/";
    public static String _url1 = "http://142.93.212.45/api/";

   /* public static String _url1 = "http://rdrprojectapi.herokuapp.com/api/";
*/
    public static String username = "USERNAME";
    public static String token = "TOKEN";
    public static String otp = "OTP";
    public static String mobile_number_otp = "MOBILE NUMBER OTP";
    public static String paytm_number_otp = "PAYTM NUMBER OTP";
    public static String user_role = "USER ROLE";
    public static String driver_id = "DRIVER ID";
    public static String owner_driver_id = "OWNER DRIVER ID";
    public static String first_time="FIRST TIME";
    public static String owner_id="OWNER ID";
    public static String ass_owner_id="ASS OWNER ID";
    public static String ass_vehicle_id="ASS VEHICLE ID";
    public static String ass_campaign_id="ASS CAMPAIGN ID";
    public static String id="ID";
    public static String vid="VID";
    public static String pos="POS";
    public static String vehicleId="VEHICLE ID";
    public static String vdToken="TOKEN";
    public static boolean checkInternetConnection(Context context) {
        boolean isInternet=false;
        try {
            ConnectivityManager ConnectionManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected() == true) {
                isInternet = true;

            } else {
                isInternet = false;
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }        return isInternet;
    }

    public static void enableWIFI(Context context){
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(true);
    }

}
