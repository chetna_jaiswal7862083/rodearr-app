package rodearr.app.com.rodearrapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import rodearr.app.com.rodearrapp.models.AreaListResponse;

/**
 * Created by wel come on 10-06-2018.
 */

public class AreaSpinnerAdapter extends ArrayAdapter<AreaListResponse> {
ArrayList<AreaListResponse> a;
        // Your sent context
        private Context context;
        // Your custom values for the spinner (User)
        private AreaListResponse[] values;

       /* public AreaSpinnerAdapter(Context context, int textViewResourceId,
                           AreaListResponse[] values,ArrayList a) {
            super(context, textViewResourceId, values);
            this.context = context;
            this.values = values;
        }*/
       public AreaSpinnerAdapter(Context context, int textViewResourceId,ArrayList<AreaListResponse> a) {
           super(context, textViewResourceId, a);
           this.context = context;
           this.a=a;
       }

        @Override
        public int getCount(){
            return a.size();
        }

       /* @Override
        public AreaListResponse getItem(int position){
            return a.get(position);
        }
*/
    /*    @Override
        public long getItemId(int position){
            return position;
        }
*/

        // And the "magic" goes here
        // This is for the "passive" state of the spinner
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
            TextView label = (TextView) super.getView(position, convertView, parent);

            // Then you can get the current item using the values array (Users array) and the current position
            // You can NOW reference each method you has created in your bean object (User class)
            label.setText(a.get(position).getName());

            // And finally return your dynamic (or custom) view for each spinner item
            return label;
        }

       /* // And here is when the "chooser" is popped up
        // Normally is the same view, but you can customize it if you want
        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            TextView label = (TextView) super.getDropDownView(position, convertView, parent);
            label.setTextColor(Color.BLACK);
            label.setText(values[position].getName());

            return label;
        }*/
    }

