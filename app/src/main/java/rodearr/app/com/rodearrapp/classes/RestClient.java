package rodearr.app.com.rodearrapp.classes;

import rodearr.app.com.rodearrapp.interfaces.APIInterface;

/**
 * Created by wel come on 08-06-2018.
 */

public class RestClient {
    //You need to change the IP if you testing environment is not local machine
    //or you may have different from what we have here
    private static final String URL = "https://rdrprojectapi.herokuapp.com/api/v1/";
    private retrofit.RestAdapter restAdapter;
    private APIInterface apiInterface;

    public RestClient()
    {

        restAdapter = new retrofit.RestAdapter.Builder()
                .setEndpoint(URL)

                .setLogLevel(retrofit.RestAdapter.LogLevel.FULL)
                .build();

        apiInterface = restAdapter.create(APIInterface.class);
    }

    public APIInterface getService()
    {
        return apiInterface;
    }
}
