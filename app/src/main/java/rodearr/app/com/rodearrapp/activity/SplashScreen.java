package rodearr.app.com.rodearrapp.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.cloudrail.si.CloudRail;
import com.cloudrail.si.interfaces.SMS;
import com.cloudrail.si.services.Nexmo;
import com.cloudrail.si.services.Twilio;

import org.json.JSONObject;

import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;

public class SplashScreen extends AppCompatActivity {
    SharedPref _sharedPref;

    Twilio twilio;
    SMS service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);
        _sharedPref = new SharedPref(SplashScreen.this);
        _sharedPref.setStringData(Global_Data.mobile_number_otp, "");
        _sharedPref.setStringData(Global_Data.paytm_number_otp, "");
        _sharedPref.setStringData(Global_Data.otp, "");
        final Handler handler = new Handler();
        /*CloudRail.setAppKey("5b6ad8d170a77f7d86b19f80");
        //service = new Twilio(this, "AC44405c9244688604fcd9d8373e9b3bbf", "1dd8a16c7f4eb4601372f3f673c02fa9");
        service = new Nexmo(SplashScreen.this, "9b158af7", "PxiysJNzhPBhx2Bv");
*/


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms

                if (_sharedPref.getStringData(Global_Data.id).equals("")) {
                    Intent intent = new Intent(SplashScreen.this, IntroScreen.class);

                    startActivity(intent);
                    finish();
                } else {

                    Intent intent = new Intent(SplashScreen.this, HomeActivity.class);

                    startActivity(intent);
                    finish();
                }
                // handler.postDelayed(this, 7000);
            }
        }, 3000);
    }


    private class SendSMSTask extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _sendOTPJSON;
        String mobileNumberOTP = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            try {
             /*   service.sendSMS(
                        "+919174737139",
                        "+919174737139",
                        "dnjsdnsndj ndsjknfdsj fnjdsknfkjdsnfjdsn jfnsdjfndsnfjsd"
                );*/
             /*   service.sendSMS(
                        "Patrick",
                        "+91 9174737139",
                        "Hey, how are you?"
                );*/

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }
}
