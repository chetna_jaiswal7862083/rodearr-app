package rodearr.app.com.rodearrapp.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.models.BrandListResponse;
import rodearr.app.com.rodearrapp.models.CategoryListResponse;
import rodearr.app.com.rodearrapp.models.GetAllModels;
import rodearr.app.com.rodearrapp.models.GetBrandsModel;
import rodearr.app.com.rodearrapp.models.GetCategoryModel;
import rodearr.app.com.rodearrapp.models.OwnerResponse;
import rodearr.app.com.rodearrapp.models.User_a;
import rodearr.app.com.rodearrapp.models.VehicleListResponse;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;
import rodearr.app.com.rodearrapp.utils.BitmapUtils;
import rodearr.app.com.rodearrapp.utils.ImageUtils;

public class AddNewVehicle extends AppCompatActivity {
    @BindViews({R.id.brand, R.id.model, R.id.category, R.id.vehicle_number, R.id.owner_list, R.id.driver_list})
    List<Spinner> spinnerViewList;
    List<BrandListResponse> brandListResponseList;
    List<BrandListResponse.VehicleModel> vehicleModelList;
    List<BrandListResponse.Category> categoryList;
    List<CategoryListResponse> categoryListResponseList;
    List<CategoryListResponse> modelListResponseList;
    List<GetAllModels> allmodelListResponseList;
    List<OwnerResponse> ownerResponseList;
    List<OwnerResponse> driverResponseList;
    //  List<CategoryListResponse> modelListResponseList;
    @BindView(R.id.carNumberEditText)
    EditText carNumberEditText;
    @BindViews({R.id.change_frontsideview_np_layout, R.id.change_backsideview_np_layout, R.id.change_leftsideview_np_layout, R.id.upload_leftsideview_layout, R.id.upload_vehicle_tax_doc_frontview_layout,
            R.id.upload_vehicle_tax_doc_back_layout, R.id.upload_vehicle_rc_card_front_layout, R.id.upload_vehicle_rc_card_back_layout, R.id.vehicle_list_layout})
    List<LinearLayout> linearLayoutsViewList;

    @BindViews({R.id.change_frontsideview_np, R.id.change_backsideview_np, R.id.change_leftsideview_np, R.id.upload_leftsideview, R.id.upload_rightsideview, R.id.upload_vehicle_tax_doc_back,
            R.id.upload_vehicle_rc_card_front, R.id.upload_vehicle_rc_card_back})
    List<Button> btnViewList;

    @BindViews({R.id.frontsideviewnp_imageview, R.id.backsideviewnp_imageview, R.id.leftsideview_imageview, R.id.rightsideview_imageview, R.id.Vehicle_tax_doc_frontview_imageview, R.id.vehicle_tax_doc_back_imageview, R.id.vehicle_rc_card_front_imageview, R.id.vehicle_rc_card_back_imageview})
    List<ImageView> imageViewList;
    private ArrayAdapter<BrandListResponse> brandListResponseArrayAdapter;
    private ArrayAdapter<CategoryListResponse> categoryListResponseArrayAdapter;
    private ArrayAdapter<CategoryListResponse> modelListResponseArrayAdapter;
    private ArrayAdapter<VehicleListResponse> vehicleListResponseArrayAdapter;
    private ArrayAdapter<OwnerResponse> ownerListResponseArrayAdapter;
    private ArrayAdapter<OwnerResponse> driverListResponseArrayAdapter;
    private View view;

    private ArrayList<String> modelList;
    private ArrayAdapter<String> modelListAdapter;
    private int PICK_IMAGE_REQUEST = 100;
    private int imageType = 9;

    public static final String TAG = "Upload Image";
    private Bitmap bitmap = null, frontsideviewnp_imageview_bitmap = null, backsideviewnp_imageview_bitmap = null, leftsideview_imageview_bitmap = null, rightsideview_imageview_bitmap = null, Vehicle_tax_doc_frontview_imageview_bitmap = null,
            vehicle_tax_doc_back_imageview_bitmap = null, vehicle_rc_card_front_imageview_bitmap = null, vehicle_rc_card_back_imageview_bitmap = null;

    ImageView imgView, frontsideviewnp_imageview, backsideviewnp_imageview, leftsideviewnp_imageview, leftsideview_imageview, rightsideview_imageview, rcbook_imageview, vehicle_taxpaper_imageview, vehicle_travel_permit_imageview;
    SharedPref _sharedPref;
    private Uri filePath;
    String owner_id = "", vehicle_id = "0", driver_id = "";
    @BindView(R.id.back)
    ImageView back;
    private String selectedFilePath;

    @BindView(R.id.save_vehicle_settings)
    Button save_vehicle_settings;
    @BindView(R.id.select_vehicle_list_textview)
    TextView select_vehicle_list_textview;
    Dialog dialog, dialog2;
    TextView yes, no;
    TextView ok, msg;
    List<VehicleListResponse> vehicleListResponseList;

    @BindView(R.id.add_self)
    CheckBox add_self;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_vehicle);
        ButterKnife.bind(this);
        _sharedPref = new SharedPref(AddNewVehicle.this);
        brandListResponseList = new ArrayList<>();
        vehicleModelList = new ArrayList<>();
        categoryList = new ArrayList<>();
        categoryListResponseList = new ArrayList<>();
        modelListResponseList = new ArrayList<>();
        vehicleListResponseList = new ArrayList<>();
        ownerResponseList = new ArrayList<>();
        driverResponseList = new ArrayList<>();
        allmodelListResponseList = new ArrayList<>();
        brandListResponseArrayAdapter = new ArrayAdapter<BrandListResponse>
                (AddNewVehicle.this, android.R.layout.simple_spinner_item,
                        brandListResponseList); //selected item will look like a spinner set from XML
        brandListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(0).setAdapter(brandListResponseArrayAdapter);

        categoryListResponseArrayAdapter = new ArrayAdapter<CategoryListResponse>
                (AddNewVehicle.this, android.R.layout.simple_spinner_item,
                        categoryListResponseList); //selected item will look like a spinner set from XML
        categoryListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(2).setAdapter(categoryListResponseArrayAdapter);
        modelList = new ArrayList<>();

        modelListResponseArrayAdapter = new ArrayAdapter<CategoryListResponse>
                (AddNewVehicle.this, android.R.layout.simple_spinner_item,
                        modelListResponseList); //selected item will look like a spinner set from XML
        modelListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        spinnerViewList.get(1).setAdapter(modelListResponseArrayAdapter);
        vehicleListResponseArrayAdapter = new ArrayAdapter<VehicleListResponse>
                (AddNewVehicle.this, android.R.layout.simple_spinner_item,
                        vehicleListResponseList); //selected item will look like a spinner set from XML
        vehicleListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        spinnerViewList.get(3).setAdapter(vehicleListResponseArrayAdapter);

        ownerListResponseArrayAdapter = new ArrayAdapter<OwnerResponse>
                (AddNewVehicle.this, android.R.layout.simple_spinner_item,
                        ownerResponseList); //selected item will look like a spinner set from XML
        ownerListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        spinnerViewList.get(4).setAdapter(ownerListResponseArrayAdapter);

        driverListResponseArrayAdapter = new ArrayAdapter<OwnerResponse>
                (AddNewVehicle.this, android.R.layout.simple_spinner_item,
                        driverResponseList); //selected item will look like a spinner set from XML
        driverListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        spinnerViewList.get(5).setAdapter(driverListResponseArrayAdapter);


        if (Global_Data.checkInternetConnection(AddNewVehicle.this)) {
            if (Build.VERSION.SDK_INT > 11) {
                new GetBrandList().executeOnExecutor(Global_Data.sExecutor);
                new GetCategory().executeOnExecutor(Global_Data.sExecutor);
                new GetAllVehicleModel().executeOnExecutor(Global_Data.sExecutor);
                //   new GetOwnerList().executeOnExecutor(Global_Data.sExecutor);
                new GetDriverList().executeOnExecutor(Global_Data.sExecutor);
                new LoginTask().executeOnExecutor(Global_Data.sExecutor);
            } else {
                new GetBrandList().execute();
                new GetCategory().execute();
                //  new GetOwnerList().execute();
                new GetDriverList().execute();
                new GetAllVehicleModel().execute();
                new LoginTask().execute();
            }

        } else {

            Toast.makeText(AddNewVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }
        spinnerViewList.get(2).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("Item selecting.....   ");
                if (Global_Data.checkInternetConnection(AddNewVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetVehicleModel().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetVehicleModel().execute();
                    }

                } else {

                    Toast.makeText(AddNewVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerViewList.get(0).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("Item selecting.....   ");
                if (Global_Data.checkInternetConnection(AddNewVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new GetCategory().executeOnExecutor(Global_Data.sExecutor);
                        new GetVehicleModel().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new GetCategory().execute();
                        new GetVehicleModel().execute();
                    }

                } else {

                    Toast.makeText(AddNewVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

     /*   if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {
            select_vehicle_list_textview.setVisibility(View.GONE);
            spinnerViewList.get(3).setVisibility(View.GONE);
            linearLayoutsViewList.get(8).setVisibility(View.GONE);
            save_vehicle_settings.setVisibility(View.GONE);
        } else {
            select_vehicle_list_textview.setVisibility(View.VISIBLE);
            spinnerViewList.get(3).setVisibility(View.VISIBLE);
            linearLayoutsViewList.get(8).setVisibility(View.VISIBLE);
            save_vehicle_settings.setVisibility(View.VISIBLE);
        }*/
        spinnerViewList.get(3).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("Item selecting.....   ");
                vehicle_id = String.valueOf(vehicleListResponseList.get(spinnerViewList.get(3).getSelectedItemPosition()).getId());
                if (Global_Data.checkInternetConnection(AddNewVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new GetVehicleDetail().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new GetVehicleDetail().execute();

                    }

                } else {

                    Toast.makeText(AddNewVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialog = new Dialog(AddNewVehicle.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.successfull);
        yes = (TextView) dialog.findViewById(R.id.ok);
        // no = (TextView) dialog.findViewById(R.id.no);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
                finish();
            }
        });
     /*   no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
            }
        });*/
        dialog.setCancelable(false);
        dialog2 = new Dialog(AddNewVehicle.this);
        // Include dialog.xml file
        dialog2.setContentView(R.layout.no_vehicle);
        msg = (TextView) dialog2.findViewById(R.id.message);
        msg.setText("No Internet connection, Click OK to enable internet.");
        ok = (TextView) dialog2.findViewById(R.id.ok);
        // no = (TextView) dialog.findViewById(R.id.no);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog2.hide();
                    Global_Data.enableWIFI(AddNewVehicle.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog2.setCancelable(false);
        if (!Global_Data.checkInternetConnection(AddNewVehicle.this)) {
            dialog2.show();
        }
        save_vehicle_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if (spinnerViewList.get(5).getSelectedItemPosition() == 0) {
                    Toast.makeText(AddNewVehicle.this, "Please select driver", Toast.LENGTH_SHORT).show();
                } else {*/

               if(carNumberEditText.getText().toString().length()==0) {
                   Toast.makeText(AddNewVehicle.this, "Please enter vehicle number", Toast.LENGTH_SHORT).show();
               }else {
                   if (Global_Data.checkInternetConnection(AddNewVehicle.this)) {
                       if (Build.VERSION.SDK_INT > 11) {

                           new UpdateVehicle().executeOnExecutor(Global_Data.sExecutor);
                       } else {

                           new UpdateVehicle().execute();
                       }

                   } else {
                       dialog2.show();
                       Toast.makeText(AddNewVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                   }
               }
                ///}
            }
        });
    /*    if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {
            if (Global_Data.checkInternetConnection(AddNewVehicle.this)) {
                if (Build.VERSION.SDK_INT > 11) {

                    new GetVehicleListOfDriver().executeOnExecutor(Global_Data.sExecutor);
                } else {

                    new GetVehicleListOfDriver().execute();
                }

            } else {

                Toast.makeText(AddNewVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (Global_Data.checkInternetConnection(AddNewVehicle.this)) {
                if (Build.VERSION.SDK_INT > 11) {

                    new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
                } else {

                    new GetVehicleListOfOwner().execute();
                }

            } else {

                Toast.makeText(AddNewVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
            }

        }*/
        carNumberEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable s1) {
                String s = s1.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    carNumberEditText.setText(s);
                }
                carNumberEditText.setSelection(carNumberEditText.getText().length());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

        });
        if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Owner")) {
            linearLayoutsViewList.get(0).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    imageType = 0;
                /*    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            linearLayoutsViewList.get(1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 1;
                  /*  Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            linearLayoutsViewList.get(2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 2;
                   /* Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            linearLayoutsViewList.get(3).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 3;
                  /*  Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            linearLayoutsViewList.get(4).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 4;
                 /*   Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            linearLayoutsViewList.get(5).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 5;
              /*      Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            linearLayoutsViewList.get(6).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 6;
               /*     Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            linearLayoutsViewList.get(7).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 7;
                  /*  Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });
//....................................................................

            btnViewList.get(0).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 0;
                /*    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 1;
               /*     Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 2;
                  /*  Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(3).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 3;
              /*      Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(4).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 4;
               /*     Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(5).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 5;
              /*      Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(6).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 6;
           /*         Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(7).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 7;
                /*    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            owner_id = _sharedPref.getStringData(Global_Data.id);
        } else {

        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//showDialog("Rodearr", AddNewVehicle.this,"Rodearr");
    }

    private class GetBrandList extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AndroidNetworking.get(Global_Data._url + "brands/")
                       /* .addPathParameter("pageNumber", "0")
                        .addQueryParameter("limit", "3")
                        .addHeaders("token", "1234")*/
                        .setTag("test")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                // do anything with response
                                jsonArray =response;


                                try {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        brandListResponseList.add(new BrandListResponse(jsonArray.getJSONObject(i).getInt("id"),
                                                jsonArray.getJSONObject(i).getString("name").trim(), jsonArray.getJSONObject(i).getString("name"), vehicleModelList));

                                    }

                                    brandListResponseArrayAdapter.notifyDataSetChanged();


                                    progressDialog.dismiss();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });


              /*  String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "brands/", "GET", json2);
                System.out.println("Brand response in vehicle---   " + jsonArray.length());
      */      } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

         /*   try {
                for (int i = 0; i < jsonArray.length(); i++) {
                    brandListResponseList.add(new BrandListResponse(jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name").trim(), jsonArray.getJSONObject(i).getString("name"), vehicleModelList));

                }

                brandListResponseArrayAdapter.notifyDataSetChanged();


                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }*/


        }
    }

    //............................................................................................

    private class GetCategory extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AndroidNetworking.get(Global_Data._url + "categories/")
                       /* .addPathParameter("pageNumber", "0")
                        .addQueryParameter("limit", "3")
                        .addHeaders("token", "1234")*/
                        .setTag("test")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                // do anything with response
                                jsonArray =response;
                                try {
                                    categoryListResponseList.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        // System.out.println("JSONARRAY-----   " + jsonArray.getJSONObject(i).getJSONArray("brands"));
                                        for (int j = 0; j < jsonArray.getJSONObject(i).getJSONArray("brands").length(); j++) {
                       /* System.out.println("BRAND  JSONARRAY-----   " + jsonArray.getJSONObject(i).getJSONArray("brands").getJSONObject(j).get("name"));
                        System.out.println("Selected brand---   " + brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()));
                      */
                                            if (brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONArray("brands").getJSONObject(j).getString("name").trim())) {
                                                categoryListResponseList.add(new CategoryListResponse(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("desc")));

                                            }
                                        }

                                    }

                                    categoryListResponseArrayAdapter.notifyDataSetChanged();
                                    progressDialog.dismiss();
                                    if (Global_Data.checkInternetConnection(AddNewVehicle.this)) {
                                        if (Build.VERSION.SDK_INT > 11) {

                                            new GetVehicleModel().executeOnExecutor(Global_Data.sExecutor);
                                        } else {

                                            new GetVehicleModel().execute();
                                        }

                                    } else {

                                        Toast.makeText(AddNewVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    progressDialog.dismiss();
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });



               /* String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "categories/", "GET", json2);
                System.out.println("Category response in vehicle---   " + jsonArray.length());
         */   } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        /*    try {
                categoryListResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    // System.out.println("JSONARRAY-----   " + jsonArray.getJSONObject(i).getJSONArray("brands"));
                    for (int j = 0; j < jsonArray.getJSONObject(i).getJSONArray("brands").length(); j++) {
                       *//* System.out.println("BRAND  JSONARRAY-----   " + jsonArray.getJSONObject(i).getJSONArray("brands").getJSONObject(j).get("name"));
                        System.out.println("Selected brand---   " + brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()));
                      *//*
                        if (brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONArray("brands").getJSONObject(j).getString("name").trim())) {
                            categoryListResponseList.add(new CategoryListResponse(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("desc")));

                        }
                    }

                }

                categoryListResponseArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();
                if (Global_Data.checkInternetConnection(AddNewVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetVehicleModel().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetVehicleModel().execute();
                    }

                } else {

                    Toast.makeText(AddNewVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }*/


        }
    }

    //............................................................................................

    private class GetOwnerList extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "owners/", "GET", json2);
                System.out.println("Owner List response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                ownerResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    //  ownerResponseList.add(new OwnerResponse(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("firstname"), jsonArray.getJSONObject(i).getString("lastname")));

                }

                ownerListResponseArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();

            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }


        }
    }

    //............................................................................................

    private class GetDriverList extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "drivers/", "GET", json2);
                System.out.println("Driver list response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                driverResponseList.clear();
                driverResponseList.add(new OwnerResponse(122112, "Select Driver", "", "", new User_a("", "")));
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getJSONObject(i).isNull("vehicle")) {

                        driverResponseList.add(new OwnerResponse(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("firstname"), jsonArray.getJSONObject(i).getString("lastname"), jsonArray.getJSONObject(i).getString("contact_number"), new User_a(jsonArray.getJSONObject(i).getJSONObject("user").getString("username"), jsonArray.getJSONObject(i).getJSONObject("user").getString("role"))));
                    }
                }

                driverListResponseArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();

            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }


        }
    }

    //............................................................................................

    private class GetAllVehicleModel extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "models/", "GET", json2);
                System.out.println("Model response in vehicle---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                allmodelListResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    System.out.println("Check values----   " + jsonArray.getJSONObject(i).getJSONObject("brand").getString("name") + "   ///   " + jsonArray.getJSONObject(i).getJSONObject("brand").getString("code"));

                    allmodelListResponseList.add(new GetAllModels(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("name"), new GetBrandsModel(Integer.parseInt(jsonArray.getJSONObject(i).getJSONObject("brand").getString("id")), jsonArray.getJSONObject(i).getJSONObject("brand").getString("name"), jsonArray.getJSONObject(i).getJSONObject("brand").getString("code")), new GetCategoryModel(Integer.parseInt(jsonArray.getJSONObject(i).getJSONObject("category").getString("id")), jsonArray.getJSONObject(i).getJSONObject("category").getString("name"), jsonArray.getJSONObject(i).getJSONObject("category").getString("desc"))));


                }
                System.out.println("Get All Models----   " + allmodelListResponseList.size());
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }


        }
    }


    private class GetVehicleModel extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                modelListResponseList.clear();
                for (int i = 0; i < allmodelListResponseList.size(); i++) {
                    //  System.out.println("JSONARRAY-----   " + jsonArray.getJSONObject(i).getJSONArray("brands"));
                    // for (int j = 0; j < jsonArray.getJSONObject(i).getJSONArray("brands").length(); j++) {
                    //System.out.println("MODEL  JSONARRAY-----   " + jsonArray.getJSONObject(i).getJSONObject("brand").getString("name"));
                    //System.out.println("Selected Model---   " + brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()));
                    if (brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).toString().trim().equals(allmodelListResponseList.get(i).getBrand().getName().trim()) && categoryListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).toString().trim().equals(allmodelListResponseList.get(i).getCategory().getName().trim())) {
                        modelListResponseList.add(new CategoryListResponse(allmodelListResponseList.get(i).getId(), allmodelListResponseList.get(i).getName(), ""));

                        //    }
                    }

                }

                modelListResponseArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }

            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
              /*  modelListResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    //  System.out.println("JSONARRAY-----   " + jsonArray.getJSONObject(i).getJSONArray("brands"));
                    // for (int j = 0; j < jsonArray.getJSONObject(i).getJSONArray("brands").length(); j++) {
                    //System.out.println("MODEL  JSONARRAY-----   " + jsonArray.getJSONObject(i).getJSONObject("brand").getString("name"));
                    //System.out.println("Selected Model---   " + brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()));
                    if (brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONObject("brand").getString("name").trim()) && categoryListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONObject("category").getString("name").trim())) {
                        modelListResponseList.add(new CategoryListResponse(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("name"), ""));

                        //    }
                    }*/

                // }
                System.out.println(",,,,,,,,   ---  " + modelListResponseList.size());
                modelListResponseArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }
            // checkPermission();


        }
    }


    //..............................................................................................

    private class UpdateVehicle extends AsyncTask<String, String, String> {
        String message = "", id = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONArray _fetchUserProfileJsonArray;
        JSONObject _updateUserProfileOutputJSON, _updateUserProfileInput;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {


                id = _sharedPref.getStringData(Global_Data.id);
                _updateUserProfileInput = new JSONObject();

                _updateUserProfileInput.put("brand", brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).getId());
                _updateUserProfileInput.put("category", categoryListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).getId());
                _updateUserProfileInput.put("model", modelListResponseList.get(spinnerViewList.get(1).getSelectedItemPosition()).getId());
                _updateUserProfileInput.put("vehicle_number", carNumberEditText.getText().toString());
                _updateUserProfileInput.put("owner", _sharedPref.getStringData(Global_Data.id));
                _updateUserProfileInput.put("vdtoken", "");

          /*  if(spinnerViewList.get(5).getSelectedItemPosition()>0) {
                _updateUserProfileInput.put("driver", driverResponseList.get(spinnerViewList.get(5).getSelectedItemPosition()).getId());
            }else {*/
                if (add_self.isChecked()) {
                    //  _updateUserProfileInput.put("driver", _sharedPref.getStringData(Global_Data.owner_driver_id));
                    _updateUserProfileInput.put("addSelf", true);
                } else {
                    //  _updateUserProfileInput.put("driver", JSONObject.NULL);
                    _updateUserProfileInput.put("addSelf", false);
                }

                //   }
                // _updateUserProfileInput.put("driver", JSONObject.NULL);

                if (frontsideviewnp_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_front_image", encodeToBase64(frontsideviewnp_imageview_bitmap));
                }
                if (backsideviewnp_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_back_image", encodeToBase64(backsideviewnp_imageview_bitmap));
                }
                if (leftsideview_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_left_image", encodeToBase64(leftsideview_imageview_bitmap));
                }
                if (rightsideview_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_right_image", encodeToBase64(rightsideview_imageview_bitmap));
                }
                if (Vehicle_tax_doc_frontview_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_tax_doc_front_image", encodeToBase64(Vehicle_tax_doc_frontview_imageview_bitmap));
                }
                if (vehicle_tax_doc_back_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_tax_doc_back_image", encodeToBase64(vehicle_tax_doc_back_imageview_bitmap));
                }
                if (vehicle_rc_card_front_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_rc_card_front_image", encodeToBase64(vehicle_rc_card_front_imageview_bitmap));
                }
                if (vehicle_rc_card_back_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_rc_card_back_image", encodeToBase64(vehicle_rc_card_back_imageview_bitmap));
                }
                String json2 = _updateUserProfileInput.toString();
                System.out.println("Driver id---   " + _sharedPref.getStringData(Global_Data.driver_id) + "   //   " + _sharedPref.getStringData(Global_Data.owner_driver_id));
                //  json2 = _sharedPref.getStringData(Global_Data.driver_id);
                // json2 = _sharedPref.getStringData(Global_Data.id);
                JSONParser jsonParser = new JSONParser();
                System.out.println("_updateUserProfileInput  ...   " + json2);
                _updateUserProfileOutputJSON = jsonParser.makeHttpRequest(Global_Data._url + "vehicles/", "POST", json2);
                System.out.println(_updateUserProfileOutputJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_updateUserProfileOutputJSON.has("driver") && _updateUserProfileOutputJSON.getString("driver").equals("[\"This field must be unique.\"]")) {
                    Toast.makeText(AddNewVehicle.this, "Driver must be unique.", Toast.LENGTH_SHORT).show();

                }
                if (_updateUserProfileOutputJSON.has("vehicle_number") && _updateUserProfileOutputJSON.getString("vehicle_number").equals("[\"Vehicle Details with this Vehicle Number already exists.\"]")) {
                    Toast.makeText(AddNewVehicle.this, "Vehicle Details with this Vehicle Number already exists.", Toast.LENGTH_SHORT).show();
                    carNumberEditText.setError("Vehicle Details with this Vehicle Number already exists.");
                } else {
                    Toast.makeText(AddNewVehicle.this, "Added vehicle successfully", Toast.LENGTH_SHORT).show();
                    dialog.show();
                    if (add_self.isChecked()) {
                        if (Global_Data.checkInternetConnection(AddNewVehicle.this)) {
                            if (Build.VERSION.SDK_INT > 11) {

                                new FetchUserProfile().executeOnExecutor(Global_Data.sExecutor);
                            } else {

                                new FetchUserProfile().execute();
                            }

                        } else {
                            Toast.makeText(AddNewVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                /*if (_updateUserProfileOutputJSON != null) {

                }*/
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }
    //............................................................................................

    private class GetDriver_OwnerInfo extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _ownerInfoJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _ownerInfoJSON = jsonParser.makeHttpRequest(Global_Data._url + "drivers/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);
                System.out.println("Owner details------   " + _ownerInfoJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                owner_id = _ownerInfoJSON.getJSONObject("owner").getString("id").toString();
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    public static String encodeToBase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            // selectedFilePath = getPath( filePath);
            //  selectedFilePath= PathUtils.getPath(AddNewVehicle.this, filePath);
            String selectedImagePath = BitmapUtils.getFilePathFromUri(this, data.getData());
            //   selectedFilePath=getRealPathFromURI( filePath,AddNewVehicle.this);
            Log.i(TAG, " File path : " + filePath);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(AddNewVehicle.this.getContentResolver(), filePath);
                // ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // bitmap.compress(Bitmap.CompressFormat.JPEG,80,stream);
                System.out.println("After Compress Dimension     " + bitmap.getWidth() + "-" + bitmap.getHeight() + "    file - " + filePath + "    select--   " + selectedImagePath);

                //  byte[] byteArray = stream.toByteArray();
                //  Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray,0,byteArray.length);
                Bitmap compressedBitmap = ImageUtils.getInstant().getCompressedBitmap(selectedImagePath);
                //   Log.i("After Compress Dimension", compressedBitmap.getWidth() + "-" + compressedBitmap.getHeight());

                System.out.println("After Compress Dimension     " + compressedBitmap.getWidth() + "-" + compressedBitmap.getHeight() + "    file - " + filePath + "    select--   " + selectedImagePath);
                persistImage(compressedBitmap, "imagefile");

   /*
   frontsideviewnp_imageview_bitmap = null, backsideviewnp_imageview_bitmap = null, leftsideview_imageview_bitmap = null, rightsideview_imageview_bitmap = null, Vehicle_tax_doc_frontview_imageview_bitmap = null,
            vehicle_tax_doc_back_imageview_bitmap = null, vehicle_rc_card_front_imageview_bitmap = null, vehicle_rc_card_back_imageview_bitmap = null;
*/

                if (imageType == 0) {
                    frontsideviewnp_imageview_bitmap = compressedBitmap;
                    imageViewList.get(0).setImageBitmap(compressedBitmap);
                } else if (imageType == 1) {
                    backsideviewnp_imageview_bitmap = compressedBitmap;
                    imageViewList.get(1).setImageBitmap(compressedBitmap);
                } else if (imageType == 2) {
                    leftsideview_imageview_bitmap = compressedBitmap;
                    imageViewList.get(2).setImageBitmap(compressedBitmap);
                } else if (imageType == 3) {
                    rightsideview_imageview_bitmap = compressedBitmap;
                    imageViewList.get(3).setImageBitmap(compressedBitmap);

                } else if (imageType == 4) {
                    Vehicle_tax_doc_frontview_imageview_bitmap = compressedBitmap;
                    imageViewList.get(4).setImageBitmap(compressedBitmap);
                } else if (imageType == 5) {
                    vehicle_tax_doc_back_imageview_bitmap = compressedBitmap;
                    imageViewList.get(5).setImageBitmap(compressedBitmap);
                } else if (imageType == 6) {
                    vehicle_rc_card_front_imageview_bitmap = compressedBitmap;
                    imageViewList.get(6).setImageBitmap(compressedBitmap);
                } else if (imageType == 7) {
                    vehicle_rc_card_back_imageview_bitmap = compressedBitmap;
                    imageViewList.get(7).setImageBitmap(compressedBitmap);
                }
                // imgView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Run time permission for above marshmallow
     * if user deny the permission when install the application then system ask for permission
     * camera & storage
     */
    //  private int PICK_IMAGE_REQUEST = 2;
    public final static int PERMISSIONS_REQUEST = 1;

    @TargetApi(Build.VERSION_CODES.M)
    public void getPermissionToStorage() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            }

            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA},
                    PERMISSIONS_REQUEST);

        } else {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
           /* Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
           */
            startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
            //  startActivity( new Intent(MoreOptionActivity.this,PostStatusActivitiy.class));

        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[2])) {
            } else if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, permissions[1]) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, permissions[2]) == PackageManager.PERMISSION_GRANTED) {
                //allowed
               /* Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
              */  /*Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);*/

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
            } else {
              /*  Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);*/
                Intent intent1 = new Intent();
                intent1.setType("image/*");
                intent1.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent1, "Select Image"), PICK_IMAGE_REQUEST);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    public static String getRealPathFromURI(Uri uri, Context context) {
        Uri contentUri = uri;
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private String persistImage(Bitmap bitmap, String name) {
        File filesDir = getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");
        String file_String = "";
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            System.out.println("OS---   " + imageFile.getName());
            file_String = imageFile.toString();
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return file_String;
    }


    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    //............................................................................................

    private class GetVehicleListOfOwner extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfOwnerJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);
                System.out.println("Owner's vehicle list------   " + _vehicleListOfOwnerJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (_vehicleListOfOwnerJSON != null) {
                    // for(int i=0;i<_vehicleListOfOwnerJSON.getJSONArray("vehicles").length();i++) {
                    if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                        jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                        // }
                        //   if (jsonArray.length() != 0 || jsonArray != null) {
                        System.out.println("Check vehicle json array---   " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").length());

                        for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {
                            vehicleListResponseList.add(new VehicleListResponse(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number")));
                        }
                        vehicle_id = String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(0).getInt("id"));
                        driver_id = vehicle_id;
                        vehicleListResponseArrayAdapter.notifyDataSetChanged();

                        if (Global_Data.checkInternetConnection(AddNewVehicle.this)) {
                            if (Build.VERSION.SDK_INT > 11) {

                                new GetVehicleDetail().executeOnExecutor(Global_Data.sExecutor);
                            } else {

                                new GetVehicleDetail().execute();
                            }

                        } else {

                            Toast.makeText(AddNewVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                        }
                    /*} else {
                        Toast.makeText(getActivity(), "Vehicles are not associated for this owner.", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }*/
                    }
                } else {
                    Toast.makeText(AddNewVehicle.this, _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();


                Toast.makeText(AddNewVehicle.this, "Vehicles are not associated for this owner.", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }


        }
    }

    //............................................................................................

    private class GetVehicleListOfDriver extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfDriverJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfDriverJSON = jsonParser.makeHttpRequest(Global_Data._url + "drivers/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);
                System.out.println("Driver's vehicle list------   " + _vehicleListOfDriverJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (_vehicleListOfDriverJSON.getString("vehicle") != null) {
                    vehicle_id = String.valueOf(_vehicleListOfDriverJSON.getJSONObject("vehicle").getInt("id"));
                    driver_id = vehicle_id;
              /*  // for(int i=0;i<_vehicleListOfOwnerJSON.getJSONArray("vehicles").length();i++) {
                jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                // }
                System.out.println("Check vehicle json array---   " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").length());

                for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {
                    vehicleListResponseList.add(new VehicleListResponse(Integer.parseInt(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("id")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number")));
                }
                vehicleListResponseArrayAdapter.notifyDataSetChanged();
           */
                    if (Global_Data.checkInternetConnection(AddNewVehicle.this)) {
                        if (Build.VERSION.SDK_INT > 11) {

                            new GetVehicleDetail().executeOnExecutor(Global_Data.sExecutor);
                        } else {

                            new GetVehicleDetail().execute();
                        }
                    }
                } else {

                    Toast.makeText(AddNewVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }


        }
    }

    //............................................................................................

    private class GetVehicleDetail extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleDetailJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleDetailJSON = jsonParser.makeHttpRequest(Global_Data._url + "vehicles/" + vehicle_id, "GET", json2);
                System.out.println("Vehicle details------   " + _vehicleDetailJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                int pos1 = -1, pos2 = -1, pos3 = -1;
                for (int i = 0; i < brandListResponseList.size(); i++) {
                    //  System.out.println("check brand for spinner--   " + brandListResponseList.get(i).gecontains("Maruti Suzuki"));
                    if (brandListResponseList.get(i).getName().equals(_vehicleDetailJSON.getJSONObject("brand").getString("name")))
                        ;
                    pos1 = i;
                }
                spinnerViewList.get(0).setSelection(pos1);
                for (int j = 0; j < categoryListResponseList.size(); j++) {

                    if (categoryListResponseList.get(j).getName().equals(_vehicleDetailJSON.getJSONObject("category").getString("name"))) {
                        pos2 = j;
                    }
                }
                spinnerViewList.get(2).setSelection(pos2);

                for (int k = 0; k < modelListResponseList.size(); k++) {
                    if (modelListResponseList.get(k).getName().equals(_vehicleDetailJSON.getJSONObject("model").getString("name"))) {
                        pos3 = k;
                    }
                }
                spinnerViewList.get(1).setSelection(pos3);
                carNumberEditText.setText(_vehicleDetailJSON.getString("vehicle_number"));
         /*       if (_vehicleDetailJSON.getString("vehicle_front_image") != null) {
                    Glide.with(getActivity()).load(_vehicleDetailJSON.getString("vehicle_front_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(0));
                }

                if (_vehicleDetailJSON.getString("vehicle_back_image") != null) {
                    Glide.with(getActivity()).load(_vehicleDetailJSON.getString("vehicle_back_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(1));
                }
                if (_vehicleDetailJSON.getString("vehicle_left_image") != null) {
                    Glide.with(getActivity()).load(_vehicleDetailJSON.getString("vehicle_left_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(2));
                }
                if (_vehicleDetailJSON.getString("vehicle_right_image") != null) {
                    Glide.with(getActivity()).load(_vehicleDetailJSON.getString("vehicle_right_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(3));
                }
                if (_vehicleDetailJSON.getString("vehicle_tax_doc_front_image") != null) {
                    Glide.with(getActivity()).load(_vehicleDetailJSON.getString("vehicle_tax_doc_front_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(4));
                }
                if (_vehicleDetailJSON.getString("vehicle_tax_doc_back_image") != null) {
                    Glide.with(getActivity()).load(_vehicleDetailJSON.getString("vehicle_tax_doc_back_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(5));
                }
                if (_vehicleDetailJSON.getString("vehicle_rc_card_front_image") != null) {
                    Glide.with(getActivity()).load(_vehicleDetailJSON.getString("vehicle_rc_card_front_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(6));
                }
                if (_vehicleDetailJSON.getString("vehicle_rc_card_back_image") != null) {
                    Glide.with(getActivity()).load(_vehicleDetailJSON.getString("vehicle_rc_card_back_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(7));
                }*/

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    //......................................GET DRIVER ID FROM HIS MOBILE NUMBER............................................
    long vId = 0;

    private class LoginTask extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        JSONObject _json;
        JSONArray _jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "drivers/?contact_number=" + "9685535112", "GET", json2);
                System.out.println("contact number exist in add new vehicle---    " + _jsonArray);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                vId = 0;
                for (int i = 0; i < _jsonArray.length(); i++) {
                    //   if (_json != JSONObject.NULL) {
                    System.out.println("check owner driver id ----   " + _jsonArray.getJSONObject(i).getInt("id"));
                    _sharedPref.setStringData(Global_Data.owner_driver_id, String.valueOf(_jsonArray.getJSONObject(i).getInt("id")));

                    //   }
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
              /*  Toast.makeText(DriverManagement.this, "Error is-  " + e,
                        Toast.LENGTH_SHORT).show();
               */


                progressDialog.dismiss();

            }
        }
    }

    //............................................
    private class FetchUserProfile extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONArray _fetchUserProfileJsonArray;
        JSONObject _fetchUserProfileJSON;
        ProgressDialog progressDialog = new ProgressDialog(AddNewVehicle.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                String json2 = "";
                //System.out.println("Driver id---   " + _sharedPref.getStringData(Global_Data.driver_id));
                //  json2 = _sharedPref.getStringData(Global_Data.driver_id);
                json2 = "";
                JSONParser jsonParser = new JSONParser();
                System.out.println("");
                if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {
                    System.out.println("Driver----    " + _sharedPref.getStringData(Global_Data.id));

                    _fetchUserProfileJSON = jsonParser.makeHttpRequest(Global_Data._url + "drivers/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);

                } else {
                    System.out.println("Owner----    " + _sharedPref.getStringData(Global_Data.id));
                    _fetchUserProfileJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);


                }   //   _fetchUserProfileJsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "drivers/"+_sharedPref.getStringData(Global_Data.id), "GET", json2);
                System.out.println("Profile output...   " + _fetchUserProfileJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_fetchUserProfileJSON != JSONObject.NULL) {

                    if (_fetchUserProfileJSON.has("selfdriverId")) {
                        _sharedPref.setStringData(Global_Data.owner_driver_id, _fetchUserProfileJSON.getString("selfdriverId"));
                    }else {
                        _sharedPref.setStringData(Global_Data.owner_driver_id, "");

                    }

                } else {
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


}
