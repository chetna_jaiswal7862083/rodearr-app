package rodearr.app.com.rodearrapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Vdtoken implements Serializable {

    @SerializedName("vehicle_driver_token")
    @Expose
    private String vehicle_driver_token;

    @SerializedName("created_at")
    @Expose
    private String created_at;

    @SerializedName("expiry_at")
    @Expose
    private String expiry_at;

    public Vdtoken(String vehicle_driver_token, String created_at, String expiry_at) {
        this.vehicle_driver_token = vehicle_driver_token;
        this.created_at = created_at;
        this.expiry_at = expiry_at;
    }

    public String getVehicle_driver_token() {
        return vehicle_driver_token;
    }

    public void setVehicle_driver_token(String vehicle_driver_token) {
        this.vehicle_driver_token = vehicle_driver_token;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getExpiry_at() {
        return expiry_at;
    }

    public void setExpiry_at(String expiry_at) {
        this.expiry_at = expiry_at;
    }
}
