package rodearr.app.com.rodearrapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.activity.Campaign_Details;
import rodearr.app.com.rodearrapp.models.CampaignDetailListModel;
import rodearr.app.com.rodearrapp.models.Compaign_response;

/**
 * Created by wel come on 07-06-2018.
 */

public class CompaignsListAdapter extends RecyclerView.Adapter<CompaignsListAdapter.MyViewHolder> {
    private List<Compaign_response> moviesList;
    private ArrayList<Integer> IMAGES;
    Context mContext;
    private ArrayList<String> url;
    private ArrayList<String> movie_name;
    private ArrayList<String> ratings;
    private ArrayList<String> votes;
    private ArrayList<String> type;
    List<CampaignDetailListModel> campaign_detailsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, duration, earn, start_date, end_date;
        ImageView campaigns_image, join_campaigns;

        public MyViewHolder(View view) {
            super(view);
            earn = (TextView) view.findViewById(R.id.earn_value);
            join_campaigns = (ImageView) view.findViewById(R.id.join_campaigns);
            campaigns_image = (ImageView) view.findViewById(R.id.campaigns_image);
            start_date = (TextView) view.findViewById(R.id.start_date_value);
            end_date = (TextView) view.findViewById(R.id.end_date_value);
            name = (TextView) view.findViewById(R.id.name_value);
          /*  compaign_image = (ImageView) view.findViewById(R.id.compaign_image);
            you_drove_week_kmsvalue = (TextView) view.findViewById(R.id.you_drove_week_kmsvalue);
            you_drove_month_kmsvalue = (TextView) view.findViewById(R.id.you_drove_month_kmsvalue);
*/
        }
    }

    public CompaignsListAdapter(Context mContext, List<CampaignDetailListModel> campaign_detailsList) {
        this.mContext = mContext;
        this.campaign_detailsList = campaign_detailsList;
    }
  /*  public CompaignsListAdapter(List<Compaign_response> moviesList) {
        this.moviesList = moviesList;

    }*/

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.campaigns_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        //holder.duration.setText(moviesList.get(position).getEarning());
        String s1 = campaign_detailsList.get(position).getMinEarning() + "-" + campaign_detailsList.get(position).getMaxEarning();
        String next = "<font color='#389BCB'>" + s1 + "</font>";
        String last = "/Months";
        holder.earn.setText(Html.fromHtml(next + "" + last));
        holder.name.setText(campaign_detailsList.get(position).getCampaign().getName());
        String sDate = campaign_detailsList.get(position).getStartDate();
        String[] separated = sDate.split("-");
        String eDate =campaign_detailsList.get(position).getEndDate();
        String[] separated1 = eDate.split("-");


       /* StringBuffer bufferStart = new StringBuffer();
        bufferStart.reverse();

        StringBuffer bufferEnd = new StringBuffer(campaign_detailsList.get(position).getEndDate());
        bufferEnd.reverse();*/
        holder.start_date.setText( separated[2]+"-"+separated[1]+"-"+separated[0]);
        holder.end_date.setText( separated1[2]+"-"+separated1[1]+"-"+separated1[0]);
        holder.join_campaigns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, Campaign_Details.class);
                System.out.println("---????   " + campaign_detailsList.get(position).getId());
                i.putExtra("CAMPAIGN LIST", (Serializable) campaign_detailsList);
                //Create the bundle
                Bundle bundle = new Bundle();

//Add your data to bundle
                bundle.putString("POS", String.valueOf(position));

//Add the bundle to the intent
                i.putExtras(bundle);


                //  i.putExtra("number", vehicleListResponseList.get(position).getVehicleNumber());
                mContext.startActivity(i);
            }
        });
        System.out.println(" Campaign logo image---   " + campaign_detailsList.get(position).getCampaign_logo_image());
        if (!campaign_detailsList.get(position).getCampaign_logo_image().equals(null)) {
            Glide.with(mContext).load(campaign_detailsList.get(position).getCampaign_logo_image())
                    .thumbnail(0.5f)
                    .crossFade()
                    //.placeholder(R.drawable.rightsideviewwithnp)

                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.campaigns_image);
        }
    }

    @Override
    public int getItemCount() {
        return campaign_detailsList.size();
    }
}
