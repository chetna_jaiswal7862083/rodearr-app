package rodearr.app.com.rodearrapp.fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.activity.HomeActivity;


public class Settings extends Fragment {

    View view,view1;
    private Toolbar toolbar;
    private TabLayout setting_tab;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.dashboard,
            R.drawable.dashboard,
            R.drawable.dashboard
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        view1 = inflater.inflate(R.layout.activity_home, container, false);
        TextView top=(TextView) view1.findViewById(R.id.top_title);
        top.setText("jhhjvhj");
        ButterKnife.bind(this, view);
        viewPager = (ViewPager) view.findViewById(R.id.setting_viewpager);
        setupViewPager(viewPager);
        getActivity().setTitle("your title");
        setting_tab = (TabLayout) view.findViewById(R.id.setting_tab);
        setting_tab.setupWithViewPager(viewPager);
        setting_tab.setSelectedTabIndicatorColor(Color.parseColor("#E1872E"));
       // setupTabIcons();
        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

  /*  @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    private void setupTabIcons() {
        setting_tab.getTabAt(0).setIcon(tabIcons[0]);
      //  setting_tab.getTabAt(1).setIcon(tabIcons[1]);
        setting_tab.getTabAt(2).setIcon(tabIcons[2]);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new ProfileFragment(), "Profile");
     //   adapter.addFragment(new VehicleSetting(), "Vehicle");
        adapter.addFragment(new LanguageFragment(), "Language");
        adapter.addFragment(new BlankFragment(), "FAQ");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return  mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
