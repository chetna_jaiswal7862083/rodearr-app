package rodearr.app.com.rodearrapp.jsonparser;

import android.support.annotation.NonNull;
import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import rodearr.app.com.rodearrapp.classes.HttpPatch;

import static android.support.constraint.Constraints.TAG;

public class JSONParser {

    static InputStream is = null;
    static JSONObject jObj = null;
    public static String json = "", issue = "";
    static JSONArray jaObj = null;

    // constructor
    public JSONParser() {

    }

    // function get json from url
    // by making HTTP POST or GET mehtod
    public JSONObject makeHttpRequest(String url, String method,
                                      String json_to_string) {
        InputStream inputStream = null;
        String result = "";
        jObj = null;
        // Making HTTP request
        try {

            // check for request method
            if (method == "POST") {
                // request method is POST
                // defaultHttpClient
                // 1. create HttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                StringEntity se = new StringEntity(json_to_string, "UTF-8");

                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpPost);

                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null) {
                    result = convertInputStreamToString(inputStream);
                    System.out.println("Result is....  " + result);
                    jObj = new JSONObject(result);
                    issue = result;
                    System.out.println("Issue--   " + issue);
                } else {
                    jObj = null;
                    System.out.println("Result is....  " + result);

                }

            } else if (method == "POST1") {
                // request method is POST
                // defaultHttpClient
                // 1. create HttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("Accept", "application/x-www-form-urlencoded");
                httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
               /* httpPost.setHeader("authkey", "231742ANhtdxc9ytq15b72c31d");

                httpPost.setHeader("message", "scasc  as  sa##123456##");
                httpPost.setHeader("sender", "kjhkjh");
                httpPost.setHeader("mobile", "+9174737139");
                httpPost.setHeader("otp_length", "6");
                httpPost.setHeader("otp","123456");
*/

                StringEntity se = new StringEntity(json_to_string, "UTF-8");

                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpPost);

                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null) {
                    result = convertInputStreamToString(inputStream);
                    System.out.println("Result is....  " + result);
                    jObj = new JSONObject(result);
                    issue = result;
                    System.out.println("Issue--   " + issue);
                } else {
                    jObj = null;
                    System.out.println("Result is....  " + result);

                }

            } else if (method == "GET") {
                System.out.println("Inside GET");

                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet(url);
                HttpResponse response = client.execute(request);
                inputStream = response.getEntity().getContent();
                if (inputStream != null) {
                    result = convertInputStreamToString(inputStream);
                    System.out.println("Result is. get...  " + result);
                    jObj = new JSONObject(result);
                } else {
                    jObj = null;
                    System.out.println("Result is. get...  " + result);

                }

            } else if (method == "PUT") {
                System.out.println("Inside GET");

                //  HttpClient client = new DefaultHttpClient();
                // request method is POST
                // defaultHttpClient
                // 1. create HttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                // 2. make POST request to the given URL
                HttpPut httpPost = new HttpPut(url);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                StringEntity se = new StringEntity(json_to_string, "UTF-8");

                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpPost);

                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null) {
                    result = convertInputStreamToString(inputStream);
                    System.out.println("Result is....  " + result);
                    jObj = new JSONObject(result);
                } else {
                    jObj = null;
                    System.out.println("Result is....  " + result);

                }

            }

            else if (method == "PATCH") {
                System.out.println("Inside GET");

                //  HttpClient client = new DefaultHttpClient();
                // request method is POST
                // defaultHttpClient
                // 1. create HttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                // 2. make POST request to the given URL
                HttpPatch httpPatch = new HttpPatch(url);
                httpPatch.setHeader("Accept", "application/json");
                httpPatch.setHeader("Content-type", "application/json");
                StringEntity se = new StringEntity(json_to_string, "UTF-8");

                httpPatch.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpPatch);

                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null) {
                    result = convertInputStreamToString(inputStream);
                    System.out.println("Result is....  " + result);
                    jObj = new JSONObject(result);
                } else {
                    jObj = null;
                    System.out.println("Result is....  " + result);

                }

            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;

    }

    //......................................................


    public JSONObject makeHttpRequestWithHeader(String url, String method,
                                                String json_to_string, String accessToken) {
        InputStream inputStream = null;
        String result = "";
        // Making HTTP request
        try {

            // check for request method
            if (method == "POST") {
                // request method is POST
                // defaultHttpClient
                // 1. create HttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                httpPost.setHeader("Authorization", "Bearer " + accessToken);
                StringEntity se = new StringEntity(json_to_string, "UTF-8");

                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpPost);

                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null) {
                    result = convertInputStreamToString(inputStream);
                    System.out.println("Result is....  " + result);
                    jObj = new JSONObject(result);
                } else {
                    jObj = null;
                    System.out.println("Result is....  " + result);

                }

            } else if (method == "GET") {
                System.out.println("Inside GET");

                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet("https://rdrprojectapi.herokuapp.com/api/v1/brands/");
                HttpResponse response = client.execute(request);
                inputStream = response.getEntity().getContent();
                if (inputStream != null) {
                    result = convertInputStreamToString(inputStream);
                    System.out.println("Result is. get...  " + result);
                    jObj = new JSONObject(result);
                } else {
                    jObj = null;
                    System.out.println("Result is. get...  " + result);

                }

            }


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;

    }

    // function get json from url
    // by making HTTP POST or GET mehtod
    public JSONArray makeHttpRequestForJSONArray(String url, String method,
                                                 String json_to_string) {
        InputStream inputStream = null;
        String result = "";
        jaObj = null;
        // Making HTTP request
        try {

            if (method == "GET") {
                System.out.println("Inside GET");

                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet(url);
                HttpResponse response = client.execute(request);
                inputStream = response.getEntity().getContent();
                if (inputStream != null) {
                    result = convertInputStreamToString(inputStream);
                    System.out.println("Result is. get...  " + result);
                    jaObj = new JSONArray(result);
                } else {
                    jaObj = null;
                    System.out.println("Result is. get...  " + result);

                }


            }else if (method == "POST") {
                    // request method is POST
                    // defaultHttpClient
                    // 1. create HttpClient
                    DefaultHttpClient httpClient = new DefaultHttpClient();
                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(url);
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");
                   StringEntity se = new StringEntity(json_to_string, "UTF-8");

                    httpPost.setEntity(se);

                    HttpResponse httpResponse = httpClient.execute(httpPost);

                    inputStream = httpResponse.getEntity().getContent();

                    if (inputStream != null) {
                        result = convertInputStreamToString(inputStream);
                        System.out.println("Result is....  " + result);
                        jaObj = new JSONArray(result);
                    } else {
                        jaObj = null;
                        System.out.println("Result is....  " + result);

                    }

                }


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jaObj;

    }

    /**
     * Response
     */
    private static Response response;
    public static final MediaType MEDIA_TYPE = MediaType.parse("application/json");

    public static JSONObject getDataFromWeb(String url, String u, String p, String token) {
        try {
            String result = "";

            JSONObject postdata = new JSONObject();
            try {
                postdata.put("username", u);
                postdata.put("new_password", p);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(MEDIA_TYPE, postdata.toString());

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .post(body)
                    .url(url)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Bearer " + token)
                    .addHeader("cache-control", "no-cache")
                    .build();
            System.out.println("OkHttp----   " + request.toString());
            response = client.newCall(request).execute();

            System.out.println("Responsepppp   --   "+response.body().string());
            return new JSONObject(response.body().string());
        } catch (@NonNull IOException | JSONException e) {
            Log.e(TAG, "" + e.getLocalizedMessage());
        }
        return null;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        //   BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

}