package rodearr.app.com.rodearrapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.HashMap;

import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.activity.Vehicle_Campaign_Association;
import rodearr.app.com.rodearrapp.models.VehicleALL;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;

/**
 * Created by wel come on 07-06-2018.
 */

public class VehicleListCampaignAssAdapter extends RecyclerView.Adapter<VehicleListCampaignAssAdapter.MyViewHolder> {
    private ArrayList<Integer> IMAGES;
    Context mContext;
    private ArrayList<String> nameList;
    ArrayList<VehicleALL> vehicleListResponseList;
    int selectedVehicleId;
    private HashMap<String, Boolean> mChecked;

    SharedPref sharedPref;
    int pos1=-1;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CheckBox name;
        ImageView image;
        LinearLayout vehicle_number_layout;
        RelativeLayout vehicleforcampaign;

        public MyViewHolder(View view) {
            super(view);
            name = (CheckBox) view.findViewById(R.id.name);
            vehicle_number_layout = (LinearLayout) view.findViewById(R.id.vehicle_number_layout);
            vehicleforcampaign = (RelativeLayout) view.findViewById(R.id.vehicleforcampaign);
        }
    }


    public VehicleListCampaignAssAdapter(ArrayList<VehicleALL> vehicleListResponseList, int selectedVehicleId, Context mContext) {
        sharedPref = new SharedPref(mContext);

        this.vehicleListResponseList = vehicleListResponseList;
        this.selectedVehicleId = selectedVehicleId;
        this.mContext = mContext;
        this.mChecked = new HashMap<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vehicleforcampaign_ass_listitem, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        pos1=position;
        if (!vehicleListResponseList.get(pos1).getDriver().getFirstname().trim().equals("")) {

            holder.name.setText(vehicleListResponseList.get(pos1).getVehicleNumber());
        } else {
            holder.vehicleforcampaign.setVisibility(View.GONE);

            holder.name.setVisibility(View.GONE);
        }
        System.out.println("");
        holder.name.setChecked(vehicleListResponseList.get(pos1).getSelected());

        // holder.checkBox.setTag(R.integer.btnplusview, convertView);
        holder.name.setTag(position);
        holder.name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
                System.out.println("vehicleListResponseList.get(position).getId()===   " + vehicleListResponseList.get(pos1).getId());
                Vehicle_Campaign_Association.selectedVehicleId = vehicleListResponseList.get(pos1).getId();
             //   notifyDataSetChanged();
                Integer pos = (Integer) holder.name.getTag();

                if (vehicleListResponseList.get(pos).getSelected()) {
                    vehicleListResponseList.get(pos).setSelected(false);
                    System.out.println("check assoc   Removed from array---   "+Vehicle_Campaign_Association.vehicleIds.indexOf(vehicleListResponseList.get(pos).getId())+"   ===   "+vehicleListResponseList.get(pos).getVehicleNumber());

                    Vehicle_Campaign_Association.vehicleIds.remove( Vehicle_Campaign_Association.vehicleIds.indexOf(vehicleListResponseList.get(pos).getId()));

                } else {
                    Vehicle_Campaign_Association.vehicleIds.add(vehicleListResponseList.get(pos).getId());

                    System.out.println("check assoc    added in array--   "+vehicleListResponseList.get(pos).getId()+"   =  "+Vehicle_Campaign_Association.vehicleIds.size()+"  ==   "+vehicleListResponseList.get(pos).getVehicleNumber());

                    vehicleListResponseList.get(pos).setSelected(true);
                }






         /*UC       //set your object's last status
                System.out.println("vehicleListResponseList.get(position).getId()===   " + vehicleListResponseList.get(pos1).getId());
                Vehicle_Campaign_Association.selectedVehicleId = vehicleListResponseList.get(pos1).getId();
                notifyDataSetChanged();


                if(isChecked){
                    Vehicle_Campaign_Association.vehicleIds.add(vehicleListResponseList.get(pos1).getId());
                    holder.name.setChecked(true);
                    System.out.println("added in array--   "+vehicleListResponseList.get(pos1).getId()+"   =  "+Vehicle_Campaign_Association.vehicleIds.size());
                }else {
                    System.out.println("Removed from array---   "+Vehicle_Campaign_Association.vehicleIds.indexOf(vehicleListResponseList.get(pos1).getId()));
                    holder.name.setChecked(false);
                    Vehicle_Campaign_Association.vehicleIds.remove( Vehicle_Campaign_Association.vehicleIds.indexOf(vehicleListResponseList.get(pos1).getId()));
                        }
*/

            }
        });
       /* if(vehicleListResponseList.get(position).getDriver().getFirstname().trim().equals("")){

            holder.name.setTextColor(Color.RED);
        }else {
            holder.name.setTextColor(mContext.getResources().getColor(R.color.color_green));
        }*/
        System.out.println("check driver for current vehicle---   " + vehicleListResponseList.get(position).getDriver().getFirstname().trim() + "   pos-  " + position + "   number -   " + vehicleListResponseList.get(position).getVehicleNumber());
     /*   holder.vehicle_number_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, UpdateVehicle.class);
                System.out.println("---????   "+vehicleListResponseList.get(position).getId());
                i.putExtra("id", vehicleListResponseList.get(position).getId());
                i.putExtra("driver_id", vehicleListResponseList.get(position).getDriver().getId());

                i.putExtra("number", vehicleListResponseList.get(position).getVehicleNumber());
                mContext.startActivity(i);
                sharedPref.setIntegerData(Global_Data.vid,vehicleListResponseList.get(position).getId());
            }
        });*/


        //  holder.image.setImageResource(IMAGES.get(position));
       /* Movie movie = moviesList.get(position);

        holder.genre.setText(movie.getGenre());
        holder.year.setText(movie.getYear());*/
    }

    @Override
    public int getItemCount() {
        return vehicleListResponseList.size();
    }
}
