package rodearr.app.com.rodearrapp.models;

/**
 * Created by wel come on 14-06-2018.
 */

public class Compaign_response {

    String image, duration, earning;

    public Compaign_response(String image, String duration, String earning) {
        this.image = image;
        this.duration = duration;
        this.earning = earning;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getEarning() {
        return earning;
    }

    public void setEarning(String earning) {
        this.earning = earning;
    }
}
