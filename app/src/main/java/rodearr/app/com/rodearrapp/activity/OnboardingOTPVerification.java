package rodearr.app.com.rodearrapp.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudrail.si.CloudRail;
import com.cloudrail.si.interfaces.SMS;
import com.cloudrail.si.services.Nexmo;
import com.cloudrail.si.services.Twilio;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;

public class OnboardingOTPVerification extends AppCompatActivity {
    @BindViews({R.id.otp1, R.id.otp2, R.id.otp3, R.id.otp4, R.id.otp5, R.id.otp6, R.id.otp11, R.id.otp22, R.id.otp33, R.id.otp44, R.id.otp55, R.id.otp66})
    List<EditText> otpEditTextViews;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;


    @BindView(R.id.paytm_number_text)
    TextView paytm_number_text;
    @BindView(R.id.paytm_otp_layout)
    LinearLayout paytm_otp_layout;
    @BindView(R.id.verify_btn)
    Button verify_btn;
    Intent i;
    SharedPref _sharedPref;
    Twilio twilio;
    SMS service;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding_otpverification);
        ButterKnife.bind(this);


        _sharedPref = new SharedPref(OnboardingOTPVerification.this);
//RECEIVE DATA VIA INTENT
        i = getIntent();
        if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Owner") && (!i.getStringExtra("contact_number").equals(i.getStringExtra("paytm_number")))) {

            paytm_number_text.setVisibility(View.VISIBLE);
            paytm_otp_layout.setVisibility(View.VISIBLE);
        } else {
            paytm_number_text.setVisibility(View.GONE);
            paytm_otp_layout.setVisibility(View.GONE);
        }
        System.out.println("----");
        if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Owner")) {
            if (Global_Data.checkInternetConnection(OnboardingOTPVerification.this)) {
                if (Build.VERSION.SDK_INT > 11) {

                    new SendSMSTask().executeOnExecutor(Global_Data.sExecutor);


                } else {

                    new SendSMSTask().execute();
                }

            } else {

                Toast.makeText(OnboardingOTPVerification.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
            }
            if (!i.getStringExtra("contact_number").equals(i.getStringExtra("paytm_number"))) {
                if (Global_Data.checkInternetConnection(OnboardingOTPVerification.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new SendSMSOwnerTask().executeOnExecutor(Global_Data.sExecutor);


                    } else {

                        new SendSMSOwnerTask().execute();
                    }

                } else {

                    Toast.makeText(OnboardingOTPVerification.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            if (Global_Data.checkInternetConnection(OnboardingOTPVerification.this)) {
                if (Build.VERSION.SDK_INT > 11) {

                    new SendSMSTask().executeOnExecutor(Global_Data.sExecutor);


                } else {

                    new SendSMSTask().execute();
                }

            } else {

                Toast.makeText(OnboardingOTPVerification.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
        otpEditTextViews.get(0).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(1).requestFocus();
                    otpEditTextViews.get(1).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(1).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(2).requestFocus();
                    otpEditTextViews.get(2).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(2).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(3).requestFocus();
                    otpEditTextViews.get(3).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(3).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(4).requestFocus();
                    otpEditTextViews.get(4).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(4).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(5).requestFocus();
                    otpEditTextViews.get(5).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(5).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(5).requestFocus();
                    otpEditTextViews.get(5).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //.........................

        otpEditTextViews.get(6).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(7).requestFocus();
                    otpEditTextViews.get(7).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(7).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(8).requestFocus();
                    otpEditTextViews.get(8).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(8).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(9).requestFocus();
                    otpEditTextViews.get(9).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(9).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(10).requestFocus();
                    otpEditTextViews.get(10).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(10).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(11).requestFocus();
                    otpEditTextViews.get(11).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(11).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(11).requestFocus();
                    otpEditTextViews.get(11).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    /*constraintLayoutViewList.get(1).setVisibility(View.VISIBLE);
                    constraintLayoutViewList.get(0).setVisibility(View.GONE);
                    constraintLayoutViewList.get(2).setVisibility(View.GONE);*/

                if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Owner")) {
                    String s = "", paytm_otp = "";
                    for (int i = 0; i < 6; i++) {
                        s += otpEditTextViews.get(i).getText().toString();
                        System.out.println("String OTP--   " + s);
                    }
                    for (int i = 6; i < 12; i++) {
                        paytm_otp += otpEditTextViews.get(i).getText().toString();
                        System.out.println("String paytm OTP--   " + paytm_otp);
                    }
                    if (!s.equals(_sharedPref.getStringData(Global_Data.mobile_number_otp))) {
                        Toast.makeText(OnboardingOTPVerification.this, "Incorrect mobile number OTP.  ",
                                Toast.LENGTH_SHORT).show();
                    } else if (!paytm_otp.equals(_sharedPref.getStringData(Global_Data.paytm_number_otp))) {
                        Toast.makeText(OnboardingOTPVerification.this, "Incorrect Paytm number OTP.  ",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(OnboardingOTPVerification.this, HomeActivity.class);

                        startActivity(intent);
                        finish();
                    }
                } else {
                    String s = "", paytm_otp = "";
                    for (int i = 0; i < 6; i++) {
                        s += otpEditTextViews.get(i).getText().toString();
                        System.out.println("String OTP--   " + s);
                    }
                    if (!s.equals(_sharedPref.getStringData(Global_Data.mobile_number_otp))) {
                        Toast.makeText(OnboardingOTPVerification.this, "Incorrect mobile number OTP.  ",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(OnboardingOTPVerification.this, HomeActivity.class);

                        startActivity(intent);
                        finish();
                    }
                }


            }
        });

    }

    int range = 9;  // to generate a single number with this range, by default its 0..9
    int length = 6; // by default length is 4

    public int generateRandomNumber() {
        int randomNumber;

        SecureRandom secureRandom = new SecureRandom();
        String s = "";
        for (int i = 0; i < length; i++) {
            int number = secureRandom.nextInt(range);
            if (number == 0 && i == 0) { // to prevent the Zero to be the first number as then it will reduce the length of generated pin to three or even more if the second or third number came as zeros
                i = -1;
                continue;
            }
            s = s + number;
        }

        randomNumber = Integer.parseInt(s);

        return randomNumber;
    }

    int range1 = 9;  // to generate a single number with this range, by default its 0..9
    int length1 = 6; // by default length is 4

    public int generateRandomNumberOwner() {
        int randomNumber;

        SecureRandom secureRandom = new SecureRandom();
        String s = "4";
        for (int i = 0; i < length1; i++) {
            int number = secureRandom.nextInt(range1);
            if (number == 0 && i == 0) { // to prevent the Zero to be the first number as then it will reduce the length of generated pin to three or even more if the second or third number came as zeros
                i = -1;
                continue;
            }
            s = s + number;
        }

        randomNumber = Integer.parseInt(s);

        return randomNumber;
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);
        int receiveSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
        int readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    public JSONObject RegistrationFunctionOwner() {
        JSONObject mainJson = new JSONObject();
        try {

            /* @BindViews({R.id.first_name,R.id.last_name ,R.id.email_id ,R.id.mobile_number ,
            R.id.dob ,R.id.username ,
                        R.id.password ,R.id.license_number ,R.id.house_number ,R.id.address ,R.id.landmark })
                private List<EditText> editTextViewList;
          */

            mainJson.put("firstname", i.getStringExtra("firstname"));
            mainJson.put("lastname", i.getStringExtra("lastname"));
            mainJson.put("gender", i.getStringExtra("gender"));
            mainJson.put("contact_number", i.getStringExtra("contact_number"));
            mainJson.put("dob", i.getStringExtra("dob"));
            mainJson.put("license_number", i.getStringExtra("license_number"));
            mainJson.put("paytm_number", i.getStringExtra("paytm_number"));
            JSONObject userJson = new JSONObject();
            userJson.put("username", i.getStringExtra("username"));
            userJson.put("role", "Vehicle_Owner");
            userJson.put("password", i.getStringExtra("password"));
            JSONObject addressJson = new JSONObject();
            addressJson.put("house", i.getStringExtra("house"));
            addressJson.put("address1", i.getStringExtra("address1"));
            addressJson.put("nearby", i.getStringExtra("nearby"));
            addressJson.put("area", i.getIntExtra("area", 0));
            addressJson.put("city", i.getIntExtra("city", 0));
            addressJson.put("state", i.getIntExtra("state", 0));
            addressJson.put("country", i.getIntExtra("country", 0));
            mainJson.put("user", userJson);
            mainJson.put("address", addressJson);
            System.out.println("string RegisterModel Owner " + mainJson.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJson;
    }

    private class RegistrationTaskOwner extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _registrationJSON;
        ProgressDialog progressDialog = new ProgressDialog(OnboardingOTPVerification.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = RegistrationFunctionOwner().toString();
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _registrationJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/", "POST", json2);
                System.out.println(_registrationJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                JSONObject jo1 = null;
                if (_registrationJSON.has("user")) {
                    jo1 = new JSONObject(_registrationJSON.getString("user"));
                }
                System.out.println("if error");

                System.out.println("contact number");
                //  System.out.println("check--   " + _registrationJSON.getString("contact_number").toString());
                if (_registrationJSON.has("contact_number") && _registrationJSON.getString("contact_number").toString().equals("[\"Owners with this contact number already exists.\"]")) {
                    //     if (_registrationJSON.getString("contact_number").toString().equals("[\"Owners with this contact number already exists.\"]")) {

                    System.out.println("ddsmdcsnncds");
                    Toast.makeText(OnboardingOTPVerification.this, "Error-  Owner with this contact number already exists.".toString(),
                            Toast.LENGTH_SHORT).show();
                    // }
                } else if (_registrationJSON.has("username") && _registrationJSON.getString("username").equals("[\"Users with this Username already exists.\"]")) {

                    //   if (jo1.getString("username").equals("[\"Users with this Username already exists.\"]")) {
                    System.out.println("user");
                    Toast.makeText(OnboardingOTPVerification.this, "Error-  Users with this Username already exists.",
                            Toast.LENGTH_SHORT).show();
                    // }
                } else if (_registrationJSON.has("license_number") && _registrationJSON.getString("license_number").equals("[\"Owners with this license number already exists.\"]")) {
                    //   if (jo1.getString("license_number").equals("[\"Owners with this license number already exists.\"]")) {
                    System.out.println("license_number");
                    Toast.makeText(OnboardingOTPVerification.this, "Error-  Owners with this license number already exists.",
                            Toast.LENGTH_SHORT).show();
                    // }
                } else if (_registrationJSON.has("paytm_number") && _registrationJSON.getString("paytm_number").equals("[\"Owners with this paytm number already exists.\"]")) {
                    //  if (jo1.getString("paytm_number").equals("[\"Owners with this paytm number already exists.\"]")) {
                    System.out.println("paytm_number");
                    Toast.makeText(OnboardingOTPVerification.this, "Error-  Owners with this paytm number already exists.",
                            Toast.LENGTH_SHORT).show();
                    //}
                } else if (_registrationJSON.has("user") && jo1.getString("username").equals("[\"Users with this Username already exists.\"]")) {
                    //  if (jo1.getString("paytm_number").equals("[\"Owners with this paytm number already exists.\"]")) {
                    System.out.println("[\"Users with this Username already exists.\"]");
                    Toast.makeText(OnboardingOTPVerification.this, "Error- Users with this Username already exists.",
                            Toast.LENGTH_SHORT).show();
                    //}
                }
              /*  else  if (_registrationJSON.getString("license_number").toString().equals("[\"Drivers with this license number already exists.\"]")) {
                    System.out.println("license_number");

                    Toast.makeText(getActivity(), "Error-  Drivers with this license number already exists.",
                            Toast.LENGTH_SHORT).show();
                }*/
                else {

                    System.out.println("----");

                    if (_registrationJSON.has("user")) {
                        JSONObject jo2 = new JSONObject(_registrationJSON.getString("user"));
                        System.out.println("user ----   " + jo2.getString("username"));
                        _sharedPref.setStringData(Global_Data.username, jo2.getString("username"));
                        _sharedPref.setStringData(Global_Data.user_role, "Vehicle_Owner");
                        _sharedPref.setStringData(Global_Data.id, String.valueOf(_registrationJSON.getInt("id")));


                        Intent intent = new Intent(OnboardingOTPVerification.this, HomeActivity.class);

                        startActivity(intent);
                        finish();

                    } else {

                        Toast.makeText(OnboardingOTPVerification.this, "Error-  " + _registrationJSON.toString(),
                                Toast.LENGTH_SHORT).show();
                    }
                }

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();

                progressDialog.dismiss();
              /*  if (Global_Data.checkInternetConnection(OnboardingOTPVerification.this)) {

                    if (Build.VERSION.SDK_INT > 11) {
                        new RegistrationTaskOwner().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new RegistrationTaskOwner().execute();
                    }
                } else {

                    Toast.makeText(OnboardingOTPVerification.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }*/
            }


        }
    }

    //......................................................


    public JSONObject RegistrationFunctionDriver() {
        JSONObject mainJson = new JSONObject();
        try {

            /* @BindViews({R.id.first_name,R.id.last_name ,R.id.email_id ,R.id.mobile_number ,
            R.id.dob ,R.id.username ,
                        R.id.password ,R.id.license_number ,R.id.house_number ,R.id.address ,R.id.landmark })
                private List<EditText> editTextViewList;
          */

            mainJson.put("firstname", i.getStringExtra("firstname"));
            mainJson.put("lastname", i.getStringExtra("lastname"));
            mainJson.put("gender", i.getStringExtra("gender"));
            mainJson.put("contact_number", i.getStringExtra("contact_number"));
            mainJson.put("dob", i.getStringExtra("dob"));
            mainJson.put("license_number", i.getStringExtra("license_number"));

            JSONObject userJson = new JSONObject();
            userJson.put("username", i.getStringExtra("username"));
            userJson.put("role", "Vehicle_Driver");
            userJson.put("password", i.getStringExtra("password"));
            JSONObject addressJson = new JSONObject();
            addressJson.put("house", i.getStringExtra("house"));
            addressJson.put("address1", i.getStringExtra("address1"));
            addressJson.put("nearby", i.getStringExtra("nearby"));
            addressJson.put("area", i.getIntExtra("area", 0));
            addressJson.put("city", i.getIntExtra("city", 0));
            addressJson.put("state", i.getIntExtra("state", 0));
            addressJson.put("country", i.getIntExtra("country", 0));
            mainJson.put("user", userJson);
            mainJson.put("address", addressJson);

            System.out.println("string RegisterModel  " + mainJson.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJson;
    }


    private class RegistrationTaskDriver extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _registrationJSON;
        ProgressDialog progressDialog = new ProgressDialog(OnboardingOTPVerification.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = RegistrationFunctionDriver().toString();
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _registrationJSON = jsonParser.makeHttpRequest(Global_Data._url + "drivers/", "POST", json2);
                System.out.println(_registrationJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jo1 = null;
                if (_registrationJSON.has("user")) {
                    jo1 = new JSONObject(_registrationJSON.getString("user"));
                }
                System.out.println("if error");

                //   System.out.println("contact number");
                // System.out.println("check--   " + _registrationJSON.getString("contact_number").toString());
                if (_registrationJSON.has("contact_number") && (_registrationJSON.getString("contact_number").toString().equals("[\"Drivers with this contact number already exists.\"]"))) {
                    // if (_registrationJSON.getString("contact_number").toString().equals("[\"Drivers with this contact number already exists.\"]")) {

                    System.out.println("ddsmdcsnncds");
                    Toast.makeText(OnboardingOTPVerification.this, "Error-  Drivers with this contact number already exists.".toString(),
                            Toast.LENGTH_SHORT).show();
                    //}
                } else if (_registrationJSON.has("user") && (jo1.getString("username").equals("[\"Users with this Username already exists.\"]"))) {
                    //  if (jo1.getString("username").equals("[\"Users with this Username already exists.\"]")) {
                    System.out.println("user");
                    Toast.makeText(OnboardingOTPVerification.this, "Error-  Users with this Username already exists.",
                            Toast.LENGTH_SHORT).show();
                    //}
                } else if (_registrationJSON.has("license_number") && _registrationJSON.getString("license_number").toString().equals("[\"Drivers with this license number already exists.\"]")) {

                    //    if (_registrationJSON.getString("license_number").toString().equals("[\"Drivers with this license number already exists.\"]")) {
                    System.out.println("license_number");

                    Toast.makeText(OnboardingOTPVerification.this, "Error-  Drivers with this license number already exists.",
                            Toast.LENGTH_SHORT).show();
                    //  }
                } /*else if (_registrationJSON.has("gender")) {
                    if (_registrationJSON.getString("gender").toString().equals("[\"\\\"0\\\" is not a valid choice.\"]")) {
                        System.out.println("gender");

                        Toast.makeText(OnboardingOTPVerification.this, "Error-  Incorrect selection of gender.",
                                Toast.LENGTH_SHORT).show();
                    }
                } */ else {


                    if (_registrationJSON.has("user")) {
                        JSONObject jo2 = new JSONObject(_registrationJSON.getString("user"));
                        System.out.println("user ----   " + jo2.getString("username"));
                        _sharedPref.setStringData(Global_Data.username, jo2.getString("username"));
                        _sharedPref.setStringData(Global_Data.user_role, "Vehicle_Driver");
                        _sharedPref.setStringData(Global_Data.id, String.valueOf(_registrationJSON.getInt("id")));
                        Intent intent = new Intent(OnboardingOTPVerification.this, HomeActivity.class);

                        startActivity(intent);
                        finish();

                    } else {
                        //   smsManager.sendTextMessage(editTextViewList.get(3).getText().toString(), null, mobileNumberOTP, null, null);

                        Toast.makeText(OnboardingOTPVerification.this, "Error-  " + _registrationJSON.toString(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                Toast.makeText(OnboardingOTPVerification.this, "SMS RCVD--  " + message + "  &  " + intent.getStringExtra("Sender"), Toast.LENGTH_SHORT).show();
                if (intent.getStringExtra("Sender").equals("IX-TFCTOR")  || intent.getStringExtra("Sender").contains("-Roderr")) {
                    /*Intent intent1 = new Intent(OnboardingOTPVerification.this, OnboardingOTPVerification.class);
                    intent1.putExtra("message", message);
                    startActivity(intent1);*/
                    //intent1.putExtra("firstname", editTextViewList.get(0).getText().toString());
                  try {
                      otpEditTextViews.get(0).setText(_sharedPref.getStringData(Global_Data.mobile_number_otp).substring(0, 1));
                      otpEditTextViews.get(1).setText(_sharedPref.getStringData(Global_Data.mobile_number_otp).substring(1, 2));
                      otpEditTextViews.get(2).setText(_sharedPref.getStringData(Global_Data.mobile_number_otp).substring(2, 3));
                      otpEditTextViews.get(3).setText(_sharedPref.getStringData(Global_Data.mobile_number_otp).substring(3, 4));
                      otpEditTextViews.get(4).setText(_sharedPref.getStringData(Global_Data.mobile_number_otp).substring(4, 5));
                      otpEditTextViews.get(5).setText(_sharedPref.getStringData(Global_Data.mobile_number_otp).substring(5, 6));
                      if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Owner")) {
                          otpEditTextViews.get(6).setText(_sharedPref.getStringData(Global_Data.paytm_number_otp).substring(0, 1));
                          otpEditTextViews.get(7).setText(_sharedPref.getStringData(Global_Data.paytm_number_otp).substring(1, 2));
                          otpEditTextViews.get(8).setText(_sharedPref.getStringData(Global_Data.paytm_number_otp).substring(2, 3));
                          otpEditTextViews.get(9).setText(_sharedPref.getStringData(Global_Data.paytm_number_otp).substring(3, 4));
                          otpEditTextViews.get(10).setText(_sharedPref.getStringData(Global_Data.paytm_number_otp).substring(4, 5));
                          otpEditTextViews.get(11).setText(_sharedPref.getStringData(Global_Data.paytm_number_otp).substring(5, 6));

                      }
                 /*   Intent intent2 = new Intent(OnboardingOTPVerification.this, HomeActivity.class);

                    startActivity(intent2);
                    finish();  */

                  }catch (Exception e){
                      e.printStackTrace();
                  }
                }

               /* TextView tv = (TextView) findViewById(R.id.txtview);
                tv.setText(message);
           */
            }
        }
    };

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(OnboardingOTPVerification.this).
                registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(OnboardingOTPVerification.this).unregisterReceiver(receiver);
    }

    private class SendSMSTask extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _sendOTPJSON;
        ProgressDialog progressDialog = new ProgressDialog(OnboardingOTPVerification.this);
        String mobileNumberOTP = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                /*
                *  String mobileNumberOTP = String.valueOf(generateRandomNumber());
                    SmsManager smsManager = SmsManager.getDefault();

                    if (_registrationJSON.has("user")) {
                        JSONObject jo2 = new JSONObject(_registrationJSON.getString("user"));
                        System.out.println("user ----   " + jo2.getString("username"));
                        _sharePref.setStringData(Global_Data.username, jo2.getString("username"));
                        _sharePref.setStringData(Global_Data.user_role, "Vehicle_Driver");
                        _sharePref.setStringData(Global_Data.id,String.valueOf(_registrationJSON.getInt("id")));
                         smsManager.sendTextMessage(editTextViewList.get(3).getText().toString(), null, mobileNumberOTP, null, null);

                *
                * */
              /*  service = new Nexmo(OnboardingOTPVerification.this, "bb426b52", "8f89b6eb12469bf1");
                service.sendSMS(
                        "Patrick",
                        "+917879977162",
                        "Hey, how are you?"
                );*/

                mobileNumberOTP = String.valueOf(generateRandomNumber());
                String json2 = i.getStringExtra("contact_number") + "/" + mobileNumberOTP;
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
              //  _sendOTPJSON = jsonParser.makeHttpRequest("https://2factor.in/API/V1/4a668095-46be-11e8-a895-0200cd936042/SMS/" + json2, "POST", "");



                String msg = mobileNumberOTP+ "is your one time password (OTP) for Rodearr mobile verification";
                msg = URLEncoder.encode(msg, "UTF-8");

                _sendOTPJSON = jsonParser.makeHttpRequest("http://api.msg91.com/api/sendotp.php?authkey=233400AIB8GlSxIoE95b7ef9cc&mobile=" + i.getStringExtra("contact_number") + "&message=" + msg + "&sender=Roderr&otp=" + mobileNumberOTP, "GET", json2);
                System.out.println("===>  second otp  "+mobileNumberOTP);

                System.out.println(_sendOTPJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jo1 = null;
                /*if (_sendOTPJSON.has("Status")) {
                    //Toast.makeText(OnboardingOTPVerification.this, "SMS sent--  " + _sendOTPJSON.getString("Status"), Toast.LENGTH_SHORT).show();

                    _sharedPref.setStringData(Global_Data.mobile_number_otp, mobileNumberOTP);


                }*/
                System.out.println("Message output---   " + _sendOTPJSON.toString());

                if (_sendOTPJSON.getString("type").equals("success")) {
                    _sharedPref.setStringData(Global_Data.mobile_number_otp, mobileNumberOTP);

                  /*  msg.setText("Invitaton sent to "+senderNumber);
                    dialog.show();*/
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }
//..............


    private class SendSMSOwnerTask extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _sendOTPJSON;
        ProgressDialog progressDialog = new ProgressDialog(OnboardingOTPVerification.this);
        String paytmNumberOTP = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {


                paytmNumberOTP = String.valueOf(generateRandomNumber());
                String json2 = i.getStringExtra("paytm_number") + "/" + paytmNumberOTP;
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
             //   _sendOTPJSON = jsonParser.makeHttpRequest("https://2factor.in/API/V1/4a668095-46be-11e8-a895-0200cd936042/SMS/" + json2, "POST", "");




                String msg = paytmNumberOTP+ "is your one time password (OTP) for Rodearr mobile verification";
                msg = URLEncoder.encode(msg, "UTF-8");

                _sendOTPJSON = jsonParser.makeHttpRequest("http://api.msg91.com/api/sendotp.php?authkey=233400AIB8GlSxIoE95b7ef9cc&mobile=" + i.getStringExtra("contact_number") + "&message=" + msg + "&sender=Roderr&otp=" + paytmNumberOTP, "GET", json2);
                System.out.println("===>  second otp  "+paytmNumberOTP);

                System.out.println(_sendOTPJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jo1 = null;
                System.out.println("Message output---   " + _sendOTPJSON.toString());

                if (_sendOTPJSON.getString("type").equals("success")) {
                    _sharedPref.setStringData(Global_Data.paytm_number_otp, paytmNumberOTP);

                  /*  msg.setText("Invitaton sent to "+senderNumber);
                    dialog.show();*/
                }
              /*  if (_sendOTPJSON.has("Status") && _sendOTPJSON.getString("Status").equals("Success")) {
                    Toast.makeText(OnboardingOTPVerification.this, "SMS sent--  " + _sendOTPJSON.getString("Status"), Toast.LENGTH_SHORT).show();

                    _sharedPref.setStringData(Global_Data.paytm_number_otp, paytmNumberOTP);


                }*/

              /*  if (_sendOTPJSON.has("Status") && _sendOTPJSON.getString("Status").equals("Error")) {
                    if (Global_Data.checkInternetConnection(OnboardingOTPVerification.this)) {
                        if (Build.VERSION.SDK_INT > 11) {

                            new SendSMSOwnerTask().executeOnExecutor(Global_Data.sExecutor);


                        } else {

                            new SendSMSOwnerTask().execute();
                        }

                    } else {

                        Toast.makeText(OnboardingOTPVerification.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }


                }*/
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

}
