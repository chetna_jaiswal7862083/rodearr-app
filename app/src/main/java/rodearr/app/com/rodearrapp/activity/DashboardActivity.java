package rodearr.app.com.rodearrapp.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.adapter.VehicleWithDriverListAdapter;
import rodearr.app.com.rodearrapp.classes.WorkaroundMapFragment;
import rodearr.app.com.rodearrapp.fragment.Dashboard;
import rodearr.app.com.rodearrapp.fragment.ListOfVehiclesOnDashboard;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.models.Brand;
import rodearr.app.com.rodearrapp.models.Category;
import rodearr.app.com.rodearrapp.models.Driver;
import rodearr.app.com.rodearrapp.models.Model;
import rodearr.app.com.rodearrapp.models.Stats;
import rodearr.app.com.rodearrapp.models.VehicleALL;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;


public class DashboardActivity extends AppCompatActivity implements OnMapReadyCallback {
    private ScrollView mScrollView;
    View view;
    MapView mMapView;
    private GoogleMap googleMap;
    ArrayList<VehicleALL> vehicleListResponseList;
    @BindViews({R.id.driving_through_textview1, R.id.you_drove_kms1, R.id.you_drove_week_kmsvalue1, R.id.you_drove_month_kmsvalue1, R.id.you_drove_lastyear_kmsvalue1})
    List<TextView> textViewList;
    String pos = "";
    String username = "", vehicle_number = "";
    SharedPref _sp;
    private VehicleWithDriverListAdapter vehicleListAdapter;
    String owner_id = "", vehicle_id = "0", driver_id = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        // Inflate the layout for this fragment
          _sp = new SharedPref(DashboardActivity.this);

        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).getMapAsync(this);
        mScrollView = (ScrollView) findViewById(R.id.sv_container);

        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                mScrollView.requestDisallowInterceptTouchEvent(true);
            }
        });
        /* mMapView.getMapAsync(this);
         */

        vehicleListResponseList = (ArrayList<VehicleALL>) getIntent().getSerializableExtra(
                "VEHICLE");

        pos = String.valueOf(getIntent().getExtras().getInt("POS"));
        System.out.println("------->>>>>>     " + vehicleListResponseList.get(Integer.parseInt(pos)).getDriver().getStats().getLattitude() + "   pos--   " + pos);


    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;

        setUpMap();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                if (Global_Data.checkInternetConnection(DashboardActivity.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetVehicleListOfOwner().execute();
                    }

                } else {
                    Snackbar.make(findViewById(R.id.rdashboard_layout), "Please check internet connection.", Snackbar.LENGTH_LONG).show();

                  //  Toast.makeText(DashboardActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                handler.postDelayed(this, 40000);
                // handler.postDelayed(this, 7000);
            }
        }, 40000);
    }

    public void setUpMap() {
        try {
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            if (ActivityCompat.checkSelfPermission(DashboardActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DashboardActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
           // googleMap.setMyLocationEnabled(true);
            googleMap.setTrafficEnabled(true);

            googleMap.setIndoorEnabled(true);
            googleMap.setBuildingsEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            googleMap.clear();

            // latitude and longitude
           /* double latitude = 17.385044;
            double longitude = 78.486671;

            // create marker
            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(latitude, longitude)).title("Hello Maps");

            // Changing marker icon
            marker.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
*/
            // adding marker
            // googleMap.addMarker(marker);
            for (int i = 0; i < vehicleListResponseList.size(); i++) {
                //   System.out.println("check coordinates----   "+vehicleListResponseList.get(i).getDriver().getStats().getLattitude()+"   ...   "+vehicleListResponseList.get(i).getDriver().getStats().getLongitude());
                if (vehicleListResponseList.get(i).getDriver().getStats().getLattitude()!=null && vehicleListResponseList.get(i).getDriver().getStats().getLongitude()!=null) {


                    if (_sp.getIntegerData(Global_Data.pos) == i) {
                        try {

                            createMarker(Double.parseDouble(vehicleListResponseList.get(i).getDriver().getStats().getLattitude()), Double.parseDouble(vehicleListResponseList.get(i).getDriver().getStats().getLongitude()), vehicleListResponseList.get(i).getDriver().getFirstname() + " " + vehicleListResponseList.get(i).getDriver().getLastname(), vehicleListResponseList.get(i).getVehicleNumber(), R.drawable.bc);
                            CameraPosition cameraPosition1 = new CameraPosition.Builder()
                                    .target(new LatLng(Double.parseDouble(vehicleListResponseList.get(i).getDriver().getStats().getLattitude()), Double.parseDouble(vehicleListResponseList.get(i).getDriver().getStats().getLongitude()))).zoom(15).build();
                            googleMap.animateCamera(CameraUpdateFactory
                                    .newCameraPosition(cameraPosition1));
                            latitude = Double.parseDouble(vehicleListResponseList.get(i).getDriver().getStats().getLattitude());
                            longitude = Double.parseDouble(vehicleListResponseList.get(i).getDriver().getStats().getLongitude());

                            String first = "You have selected", middle = " car he is driving though ";
                            String selected_driver = "<font color='#389BCB'>" + " " + vehicleListResponseList.get(i).getDriver().getFirstname() + " " + vehicleListResponseList.get(i).getDriver().getLastname() + "'s " + "</font>";
                            String next = "<font color='#389BCB'>" + getAddress() + "</font>";
                            String last = " Be happy that you will earn more!";
                            textViewList.get(0).setText(Html.fromHtml(first + selected_driver + middle + next + "" + last));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {
                        try{

                        createMarker1(Double.parseDouble(vehicleListResponseList.get(i).getDriver().getStats().getLattitude()), Double.parseDouble(vehicleListResponseList.get(i).getDriver().getStats().getLongitude()), vehicleListResponseList.get(i).getDriver().getFirstname() + " " + vehicleListResponseList.get(i).getDriver().getLastname(), vehicleListResponseList.get(i).getVehicleNumber(), R.drawable.yc);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    }


                }
            }

                      /*  @BindViews({R.id.driving_through_textview,R.id.you_drove_kms,R.id.you_drove_week_kmsvalue,R.id.you_drove_month_kmsvalue,R.id.you_drove_lastyear_kmsvalue})
                        List<TextView> textViewList;
  */
            System.out.println("dashboard activity---     "+_sp.getIntegerData(Global_Data.pos)+"  --    "+vehicleListResponseList.get(_sp.getIntegerData(Global_Data.pos)).getDriver().getStats().getDaily());
            textViewList.get(1).setText(String.valueOf(vehicleListResponseList.get(_sp.getIntegerData(Global_Data.pos)).getDriver().getStats().getDaily()));
            textViewList.get(2).setText(String.valueOf(vehicleListResponseList.get(_sp.getIntegerData(Global_Data.pos)).getDriver().getStats().getWeekly()));

            textViewList.get(3).setText(String.valueOf(vehicleListResponseList.get(_sp.getIntegerData(Global_Data.pos)).getDriver().getStats().getMonthly()));
            textViewList.get(4).setText(String.valueOf(vehicleListResponseList.get(_sp.getIntegerData(Global_Data.pos)).getDriver().getStats().getYearly()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    double latitude = 0.0, longitude = 0.0;

    public String getAddress() {
        String address = "", city = "", state = "", country = "", postalCode = "", knownName = "", s = "";
        try {
            Geocoder geocoder;
            List<android.location.Address> addresses;
            geocoder = new Geocoder(DashboardActivity.this, Locale.getDefault());


            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            postalCode = addresses.get(0).getPostalCode();
            knownName = addresses.get(0).getFeatureName();
            s = addresses.get(0).getThoroughfare()+","+ city+" ";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;

    }
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                // Disallow ScrollView to intercept touch events.
                mScrollView.requestDisallowInterceptTouchEvent(true);
                break;

            case MotionEvent.ACTION_UP:
                // Allow ScrollView to intercept touch events.
                mScrollView.requestDisallowInterceptTouchEvent(false);
                break;
        }

        // Handle MapView's touch events.
        super.onTouchEvent(ev);
        return true;
    }

    protected Marker createMarker(double latitude, double longitude, String title, String snippet, int iconResID) {

        return googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
               /* .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));*/
        // .icon(BitmapDescriptorFactory.fromResource(iconResID)));
    }

    protected Marker createMarker1(double latitude, double longitude, String title, String snippet, int iconResID) {

        return googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
                /*.icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));*/
        // .icon(BitmapDescriptorFactory.fromResource(iconResID)));
    }

    //............................................................................................

    private class GetVehicleListOfOwner extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfOwnerJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(DashboardActivity.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*progressDialog.setMessage("Please wait....");
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sp.getStringData(Global_Data.id), "GET", json2);

                //Uncomment it when code is running properly.       _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + 2, "GET", json2);

                System.out.println("Owner's vehicle list in listof vehicles------   " + _vehicleListOfOwnerJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                vehicleListResponseList.clear();
                if (_vehicleListOfOwnerJSON != null) {
                     if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                        System.out.println("Inside not null");
                        jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                        System.out.println("Check vehicle json array---   " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").length());


                        for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {

                        try {
                                System.out.println("value of i---->..   " + i);

                                vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getJSONObject("location").getString("longitude"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getJSONObject("location").getString("lattitude")))));

                            } catch (Exception e) {
                                System.out.println("value of i---->..   " + i);

                                vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(111, "", "", "", "", "", "", "", new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                e.printStackTrace();
                            }

                        }
                        setUpMap();
                        vehicle_id = String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(0).getInt("id"));
                        driver_id = vehicle_id;
                        //   vehicleListResponseArrayAdapter.notifyDataSetChanged();
                        //rv.setVisibility(View.VISIBLE);
                        //  vehicleListAdapter.notifyDataSetChanged();
                    } else {
                        System.out.println("Inside null");
                    }
                } else {

                    Toast.makeText(DashboardActivity.this, _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();
                 //   Snackbar.make(findViewById(R.id.rdashboard_layout), _vehicleListOfOwnerJSON.toString(), Snackbar.LENGTH_LONG).show();

                }
              /*  progressDialog.dismiss();
*/
            } catch (Exception e) {
                e.printStackTrace();

              //  Toast.makeText(DashboardActivity.this, "Vehicles are not associated for this owner.", Toast.LENGTH_SHORT).show();
              /*  progressDialog.dismiss();
*/
            }


        }
    }

}
