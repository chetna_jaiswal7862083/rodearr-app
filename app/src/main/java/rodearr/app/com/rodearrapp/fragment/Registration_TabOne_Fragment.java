package rodearr.app.com.rodearrapp.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.activity.OnboardingOTPVerification;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.models.AreaListResponse;
import rodearr.app.com.rodearrapp.models.CityListResponse;
import rodearr.app.com.rodearrapp.models.CountryListResponse;
import rodearr.app.com.rodearrapp.models.StateListResponse;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;


public class Registration_TabOne_Fragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    @BindViews({R.id.gender, R.id.area, R.id.state, R.id.country, R.id.city})
    List<Spinner> spinnerViewList;
    @BindViews({R.id.first_name, R.id.last_name, R.id.email_id, R.id.mobile_number, R.id.dob, R.id.username,
            R.id.password, R.id.license_number, R.id.house_number, R.id.address, R.id.landmark, R.id.cpassword})
    List<EditText> editTextViewList;

    @BindViews({R.id.register_one_next})
    List<Button> buttonViewList;
    List<AreaListResponse> areaListResponseList;
    List<StateListResponse> stateListResponseList;
    List<CountryListResponse> countryListResponseList;
    List<CityListResponse> cityListResponseList;

    /*@SuppressLint("ValidFragment")
    public Registration_TabOne_Fragment(List<AreaListResponse> areaListResponseList, List<StateListResponse> stateListResponseList, List<CountryListResponse> countryListResponseList, List<CityListResponse> cityListResponseList) {
        this.areaListResponseList = areaListResponseList;
        this.stateListResponseList = stateListResponseList;
        this.countryListResponseList = countryListResponseList;
        this.cityListResponseList = cityListResponseList;
    }*/

    ArrayList<AreaListResponse> areaListResponseArrayList;

    private ArrayAdapter<AreaListResponse> areaSpinnerArrayAdapter;
    private ArrayAdapter<CityListResponse> citySpinnerArrayAdapter;
    private ArrayAdapter<StateListResponse> stateSpinnerArrayAdapter;
    private ArrayAdapter<CountryListResponse> countrySpinnerArrayAdapter;

    private View view;
    private List<String> genderList, areaList, cityList, stateList, countryList;
    private ArrayAdapter<String> genderSpinnerArrayAdapter;
    private Dialog calendarDialog;
    private CalendarView date_calendar;
    private DatePicker datePicker;
    private SharedPref _sharePref;
    ProgressDialog progressDialog;
    Dialog dialog, dialog2;

    TextView ok, msg;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_registration__tab_one_, container, false);
        ButterKnife.bind(this, view);
        genderList = new ArrayList<>();
        areaList = new ArrayList<>();
        cityList = new ArrayList<>();
        stateList = new ArrayList<>();
        countryList = new ArrayList<>();

        areaListResponseArrayList = new ArrayList<>();
        stateListResponseList = new ArrayList<>();
        countryListResponseList = new ArrayList<>();
        cityListResponseList = new ArrayList<>();
        areaListResponseList = new ArrayList<>();


        genderList.add("Gender");
        genderList.add("Male");
        genderList.add("Female");

        areaList.add("<-HOODI-560048->");
        areaList.add("<-INDIRANAGAR-560047->");

        cityList.add("<-BANGLORE-BANG->");
        cityList.add("<-INDORE-4520->");
        stateList.add("<-KARNATAKA-KAR->");

        countryList.add("<-INDIA-IND->");

        progressDialog = new ProgressDialog(getActivity());
        genderSpinnerArrayAdapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item,
                        genderList); //selected item will look like a spinner set from XML
        genderSpinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(0).setAdapter(genderSpinnerArrayAdapter);

        areaSpinnerArrayAdapter = new ArrayAdapter<AreaListResponse>
                (getActivity(), android.R.layout.simple_spinner_item,
                        areaListResponseArrayList);
       /* areaSpinnerArrayAdapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item,
                        are);*/ //selected item will look like a spinner set from XML
        areaSpinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(1).setAdapter(areaSpinnerArrayAdapter);

        citySpinnerArrayAdapter = new ArrayAdapter<CityListResponse>
                (getActivity(), android.R.layout.simple_spinner_item,
                        cityListResponseList); //selected item will look like a spinner set from XML
        citySpinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(4).setAdapter(citySpinnerArrayAdapter);
        stateSpinnerArrayAdapter = new ArrayAdapter<StateListResponse>
                (getActivity(), android.R.layout.simple_spinner_item,
                        stateListResponseList); //selected item will look like a spinner set from XML
        stateSpinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(2).setAdapter(stateSpinnerArrayAdapter);

        countrySpinnerArrayAdapter = new ArrayAdapter<CountryListResponse>
                (getActivity(), android.R.layout.simple_spinner_item,
                        countryListResponseList); //selected item will look like a spinner set from XML
        countrySpinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(3).setAdapter(countrySpinnerArrayAdapter);


        calendarDialog = new Dialog(getActivity());
        calendarDialog.setContentView(R.layout.open_calendar);
        datePicker = (DatePicker) calendarDialog.findViewById(R.id.calendar);
        datePicker.setMaxDate(System.currentTimeMillis());
        // date_calendar = (CalendarView) calendarDialog.findViewById(R.id.calendar);
      /*  date_calendar.
        date_calendar.setMaxDate(System.currentTimeMillis());
*/
        _sharePref = new SharedPref(getActivity());
        editTextViewList.get(4).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                calendarDialog.setTitle("Select Date");
                calendarDialog.show();
                getActivity().showDialog(999);
                return false;


            }


        });

        if (Global_Data.checkInternetConnection(getActivity())) {

            if (Build.VERSION.SDK_INT > 11) {

                new GetCountryListTask().executeOnExecutor(Global_Data.sExecutor);
            } else {

                new GetCountryListTask().execute();
            }
        } else {

            Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }

        spinnerViewList.get(2).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {

                        new GetCityListTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetCityListTask().execute();
                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerViewList.get(3).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {

                        new GetStateListTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetStateListTask().execute();
                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerViewList.get(4).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {

                        new GetAreaListTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetAreaListTask().execute();
                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            datePicker.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
                @Override
                public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
             /*       Toast.makeText(getActivity(),
                            "onDateChanged if  Year: " + year + "\n" +
                                    "Month of Year: " + monthOfYear + "\n" +
                                    "Day of Month: " + dayOfMonth , Toast.LENGTH_SHORT).show();
*/
                    try {
                        //Toast.makeText(getApplicationContext(), "check chala  ", Toast.LENGTH_SHORT).show();
                        int actual_month = monthOfYear + 1;
                        /*_userDob = dayOfMonth + "." + actual_month + "." + year;
                         */
                        String months = "", day = "";
                        if (actual_month < 10) {
                            months = "0" + actual_month;
                        } else {
                            months = "" + actual_month;
                        }
                        if (dayOfMonth < 10) {
                            day = "0" + dayOfMonth;
                        } else {
                            day = "" + dayOfMonth;
                        }

                        editTextViewList.get(4).setText(year + "-" + months + "-" + day);

                        calendarDialog.hide();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                   /* String date = "You picked the following date: "+dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
                    editTextViewList.get(4).setText(date);*/
                }
            });
        } else {

            Calendar today = Calendar.getInstance();
            datePicker.init(today.get(Calendar.YEAR),
                    today.get(Calendar.MONTH),
                    today.get(Calendar.DAY_OF_MONTH),
                    new DatePicker.OnDateChangedListener() {

                        @Override
                        public void onDateChanged(DatePicker view,
                                                  int year, int monthOfYear, int dayOfMonth) {
                   /*         Toast.makeText(getActivity(),
                                    "onDateChanged else Year: " + year + "\n" +
                                            "Month of Year: " + monthOfYear + "\n" +
                                            "Day of Month: " + dayOfMonth , Toast.LENGTH_SHORT).show();
              */
                            try {
                                //Toast.makeText(getApplicationContext(), "check chala  ", Toast.LENGTH_SHORT).show();
                                int actual_month = monthOfYear + 1;
                                /*_userDob = dayOfMonth + "." + actual_month + "." + year;
                                 */
                                String months = "", day = "";
                                if (actual_month < 10) {
                                    months = "0" + actual_month;
                                } else {
                                    months = "" + actual_month;
                                }

                                if (dayOfMonth < 10) {
                                    day = "0" + dayOfMonth;
                                } else {
                                    day = "" + dayOfMonth;
                                }

                                editTextViewList.get(4).setText(year + "-" + months + "-" + day);

                                calendarDialog.hide();


                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    });
        }


        editTextViewList.get(0).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextViewList.get(0).setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextViewList.get(1).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextViewList.get(1).setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextViewList.get(3).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextViewList.get(3).setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextViewList.get(4).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextViewList.get(4).setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextViewList.get(5).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextViewList.get(5).setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextViewList.get(6).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextViewList.get(6).setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextViewList.get(11).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextViewList.get(11).setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextViewList.get(7).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextViewList.get(7).setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextViewList.get(8).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextViewList.get(8).setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextViewList.get(9).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextViewList.get(9).setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        dialog2 = new Dialog(getActivity());
        // Include dialog.xml file
        dialog2.setContentView(R.layout.no_vehicle);
        msg = (TextView) dialog2.findViewById(R.id.message);
        msg.setText("No Internet connection, Click OK to enable internet.");
        ok = (TextView) dialog2.findViewById(R.id.ok);
        // no = (TextView) dialog.findViewById(R.id.no);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                dialog2.hide();
                Global_Data.enableWIFI(getActivity());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        dialog2.setCancelable(false);
        buttonViewList.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (editTextViewList.get(0).getText().toString().length() == 0) {
                    editTextViewList.get(0).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round_corner_whitered_bg));
                    editTextViewList.get(0).setError("Please enter first name");
                    Toast.makeText(getActivity(), "Please enter first name",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(1).getText().toString().length() == 0) {
                    editTextViewList.get(1).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round_corner_whitered_bg));
                    editTextViewList.get(1).setError("Please enter last Name");

                    Toast.makeText(getActivity(), "Please enter last Name",
                            Toast.LENGTH_SHORT).show();
                } /*else if (editTextViewList.get(2).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter email",
                            Toast.LENGTH_SHORT).show();
                }*/ else if (editTextViewList.get(3).getText().toString().length() == 0) {
                    editTextViewList.get(3).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round_corner_whitered_bg));
                    editTextViewList.get(3).setError("Please enter mobile number");

                    Toast.makeText(getActivity(), "Please enter mobile number",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(3).getText().toString().length() > 10 || editTextViewList.get(3).getText().toString().length() < 10) {
                    editTextViewList.get(10).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round_corner_whitered_bg));
                    editTextViewList.get(10).setError("Please enter 10 digit mobile number");

                    Toast.makeText(getActivity(), "Please enter 10 digit mobile number",
                            Toast.LENGTH_SHORT).show();
                } else if (spinnerViewList.get(0).getSelectedItemPosition() == 0) {
                    Toast.makeText(getActivity(), "Please select gender",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(4).getText().toString().length() == 0) {
                    editTextViewList.get(4).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round_corner_whitered_bg));
                    editTextViewList.get(4).setError("Please enter date of birth");

                    Toast.makeText(getActivity(), "Please enter date of birth",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(5).getText().toString().length() == 0) {
                    editTextViewList.get(5).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round_corner_whitered_bg));
                    editTextViewList.get(5).setError("Please enter username");

                    Toast.makeText(getActivity(), "Please enter username",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(6).getText().toString().length() == 0) {
                    editTextViewList.get(6).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round_corner_whitered_bg));
                    editTextViewList.get(6).setError("Please enter password");

                    Toast.makeText(getActivity(), "Please enter password",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(11).getText().toString().length() == 0) {
                    editTextViewList.get(11).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round_corner_whitered_bg));
                    editTextViewList.get(11).setError("Please enter confirm password");

                    Toast.makeText(getActivity(), "Please enter confirm password",
                            Toast.LENGTH_SHORT).show();
                } else if (!editTextViewList.get(11).getText().toString().equals(editTextViewList.get(6).getText().toString())) {
                    editTextViewList.get(11).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round_corner_whitered_bg));
                    editTextViewList.get(11).setError("Confirm password do not match");

                    Toast.makeText(getActivity(), "Confirm password do not match",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(7).getText().toString().length() == 0) {
                    editTextViewList.get(7).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round_corner_whitered_bg));
                    editTextViewList.get(7).setError("Please enter license number");

                    Toast.makeText(getActivity(), "Please enter license number",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(8).getText().toString().length() == 0) {
                    editTextViewList.get(8).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round_corner_whitered_bg));
                    editTextViewList.get(8).setError("Please enter house number");

                    Toast.makeText(getActivity(), "Please enter house number",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(9).getText().toString().length() == 0) {
                    editTextViewList.get(9).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round_corner_whitered_bg));
                    editTextViewList.get(9).setError("Please enter address");

                    Toast.makeText(getActivity(), "Please enter address",
                            Toast.LENGTH_SHORT).show();
                } /*else if (editTextViewList.get(10).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter landmark",
                            Toast.LENGTH_SHORT).show();
                }*/ else {

                    if (Global_Data.checkInternetConnection(getActivity())) {

                        if (Build.VERSION.SDK_INT > 11) {
                            new RegistrationTask().executeOnExecutor(Global_Data.sExecutor);
                        } else {
                            new RegistrationTask().execute();
                        }
                    } else {
                        dialog2.show();
                        Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });

        return view;
    }


    public JSONObject RegistrationFunction() {
        JSONObject mainJson = new JSONObject();
        try {

            /* @BindViews({R.id.first_name,R.id.last_name ,R.id.email_id ,R.id.mobile_number ,
            R.id.dob ,R.id.username ,
                        R.id.password ,R.id.license_number ,R.id.house_number ,R.id.address ,R.id.landmark })
                private List<EditText> editTextViewList;
          */

            mainJson.put("firstname", editTextViewList.get(0).getText().toString());
            mainJson.put("lastname", editTextViewList.get(1).getText().toString());
            mainJson.put("gender", spinnerViewList.get(0).getSelectedItem().toString());
            mainJson.put("contact_number", editTextViewList.get(3).getText().toString());
            mainJson.put("dob", editTextViewList.get(4).getText().toString());
            mainJson.put("license_number", editTextViewList.get(7).getText().toString());
            JSONObject userJson = new JSONObject();
            userJson.put("username", editTextViewList.get(5).getText().toString());
            userJson.put("role", "Vehicle_Driver");
            userJson.put("password", editTextViewList.get(6).getText().toString());
            JSONObject addressJson = new JSONObject();
            addressJson.put("house", editTextViewList.get(8).getText().toString());
            addressJson.put("address1", editTextViewList.get(9).getText().toString());
            addressJson.put("nearby", editTextViewList.get(10).getText().toString());
            // addressJson.put("nearby","");


            addressJson.put("area", spinnerViewList.get(1).getSelectedItemPosition() + 1);
            addressJson.put("city", spinnerViewList.get(4).getSelectedItemPosition() + 1);
            addressJson.put("state", spinnerViewList.get(2).getSelectedItemPosition() + 1);
            addressJson.put("country", spinnerViewList.get(3).getSelectedItemPosition() + 1);
            mainJson.put("user", userJson);
            mainJson.put("address", addressJson);
            System.out.println("string RegisterModel  " + mainJson.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJson;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        editTextViewList.get(4).setText(date);
    }


    private class RegistrationTask extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _registrationJSON;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = RegistrationFunction().toString();
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _registrationJSON = jsonParser.makeHttpRequest(Global_Data._url + "drivers/", "POST", json2);
                System.out.println(_registrationJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jo1 = null;
                if (_registrationJSON.has("user")) {
                    jo1 = new JSONObject(_registrationJSON.getString("user"));
                }
                System.out.println("if error");

                if (_registrationJSON.has("contact_number") && (_registrationJSON.getString("contact_number").toString().equals("[\"Drivers with this contact number already exists.\"]"))) {
                    // if (_registrationJSON.getString("contact_number").toString().equals("[\"Drivers with this contact number already exists.\"]")) {

                    System.out.println("ddsmdcsnncds");
                    Toast.makeText(getActivity(), "Error-  Drivers with this contact number already exists.".toString(),
                            Toast.LENGTH_SHORT).show();
                    //}
                } else if (_registrationJSON.has("user") && (jo1.getString("username").equals("[\"Users with this Username already exists.\"]"))) {
                    //  if (jo1.getString("username").equals("[\"Users with this Username already exists.\"]")) {
                    System.out.println("user");
                    Toast.makeText(getActivity(), "Error-  Users with this Username already exists.",
                            Toast.LENGTH_SHORT).show();
                    //}
                } else if (_registrationJSON.has("license_number") && _registrationJSON.getString("license_number").toString().equals("[\"Drivers with this license number already exists.\"]")) {

                    //    if (_registrationJSON.getString("license_number").toString().equals("[\"Drivers with this license number already exists.\"]")) {
                    System.out.println("license_number");

                    Toast.makeText(getActivity(), "Error-  Drivers with this license number already exists.",
                            Toast.LENGTH_SHORT).show();
                    //  }
                }  else {


                    if (_registrationJSON.has("user")) {
                        JSONObject jo2 = new JSONObject(_registrationJSON.getString("user"));
                        System.out.println("user ----   " + jo2.getString("username"));
                        _sharePref.setStringData(Global_Data.username, jo2.getString("username"));
                        _sharePref.setStringData(Global_Data.user_role, "Vehicle_Driver");
                        _sharePref.setStringData(Global_Data.id, String.valueOf(_registrationJSON.getInt("id")));
                        Intent intent = new Intent(getActivity(), OnboardingOTPVerification.class);
                        intent.putExtra("REGISTRATION_TYPE", "Vehicle_Driver");
                        intent.putExtra("contact_number", editTextViewList.get(3).getText().toString());

                        startActivity(intent);
                        getActivity().finish();

                    } else {
                        Toast.makeText(getActivity(), "Error-  " + _registrationJSON.toString(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }

    int range = 9;  // to generate a single number with this range, by default its 0..9
    int length = 6; // by default length is 4

    public int generateRandomNumber() {
        int randomNumber;

        SecureRandom secureRandom = new SecureRandom();
        String s = "";
        for (int i = 0; i < length; i++) {
            int number = secureRandom.nextInt(range);
            if (number == 0 && i == 0) { // to prevent the Zero to be the first number as then it will reduce the length of generated pin to three or even more if the second or third number came as zeros
                i = -1;
                continue;
            }
            s = s + number;
        }

        randomNumber = Integer.parseInt(s);

        return randomNumber;
    }


    private class GetAreaListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "area/", "GET", json2);
                System.out.println("Area response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                areaListResponseArrayList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (cityListResponseList.size() != 0) {
                        if (String.valueOf(jsonArray.getJSONObject(i).getInt("city")).trim().equals(cityListResponseList.get(spinnerViewList.get(4).getSelectedItemPosition()).getId().toString().trim())) {

                            areaListResponseArrayList.add(new AreaListResponse("", jsonArray.getJSONObject(i).getInt("id"),
                                    jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getString("pincode"), jsonArray.getJSONObject(i).getInt("city"), jsonArray.getJSONObject(i).getInt("state")));

                        }
                    } else {
                        areaListResponseArrayList.clear();
                        areaSpinnerArrayAdapter.notifyDataSetChanged();

                    }
                }
                areaSpinnerArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();

                if (Global_Data.checkInternetConnection(getActivity())) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new GetAreaListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {
                        new GetAreaListTask().execute();

                    }

                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }


        }
    }


    private class GetCityListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        /*ProgressDialog progressDialog = new ProgressDialog(getActivity());
         */

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                AndroidNetworking.get(Global_Data._url + "city/")

                        .setTag("test")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                // do anything with response
                                jsonArray =response;

                                try {
                                    cityListResponseList.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        if (String.valueOf(jsonArray.getJSONObject(i).getInt("state")).trim().equals(stateListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).getId().toString().trim())) {

                                            cityListResponseList.add(new CityListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                                                    jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getInt("state")));


                                        }
                                    }
                                    citySpinnerArrayAdapter.notifyDataSetChanged();
                                    if (Global_Data.checkInternetConnection(getActivity())) {
                                        if (Build.VERSION.SDK_INT > 11) {

                                            new GetAreaListTask().executeOnExecutor(Global_Data.sExecutor);
                                        } else {

                                            new GetAreaListTask().execute();
                                        }

                                    } else {

                                        Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (Global_Data.checkInternetConnection(getActivity())) {
                                        if (Build.VERSION.SDK_INT > 11) {

                                            new GetCityListTask().executeOnExecutor(Global_Data.sExecutor);

                                        } else {

                                            new GetCityListTask().execute();

                                        }

                                    } else {

                                        Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });



              /*  String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "city/", "GET", json2);
                System.out.println("Brand response---   " + jsonArray.length());
     */       } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

           /* try {
                cityListResponseList.clear();
                  for (int i = 0; i < jsonArray.length(); i++) {

                    if (String.valueOf(jsonArray.getJSONObject(i).getInt("state")).trim().equals(stateListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).getId().toString().trim())) {

                        cityListResponseList.add(new CityListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                                jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getInt("state")));


                    }
                }
                citySpinnerArrayAdapter.notifyDataSetChanged();
                if (Global_Data.checkInternetConnection(getActivity())) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetAreaListTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetAreaListTask().execute();
                    }

                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
                if (Global_Data.checkInternetConnection(getActivity())) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetCityListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {

                        new GetCityListTask().execute();

                    }

                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }*/


        }
    }

    private class GetStateListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        // ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);*/
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AndroidNetworking.get(Global_Data._url + "state/")
                        .setTag("test")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                // do anything with response
                                jsonArray =response;

                                try {
                                    stateListResponseList.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        if (String.valueOf(jsonArray.getJSONObject(i).getInt("country")).trim().equals(countryListResponseList.get(spinnerViewList.get(3).getSelectedItemPosition()).getId().toString().trim())) {

                                            stateListResponseList.add(new StateListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                                                    jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getInt("country")));


                                        }
                                    }
                                    stateSpinnerArrayAdapter.notifyDataSetChanged();
                                    if (Global_Data.checkInternetConnection(getActivity())) {
                                        if (Build.VERSION.SDK_INT > 11) {

                                            new GetCityListTask().executeOnExecutor(Global_Data.sExecutor);

                                        } else {

                                            new GetCityListTask().execute();

                                        }

                                    } else {

                                        Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                                    }


                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (Global_Data.checkInternetConnection(getActivity())) {
                                        if (Build.VERSION.SDK_INT > 11) {

                                            new GetStateListTask().executeOnExecutor(Global_Data.sExecutor);

                                        } else {

                                            new GetStateListTask().execute();

                                        }

                                    } else {

                                        Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });


              /*  String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "state/", "GET", json2);
                System.out.println("Brand response---   " + jsonArray.length());
          */  } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

           /* try {
                stateListResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {

                    if (String.valueOf(jsonArray.getJSONObject(i).getInt("country")).trim().equals(countryListResponseList.get(spinnerViewList.get(3).getSelectedItemPosition()).getId().toString().trim())) {

                        stateListResponseList.add(new StateListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                                jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getInt("country")));


                    }
                }
                stateSpinnerArrayAdapter.notifyDataSetChanged();
                if (Global_Data.checkInternetConnection(getActivity())) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetCityListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {

                        new GetCityListTask().execute();

                    }

                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }


            } catch (Exception e) {
                e.printStackTrace();
                if (Global_Data.checkInternetConnection(getActivity())) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetStateListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {

                        new GetStateListTask().execute();

                    }

                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }*/


        }
    }

    private class GetCountryListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        // ProgressDialog progressDialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
            progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AndroidNetworking.get(Global_Data._url + "countries/")
                        /*.addPathParameter("pageNumber", "0")
                        .addQueryParameter("limit", "3")
                        .addHeaders("token", "1234")*/
                        .setTag("test")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                // do anything with response
                                jsonArray =response;

                                try {
                                    countryListResponseList.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {


                                        countryListResponseList.add(new CountryListResponse(jsonArray.getJSONObject(i).getInt("id"),
                                                jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code")));

                                    }
                                    countrySpinnerArrayAdapter.notifyDataSetChanged();
                                    if (Global_Data.checkInternetConnection(getActivity())) {
                                        if (Build.VERSION.SDK_INT > 11) {

                                            new GetStateListTask().executeOnExecutor(Global_Data.sExecutor);
                                        } else {

                                            new GetStateListTask().execute();
                                        }

                                    } else {

                                        Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();

                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });




               /* String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "countries/", "GET", json2);
                System.out.println("Brand response---   " + jsonArray.length());
         */   } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

          /*  try {
                countryListResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {


                    countryListResponseList.add(new CountryListResponse(jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code")));

                }
                countrySpinnerArrayAdapter.notifyDataSetChanged();
                if (Global_Data.checkInternetConnection(getActivity())) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetStateListTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetStateListTask().execute();
                    }

                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();

            }*/


        }
    }

//............................


    private class SendSMSTask extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _sendOTPJSON;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                /*
                *  String mobileNumberOTP = String.valueOf(generateRandomNumber());
                    SmsManager smsManager = SmsManager.getDefault();

                    if (_registrationJSON.has("user")) {
                        JSONObject jo2 = new JSONObject(_registrationJSON.getString("user"));
                        System.out.println("user ----   " + jo2.getString("username"));
                        _sharePref.setStringData(Global_Data.username, jo2.getString("username"));
                        _sharePref.setStringData(Global_Data.user_role, "Vehicle_Driver");
                        _sharePref.setStringData(Global_Data.id,String.valueOf(_registrationJSON.getInt("id")));
                         smsManager.sendTextMessage(editTextViewList.get(3).getText().toString(), null, mobileNumberOTP, null, null);

                *
                * */

                String mobileNumberOTP = String.valueOf(generateRandomNumber());
                String json2 = editTextViewList.get(3).getText().toString() + "/" + mobileNumberOTP;
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _sendOTPJSON = jsonParser.makeHttpRequest("https://2factor.in/API/V1/4a668095-46be-11e8-a895-0200cd936042/SMS/" + json2, "POST", "");
                System.out.println(_sendOTPJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jo1 = null;
                if (_sendOTPJSON.has("Status")) {
                    Toast.makeText(getActivity(), "SMS sent--  " + _sendOTPJSON.getString("Status"), Toast.LENGTH_SHORT).show();


                 /*   Intent intent = new Intent(getActivity(), OnboardingOTPVerification.class);
                    intent.putExtra("REGISTRATION_TYPE", "Vehicle_Driver");
                    intent.putExtra("firstname", editTextViewList.get(0).getText().toString());
                    intent.putExtra("lastname", editTextViewList.get(1).getText().toString());
                    intent.putExtra("gender", spinnerViewList.get(0).getSelectedItem().toString());
                    intent.putExtra("contact_number", editTextViewList.get(3).getText().toString());
                    intent.putExtra("dob", editTextViewList.get(4).getText().toString());
                    intent.putExtra("license_number", editTextViewList.get(7).getText().toString());
                    intent.putExtra("username", editTextViewList.get(5).getText().toString());
                    intent.putExtra("role", "Vehicle_Owner");
                    intent.putExtra("password", editTextViewList.get(6).getText().toString());
                    intent.putExtra("house", editTextViewList.get(8).getText().toString());
                    intent.putExtra("address1", editTextViewList.get(9).getText().toString());
                    intent.putExtra("nearby", editTextViewList.get(10).getText().toString());
                    intent.putExtra("area", spinnerViewList.get(1).getSelectedItemPosition() + 1);
                    intent.putExtra("city", spinnerViewList.get(4).getSelectedItemPosition() + 1);
                    intent.putExtra("state", spinnerViewList.get(2).getSelectedItemPosition() + 1);
                    intent.putExtra("country", spinnerViewList.get(3).getSelectedItemPosition() + 1);

                    getActivity().startActivity(intent);
                    getActivity().finish();*/

                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

}