package rodearr.app.com.rodearrapp.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;
import rodearr.app.com.rodearrapp.utils.BitmapUtils;
import rodearr.app.com.rodearrapp.utils.ImageUtils;

public class UploadCampaignPhotos extends AppCompatActivity {
    SharedPref _sharedPref;
    String o_id = "", v_id = "";
    String vehicle_back_image = "", vehicle_left_image = "", vehicle_right_image = "", vehicle_tax_doc_front_image = "",
            vehicle_tax_doc_back_image = "", vehicle_number = "", camp_start_vehicle_back_image1 = "", camp_start_vehicle_left_image1 = "", camp_start_vehicle_right_image1 = "", camp_end_vehicle_back_image1 = "",
            camp_end_vehicle_left_image1 = "", camp_end_vehicle_right_image1 = "";

    private int PICK_IMAGE_REQUEST = 100;

    private int imageType = 11;

    private Uri filePath;
    public static final String TAG = "Upload Image";
    private Bitmap bitmap = null, camp_start_vehicle_back_image = null, camp_start_vehicle_left_image = null, camp_start_vehicle_right_image = null, camp_end_vehicle_back_image = null, camp_end_vehicle_left_image = null, camp_end_vehicle_right_image = null;
    @BindViews({R.id.activate_campaign, R.id.campaign_left_upload_button, R.id.campaign_right_upload_button, R.id.end_campaign_left_upload_button, R.id.end_campaign_right_upload_button, R.id.end_campaign})
    List<Button> buttonViewList;
    @BindViews({R.id.start_campaign_name, R.id.end_campaign_name,R.id.upload1,R.id.upload2})
    List<TextView> textViewList;
    @BindViews({R.id.campaign_number_plate, R.id.campaign_left_side_image, R.id.campaign_right_side_image, R.id.left, R.id.right, R.id.end_campaign_number_plate, R.id.end_left, R.id.end_right, R.id.end_campaign_left_side_image, R.id.end_campaign_right_side_image, R.id.okay_message})
    List<ImageView> imageViews;

    @BindViews({R.id.campaign_number_plate_layout, R.id.campaign_left_upload_layout, R.id.campaign_right_upload_layout, R.id.end_campaign_number_plate_layout})
    List<LinearLayout> linearLayoutsView;

    @BindViews({R.id.end_campaign_available_layout, R.id.start_campaign_available_layout, R.id.lock_image_layout, R.id.great_layout})
    List<RelativeLayout> relativeLayoutView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_campaign_photos);
        ButterKnife.bind(this);
        _sharedPref = new SharedPref(UploadCampaignPhotos.this);

        v_id = getIntent().getStringExtra("vehicle_id");
        System.out.println("UPLOAD CAMPAIGN PHOTOS---   " + v_id);
        if (Global_Data.checkInternetConnection(UploadCampaignPhotos.this)) {
            if (Build.VERSION.SDK_INT > 11) {
                new GetVehicleDetail().executeOnExecutor(Global_Data.sExecutor);
            } else {
                new GetVehicleDetail().execute();

            }

        } else {

            Toast.makeText(UploadCampaignPhotos.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }
        imageViews.get(10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UploadCampaignPhotos.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });
        buttonViewList.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (camp_start_vehicle_back_image == null) {
                    System.out.println("camp_end_vehicle_back_image==null");
                    Toast.makeText(UploadCampaignPhotos.this, "Please upload vehicle back image for starting campaign", Toast.LENGTH_SHORT).show();
                    imageViews.get(5).setBackground(ContextCompat.getDrawable(UploadCampaignPhotos.this, R.drawable.round_corner_whitered_bg));
                } else if (camp_start_vehicle_left_image == null) {
                    System.out.println("camp_end_vehicle_left_image==null");
                    Toast.makeText(UploadCampaignPhotos.this, "Please upload vehicle left image for starting campaign", Toast.LENGTH_SHORT).show();

                    imageViews.get(8).setBackground(ContextCompat.getDrawable(UploadCampaignPhotos.this, R.drawable.round_corner_whitered_bg));

                } else if (camp_start_vehicle_right_image == null) {
                    Toast.makeText(UploadCampaignPhotos.this, "Please upload vehicle right image for starting campaign", Toast.LENGTH_SHORT).show();

                    imageViews.get(9).setBackground(ContextCompat.getDrawable(UploadCampaignPhotos.this, R.drawable.round_corner_whitered_bg));

                } else {
                    if (Global_Data.checkInternetConnection(UploadCampaignPhotos.this)) {
                        if (Build.VERSION.SDK_INT > 11) {
                            new UpdateCampaignPhotos().executeOnExecutor(Global_Data.sExecutor);
                        } else {
                            new UpdateCampaignPhotos().execute();

                        }

                    } else {

                        Toast.makeText(UploadCampaignPhotos.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
        buttonViewList.get(5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (camp_end_vehicle_back_image == null) {
                    System.out.println("camp_end_vehicle_back_image==null");
                    Toast.makeText(UploadCampaignPhotos.this, "Please upload vehicle back image for ending campaign", Toast.LENGTH_SHORT).show();
                    imageViews.get(5).setBackground(ContextCompat.getDrawable(UploadCampaignPhotos.this, R.drawable.round_corner_whitered_bg));
                } else if (camp_end_vehicle_left_image == null) {
                    System.out.println("camp_end_vehicle_left_image==null");
                    Toast.makeText(UploadCampaignPhotos.this, "Please upload vehicle left image for ending campaign", Toast.LENGTH_SHORT).show();

                    imageViews.get(8).setBackground(ContextCompat.getDrawable(UploadCampaignPhotos.this, R.drawable.round_corner_whitered_bg));

                } else if (camp_end_vehicle_right_image == null) {
                    Toast.makeText(UploadCampaignPhotos.this, "Please upload vehicle right image for ending campaign", Toast.LENGTH_SHORT).show();

                    imageViews.get(9).setBackground(ContextCompat.getDrawable(UploadCampaignPhotos.this, R.drawable.round_corner_whitered_bg));

                } else {
                    if (Global_Data.checkInternetConnection(UploadCampaignPhotos.this)) {
                        if (Build.VERSION.SDK_INT > 11) {
                            new UploadCampaignEndPhotos().executeOnExecutor(Global_Data.sExecutor);
                        } else {
                            new UploadCampaignEndPhotos().execute();

                        }

                    } else {

                        Toast.makeText(UploadCampaignPhotos.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

    }

    //............................................................................................

    private class GetVehicleDetail extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleDetailJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(UploadCampaignPhotos.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleDetailJSON = jsonParser.makeHttpRequest(Global_Data._url + "vehicles/" + v_id, "GET", json2);
                System.out.println("Vehicle details------   " + _vehicleDetailJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (_vehicleDetailJSON != null) {
                    String first = "You have accepted ";
                    String s1 = _vehicleDetailJSON.getJSONObject("campaign").getJSONObject("campaign").getString("name") + " ";
                    String next = "<font color='#389BCB'>" + s1 + "</font>";
                    String last = "off campaign. Update photos of your car start earning";
                    textViewList.get(0).setText(Html.fromHtml(first + next + "" + last));

                    String first1 = "You're running ";
                    String s11 = _vehicleDetailJSON.getJSONObject("campaign").getJSONObject("campaign").getString("name") + "  ";
                    String next1 = "<font color='#389BCB'>" + s11 + "</font>";
                    String last1 = "off campaign. Update photos of your car start earning. You're doing a great job! You may upload the photos to end campaign after " + _vehicleDetailJSON.getJSONObject("campaign").getString("end_date");
                    ;
                    textViewList.get(1).setText(Html.fromHtml(first1 + next1 + "" + last1));

                    camp_start_vehicle_back_image1 = _vehicleDetailJSON.optString("camp_start_vehicle_back_image");
                    System.out.println("camp_end_vehicle_back_image_JSON----    " + camp_start_vehicle_back_image1);
                    //  if (camp_end_vehicle_back_image1 != null) {
                    boolean your_date_is_outdated;

                    String my_date = _vehicleDetailJSON.getJSONObject("campaign").getString("end_date");

                    StringBuilder sb = new StringBuilder(my_date);

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date strDate = sdf.parse(my_date);

                    if (System.currentTimeMillis() > strDate.getTime()) {
                        your_date_is_outdated = true;
                    } else {
                        your_date_is_outdated = false;
                    }

                    if (_vehicleDetailJSON.isNull("camp_start_vehicle_back_image")) {

                        relativeLayoutView.get(1).setVisibility(View.VISIBLE);
                        relativeLayoutView.get(0).setVisibility(View.GONE);
                        relativeLayoutView.get(2).setVisibility(View.GONE);
                        relativeLayoutView.get(3).setVisibility(View.GONE);

                        imageViews.get(0).setImageResource(R.drawable.campaign_number_plate);
                        imageViews.get(1).setImageResource(R.drawable.campaign_left_image);
                        imageViews.get(2).setImageResource(R.drawable.campaign_right_image);


                        textViewList.get(2).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                imageType = 1;
                                getPermissionToStorage();
                               /* Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                            }
                        });
                        buttonViewList.get(1).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                imageType = 2;
                                getPermissionToStorage();
                               /* Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                            }
                        });
                        buttonViewList.get(2).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                imageType = 3;
                                getPermissionToStorage();
                            /*    Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                        }
                        });

                        imageViews.get(3).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                imageType = 2;
                                getPermissionToStorage();
                               /* Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                            }
                        });

                        imageViews.get(4).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                imageType = 3;
                                getPermissionToStorage();
                              /*  Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                            }
                        });

                    } else {


                        camp_end_vehicle_back_image1 = _vehicleDetailJSON.optString("camp_end_vehicle_back_image");
                        if (_vehicleDetailJSON.isNull("camp_end_vehicle_back_image")) {
                            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                            String my_date1 = _vehicleDetailJSON.getJSONObject("campaign").getString("end_date");

                            if (date.equals(my_date1) || your_date_is_outdated) {
                                relativeLayoutView.get(1).setVisibility(View.GONE);
                                relativeLayoutView.get(0).setVisibility(View.VISIBLE);
                                relativeLayoutView.get(2).setVisibility(View.GONE);
                                relativeLayoutView.get(3).setVisibility(View.GONE);
                                imageViews.get(5).setImageResource(R.drawable.campaign_number_plate);
                                imageViews.get(8).setImageResource(R.drawable.campaign_left_image);
                                imageViews.get(9).setImageResource(R.drawable.campaign_right_image);


                                textViewList.get(3).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        imageType = 4;
                                        getPermissionToStorage();
                                      /*  Intent intent = new Intent();
                                        intent.setType("image/*");
                                        intent.setAction(Intent.ACTION_GET_CONTENT);
                                        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                                    }
                                });
                                buttonViewList.get(3).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        imageType = 5;
                                        getPermissionToStorage();
                                      /*  Intent intent = new Intent();
                                        intent.setType("image/*");
                                        intent.setAction(Intent.ACTION_GET_CONTENT);
                                        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                                    }
                                });
                                buttonViewList.get(4).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        imageType = 6;
                                        getPermissionToStorage();
                                       /* Intent intent = new Intent();
                                        intent.setType("image/*");
                                        intent.setAction(Intent.ACTION_GET_CONTENT);
                                        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                                    }
                                });

                                imageViews.get(6).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        imageType = 5;
                                        getPermissionToStorage();
                                       /* Intent intent = new Intent();
                                        intent.setType("image/*");
                                        intent.setAction(Intent.ACTION_GET_CONTENT);
                                        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                                    }
                                });

                                imageViews.get(7).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        imageType = 6;
                                        getPermissionToStorage();
                                       /* Intent intent = new Intent();
                                        intent.setType("image/*");
                                        intent.setAction(Intent.ACTION_GET_CONTENT);
                                        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                                    }
                                });
                            } else {
                                relativeLayoutView.get(1).setVisibility(View.VISIBLE);
                                relativeLayoutView.get(0).setVisibility(View.GONE);
                                relativeLayoutView.get(2).setVisibility(View.VISIBLE);
                                relativeLayoutView.get(3).setVisibility(View.GONE);
                                relativeLayoutView.get(1).setAlpha((float) 0.5);
                                Glide.with(UploadCampaignPhotos.this).load(_vehicleDetailJSON.getString("camp_start_vehicle_back_image"))
                                        .thumbnail(0.5f)
                                        .crossFade()
                                        //  .placeholder(R.drawable.backsideviewnp)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(imageViews.get(0));
                                Glide.with(UploadCampaignPhotos.this).load(_vehicleDetailJSON.getString("camp_start_vehicle_left_image"))
                                        .thumbnail(0.5f)
                                        .crossFade()
                                        //  .placeholder(R.drawable.backsideviewnp)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(imageViews.get(1));
                                Glide.with(UploadCampaignPhotos.this).load(_vehicleDetailJSON.getString("camp_start_vehicle_right_image"))
                                        .thumbnail(0.5f)
                                        .crossFade()
                                        //  .placeholder(R.drawable.backsideviewnp)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(imageViews.get(2));
                            }

                        } else {

                            relativeLayoutView.get(1).setVisibility(View.GONE);
                            relativeLayoutView.get(0).setVisibility(View.GONE);
                            relativeLayoutView.get(2).setVisibility(View.GONE);
                            relativeLayoutView.get(3).setVisibility(View.VISIBLE);
                        }


                    }

                }

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


    //..............................................................................................

    private class UpdateCampaignPhotos extends AsyncTask<String, String, String> {
        String message = "", id = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONArray _fetchUserProfileJsonArray;
        JSONObject _updateUserProfileOutputJSON, _updateUserProfileInput;
        ProgressDialog progressDialog = new ProgressDialog(UploadCampaignPhotos.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {


                id = _sharedPref.getStringData(Global_Data.id);
                _updateUserProfileInput = new JSONObject();

                _updateUserProfileInput.put("camp_start_vehicle_back_image", encodeToBase64(camp_start_vehicle_back_image));
                _updateUserProfileInput.put("camp_start_vehicle_left_image", encodeToBase64(camp_start_vehicle_left_image));
                _updateUserProfileInput.put("camp_start_vehicle_right_image", encodeToBase64(camp_start_vehicle_right_image));


                String json2 = _updateUserProfileInput.toString();
                System.out.println("Driver id---   " + _sharedPref.getStringData(Global_Data.driver_id) + "   //   " + _sharedPref.getStringData(Global_Data.owner_driver_id));
                JSONParser jsonParser = new JSONParser();
                System.out.println("_updateUserProfileInput  ...   " + json2);
                _updateUserProfileOutputJSON = jsonParser.makeHttpRequest(Global_Data._url + "vehicles/" + v_id + "/", "PATCH", json2);
                System.out.println("Campaign start image uploaded---   " + _updateUserProfileOutputJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {

                if (_updateUserProfileOutputJSON.has("id")) {
                    if (_updateUserProfileOutputJSON.getString("id").equals(v_id)) {

                        Intent intent = new Intent(UploadCampaignPhotos.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                    }
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    //..............................................................................................

    private class UploadCampaignEndPhotos extends AsyncTask<String, String, String> {
        String message = "", id = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONArray _fetchUserProfileJsonArray;
        JSONObject _updateUserProfileOutputJSON, _updateUserProfileInput;
        ProgressDialog progressDialog = new ProgressDialog(UploadCampaignPhotos.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {


                id = _sharedPref.getStringData(Global_Data.id);
                _updateUserProfileInput = new JSONObject();

                _updateUserProfileInput.put("camp_end_vehicle_back_image", encodeToBase64(camp_end_vehicle_back_image));
                _updateUserProfileInput.put("camp_end_vehicle_left_image", encodeToBase64(camp_end_vehicle_left_image));
                _updateUserProfileInput.put("camp_end_vehicle_right_image", encodeToBase64(camp_end_vehicle_right_image));


                String json2 = _updateUserProfileInput.toString();
                System.out.println("Driver id---   " + _sharedPref.getStringData(Global_Data.driver_id) + "   //   " + _sharedPref.getStringData(Global_Data.owner_driver_id));
                JSONParser jsonParser = new JSONParser();
                System.out.println("_updateUserProfileInput  ...   " + json2);
                _updateUserProfileOutputJSON = jsonParser.makeHttpRequest(Global_Data._url + "vehicles/" + v_id + "/", "PATCH", json2);
                System.out.println("Campaign end image uploaded---   " + _updateUserProfileOutputJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_updateUserProfileOutputJSON.has("id")) {
                    if (_updateUserProfileOutputJSON.getString("id").equals(v_id)) {

                        relativeLayoutView.get(1).setVisibility(View.GONE);
                        relativeLayoutView.get(0).setVisibility(View.GONE);
                        relativeLayoutView.get(2).setVisibility(View.GONE);
                        relativeLayoutView.get(3).setVisibility(View.VISIBLE);
                    }
                }

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    public static String encodeToBase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    /**
     * Run time permission for above marshmallow
     * if user deny the permission when install the application then system ask for permission
     * camera & storage
     */
    //  private int PICK_IMAGE_REQUEST = 2;
    public final static int PERMISSIONS_REQUEST = 1;

    @TargetApi(Build.VERSION_CODES.M)
    public void getPermissionToStorage() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            }
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA},
                    PERMISSIONS_REQUEST);
/*try {
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

    }

}catch (Exception e){
    e.printStackTrace();
}
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA},
                        PERMISSIONS_REQUEST);
            }*/

        } else {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);

        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[2])) {
            } else if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, permissions[1]) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, permissions[2]) == PackageManager.PERMISSION_GRANTED) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
            } else {
                Intent intent1 = new Intent();
                intent1.setType("image/*");
                intent1.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent1, "Select Image"), PICK_IMAGE_REQUEST);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    private String persistImage(Bitmap bitmap, String name) {
        File filesDir = getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");
        String file_String = "";
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            System.out.println("OS---   " + imageFile.getName());
            file_String = imageFile.toString();
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return file_String;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {


            filePath = data.getData();
            String selectedImagePath = BitmapUtils.getFilePathFromUri(this, data.getData());
            Log.i(TAG, " File path : " + filePath);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(UploadCampaignPhotos.this.getContentResolver(), filePath);
                System.out.println("After Compress Dimension     " + bitmap.getWidth() + "-" + bitmap.getHeight() + "    file - " + filePath + "    select--   " + selectedImagePath);

                Bitmap compressedBitmap = ImageUtils.getInstant().getCompressedBitmap(selectedImagePath);

                System.out.println("After Compress Dimension     " + compressedBitmap.getWidth() + "-" + compressedBitmap.getHeight() + "    file - " + filePath + "    select--   " + selectedImagePath);
                persistImage(compressedBitmap, "imagefile");
                if (imageType == 1) {
                    camp_start_vehicle_back_image = compressedBitmap;
                    imageViews.get(0).setImageBitmap(compressedBitmap);
                } else if (imageType == 2) {
                    camp_start_vehicle_left_image = compressedBitmap;
                    imageViews.get(1).setImageBitmap(compressedBitmap);
                } else if (imageType == 3) {
                    camp_start_vehicle_right_image = compressedBitmap;
                    imageViews.get(2).setImageBitmap(compressedBitmap);

                } else if (imageType == 4) {
                    camp_end_vehicle_back_image = compressedBitmap;
                    imageViews.get(5).setImageBitmap(compressedBitmap);

                } else if (imageType == 5) {
                    camp_end_vehicle_left_image = compressedBitmap;
                    imageViews.get(8).setImageBitmap(compressedBitmap);

                } else if (imageType == 6) {
                    camp_end_vehicle_right_image = compressedBitmap;
                    imageViews.get(9).setImageBitmap(compressedBitmap);

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
