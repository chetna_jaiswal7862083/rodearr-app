package rodearr.app.com.rodearrapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by wel come on 05-07-2018.
 */

public class VehicleDriverListResponse {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("vehicle_number")
    @Expose
    private String vehicle_number;

    public VehicleDriverListResponse(Integer id, String vehicle_number) {
        this.id = id;
        this.vehicle_number = vehicle_number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVehicle_number() {
        return vehicle_number;
    }

    public void setVehicle_number(String vehicle_number) {
        this.vehicle_number = vehicle_number;
    }

    @Override
    public String toString() {
        return vehicle_number;
    }
}
