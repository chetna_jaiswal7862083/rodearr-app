package rodearr.app.com.rodearrapp.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.activity.VehicleWithCampaign;
import rodearr.app.com.rodearrapp.adapter.CompaignsListAdapter;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.models.Advertiser;
import rodearr.app.com.rodearrapp.models.Brand;
import rodearr.app.com.rodearrapp.models.Campaign;
import rodearr.app.com.rodearrapp.models.CampaignDetailListModel;
import rodearr.app.com.rodearrapp.models.Campaign_Vehicle;
import rodearr.app.com.rodearrapp.models.Category;
import rodearr.app.com.rodearrapp.models.Category_;
import rodearr.app.com.rodearrapp.models.CityListResponse;
import rodearr.app.com.rodearrapp.models.Driver;
import rodearr.app.com.rodearrapp.models.Model;
import rodearr.app.com.rodearrapp.models.Owner;
import rodearr.app.com.rodearrapp.models.Stats;
import rodearr.app.com.rodearrapp.models.Vdtoken;
import rodearr.app.com.rodearrapp.models.Vehicle_;


public class Campaign_Owner extends Fragment {
    @BindView(R.id.campaign_rv)
    RecyclerView campaign_rv;

    @BindView(R.id.no_campaigns_layout)
    RelativeLayout no_campaigns_layout;
    @BindViews({R.id.open_campaign_photo})
    List<Button> buttonViewList;

    private View view;
    private CompaignsListAdapter compaignsListAdapter;
    List<CampaignDetailListModel> campaign_detailsList;
    List<CityListResponse> cityListResponseList;

    public Campaign_Owner() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_campaign, container, false);
        ButterKnife.bind(this, view);
        campaign_detailsList = new ArrayList<CampaignDetailListModel>();
        compaignsListAdapter = new CompaignsListAdapter(getActivity(), campaign_detailsList);
        cityListResponseList = new ArrayList<CityListResponse>();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        campaign_rv.setLayoutManager(mLayoutManager);
        campaign_rv.setItemAnimator(new DefaultItemAnimator());
        campaign_rv.setNestedScrollingEnabled(false);
        campaign_rv.setAdapter(compaignsListAdapter);
        buttonViewList.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callVehicleWithCampaign = new Intent(getActivity(), VehicleWithCampaign.class);
                startActivity(callVehicleWithCampaign);
            }
        });
        if (Global_Data.checkInternetConnection(getActivity())) {
            if (Build.VERSION.SDK_INT > 11) {

                new GetCampaignList().executeOnExecutor(Global_Data.sExecutor);
            } else {

                new GetCampaignList().execute();
            }

        } else {

            Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    //............................................................................................

    private class GetCampaignList extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONArray _vehicleListOfOwnerJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AndroidNetworking.get(Global_Data._url + "campaigndetails/")
                        /*.addPathParameter("pageNumber", "0")
                        .addQueryParameter("limit", "3")
                        .addHeaders("token", "1234")*/
                        .setTag("test")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                // do anything with response
                                _vehicleListOfOwnerJSON =response;


                                try {
                                    campaign_detailsList.clear();

                                    if (_vehicleListOfOwnerJSON != null || _vehicleListOfOwnerJSON.length() != 0) {
                                        buttonViewList.get(0).setVisibility(View.VISIBLE);
                                        no_campaigns_layout.setVisibility(View.GONE);
                                        campaign_rv.setVisibility(View.VISIBLE);
                                        for (int i = 0; i < _vehicleListOfOwnerJSON.length(); i++) {
                                            //  if (_vehicleListOfOwnerJSON.getJSONObject(i).has("vehicles")) {
                                            JSONArray array = _vehicleListOfOwnerJSON.getJSONObject(i).optJSONArray("vehicles");
                                            List<Campaign_Vehicle> campaignVehicleList = new ArrayList<Campaign_Vehicle>();

                                            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                                            String my_date1 =  _vehicleListOfOwnerJSON.getJSONObject(i).getString("end_date");

                                                    //-----------------

                                            boolean your_date_is_outdated;
                                            //    String my_date = _vehicleDetailJSON.getJSONObject("campaign").getString("end_date").replaceAll("-", "/");


                                            String my_date = _vehicleListOfOwnerJSON.getJSONObject(i).getString("end_date");

                                            StringBuilder sb = new StringBuilder(my_date);
                                            //  my_date = sb.reverse().toString();

                                            //    my_date = "19-09-2018";
                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                            // Date strDate = sdf.parse(my_date.replaceAll("-", "/"));
                                            Date strDate = sdf.parse(my_date);

                                            System.out.println("---> check out dated   "+my_date+"   =   "+strDate+"   ==   "+System.currentTimeMillis()+"   "+strDate.getTime());
                                            if (System.currentTimeMillis() > strDate.getTime()) {
                                                your_date_is_outdated = true;
                                            } else {
                                                your_date_is_outdated = false;
                                            }
                                            System.out.println("Check date status--   "+date.equals(my_date1)+"   ,   "+your_date_is_outdated);
                                            if (!your_date_is_outdated) {

                                                if (array != null) {
                                                    for (int j = 0; j < array.length(); j++) {

                                                        JSONObject obj = _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").optJSONObject("driver");

                                                        if (obj != null) {
                                                            campaignVehicleList.add(new Campaign_Vehicle(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getInt("id"), new Owner(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname")), new Vehicle_(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("dl_img_front"), new Stats(0, 0, 0, 0, 0, 0, "", "")), new Vdtoken(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("vdtoken").getString("vehicle_driver_token"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("vdtoken").getString("vehicle_driver_token"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("vdtoken").getString("vehicle_driver_token")))));


                                                        } else {
                                                            campaignVehicleList.add(new Campaign_Vehicle(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getInt("id"), new Owner(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname")), new Vehicle_(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getString("vehicle_number"), new Driver(1111, "", "", "", "", "", "", "", new Stats(0, 0, 0, 0, 0, 0, "", "")), new Vdtoken(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("vdtoken").getString("vehicle_driver_token"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("vdtoken").getString("vehicle_driver_token"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("vdtoken").getString("vehicle_driver_token")))));


                                                        }
                                                    }
                                                    //Do things with array.
                                                    campaign_detailsList.add(new CampaignDetailListModel(_vehicleListOfOwnerJSON.getJSONObject(i).getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getString("status"), new CityListResponse(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getString("code"), 0), _vehicleListOfOwnerJSON.getJSONObject(i).getString("start_date"), _vehicleListOfOwnerJSON.getJSONObject(i).getString("end_date"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("no_of_vehicles"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("min_earning"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("max_earning"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("min_km"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("max_km"), null, new Campaign(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getString("code"), new Category_(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getJSONObject("category").getString("name"))), new Advertiser(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("advertiser").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("advertiser").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("advertiser").getString("lastname")), campaignVehicleList, _vehicleListOfOwnerJSON.getJSONObject(i).getString("campaign_logo_image")));

                                                } else {
                                                    campaign_detailsList.add(new CampaignDetailListModel(_vehicleListOfOwnerJSON.getJSONObject(i).getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getString("status"), new CityListResponse(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getString("code"), 0), _vehicleListOfOwnerJSON.getJSONObject(i).getString("start_date"), _vehicleListOfOwnerJSON.getJSONObject(i).getString("end_date"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("no_of_vehicles"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("min_earning"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("max_earning"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("min_km"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("max_km"), null, new Campaign(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getString("code"), new Category_(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getJSONObject("category").getString("name"))), new Advertiser(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("advertiser").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("advertiser").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("advertiser").getString("lastname")), null, _vehicleListOfOwnerJSON.getJSONObject(i).getString("campaign_logo_image")));

                                                    //Do things with array
                                                }
                                            }
                                            System.out.println(" campaignVehicleList----    " + campaignVehicleList.size());
                                        }
                                        compaignsListAdapter.notifyDataSetChanged();
                                        System.out.println("Size of campaign list---   " + campaign_detailsList.get(1).getAdvertiser().getFirstname());
                                    } else {
                                        buttonViewList.get(0).setVisibility(View.GONE);
                                        no_campaigns_layout.setVisibility(View.VISIBLE);
                                        campaign_rv.setVisibility(View.GONE);
                                        Toast.makeText(getActivity(), _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                                    }
                                    progressDialog.dismiss();
                                } catch (Exception e) {
                                    e.printStackTrace();

                                    //  Toast.makeText(DashboardActivity.this, "Vehicles are not associated for this owner.", Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });


              /*  String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfOwnerJSON = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "campaigndetails/", "GET", json2);

                //Uncomment it when code is running properly.       _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + 2, "GET", json2);
*/
                System.out.println("Campaign_Owner list in list of ------   " + _vehicleListOfOwnerJSON);
                System.out.println("check lenght of blank array --   " + _vehicleListOfOwnerJSON.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

          /*  try {
                campaign_detailsList.clear();

                if (_vehicleListOfOwnerJSON != null || _vehicleListOfOwnerJSON.length() != 0) {
                   buttonViewList.get(0).setVisibility(View.VISIBLE);
                    no_campaigns_layout.setVisibility(View.GONE);
                    campaign_rv.setVisibility(View.VISIBLE);
                    for (int i = 0; i < _vehicleListOfOwnerJSON.length(); i++) {
                        //  if (_vehicleListOfOwnerJSON.getJSONObject(i).has("vehicles")) {
                        JSONArray array = _vehicleListOfOwnerJSON.getJSONObject(i).optJSONArray("vehicles");
                        List<Campaign_Vehicle> campaignVehicleList = new ArrayList<Campaign_Vehicle>();

                        if (array != null) {
                            for (int j = 0; j < array.length(); j++) {

                                JSONObject obj = _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").optJSONObject("driver");

                                if (obj != null) {
                                    campaignVehicleList.add(new Campaign_Vehicle(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getInt("id"), new Owner(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname")), new Vehicle_(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("driver").getString("dl_img_front"), new Stats(0, 0, 0, 0, 0, 0, "", "")), new Vdtoken(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("vdtoken").getString("vehicle_driver_token"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("vdtoken").getString("vehicle_driver_token"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("vdtoken").getString("vehicle_driver_token")))));


                                } else {
                                    campaignVehicleList.add(new Campaign_Vehicle(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getInt("id"), new Owner(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getString("firstname")), new Vehicle_(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("owner").getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getString("vehicle_number"), new Driver(1111, "", "", "", "", "", "", "", new Stats(0, 0, 0, 0, 0, 0, "", "")), new Vdtoken(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("vdtoken").getString("vehicle_driver_token"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("vdtoken").getString("vehicle_driver_token"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONArray("vehicles").getJSONObject(j).getJSONObject("vehicle").getJSONObject("vdtoken").getString("vehicle_driver_token")))));


                                }
                            }
                            //Do things with array.
                            campaign_detailsList.add(new CampaignDetailListModel(_vehicleListOfOwnerJSON.getJSONObject(i).getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getString("status"), new CityListResponse(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getString("code"), 0), _vehicleListOfOwnerJSON.getJSONObject(i).getString("start_date"), _vehicleListOfOwnerJSON.getJSONObject(i).getString("end_date"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("no_of_vehicles"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("min_earning"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("max_earning"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("min_km"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("max_km"), null, new rodearr.app.com.rodearrapp.models.Campaign(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getString("code"), new Category_(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getJSONObject("category").getString("name"))), new Advertiser(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("advertiser").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("advertiser").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("advertiser").getString("lastname")), campaignVehicleList, _vehicleListOfOwnerJSON.getJSONObject(i).getString("campaign_logo_image")));

                        } else {
                            campaign_detailsList.add(new CampaignDetailListModel(_vehicleListOfOwnerJSON.getJSONObject(i).getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getString("status"), new CityListResponse(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("city").getString("code"), 0), _vehicleListOfOwnerJSON.getJSONObject(i).getString("start_date"), _vehicleListOfOwnerJSON.getJSONObject(i).getString("end_date"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("no_of_vehicles"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("min_earning"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("max_earning"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("min_km"), _vehicleListOfOwnerJSON.getJSONObject(i).getInt("max_km"), null, new rodearr.app.com.rodearrapp.models.Campaign(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getString("name"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getString("code"), new Category_(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("campaign").getJSONObject("category").getString("name"))), new Advertiser(_vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("advertiser").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("advertiser").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject(i).getJSONObject("advertiser").getString("lastname")), null, _vehicleListOfOwnerJSON.getJSONObject(i).getString("campaign_logo_image")));

                            //Do things with array
                        }

                        System.out.println(" campaignVehicleList----    " + campaignVehicleList.size());
                    }
                    compaignsListAdapter.notifyDataSetChanged();
                    System.out.println("Size of campaign list---   " + campaign_detailsList.get(1).getAdvertiser().getFirstname());
                } else {
                    buttonViewList.get(0).setVisibility(View.GONE);
                    no_campaigns_layout.setVisibility(View.VISIBLE);
                    campaign_rv.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                }
                progressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();

                //  Toast.makeText(DashboardActivity.this, "Vehicles are not associated for this owner.", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }*/


        }
    }

}
