package rodearr.app.com.rodearrapp.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.classes.RestClient;
import rodearr.app.com.rodearrapp.fragment.Registration_TabOne_Fragment;
import rodearr.app.com.rodearrapp.fragment.Registration_TabTwo_Fragment;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.models.Address;
import rodearr.app.com.rodearrapp.models.AreaListResponse;
import rodearr.app.com.rodearrapp.models.CityListResponse;
import rodearr.app.com.rodearrapp.models.CountryListResponse;
import rodearr.app.com.rodearrapp.models.RegisterModel;
import rodearr.app.com.rodearrapp.models.RegisterModel_a;
import rodearr.app.com.rodearrapp.models.StateListResponse;
import rodearr.app.com.rodearrapp.models.User;

public class RegistrationScreen extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.lock,
            R.drawable.upload_campaign_icon
    };

    public static final int CORE_POOL_SIZE = 5;
    public static final int MAXIMUM_POOL_SIZE = 128;

    public static final BlockingQueue<Runnable> sWorkQueue = new LinkedBlockingQueue<Runnable>(10);

    public static final ThreadPoolExecutor sExecutor =
            new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, 200000, TimeUnit.SECONDS, sWorkQueue);

    @BindView(R.id.parent_bottom_layout)
    LinearLayout parent_bottom_layout;

    RestClient restClient;
    ProgressDialog progressDialog;

    List<AreaListResponse> areaListResponseList;
    List<StateListResponse> stateListResponseList;
    List<CountryListResponse> countryListResponseList;
    List<CityListResponse> cityListResponseList;
    ArrayList<AreaListResponse> areaListResponseArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_registration_screen);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        stateListResponseList = new ArrayList<>();
        countryListResponseList = new ArrayList<>();
        cityListResponseList = new ArrayList<>();
        areaListResponseList = new ArrayList<>();
        areaListResponseArrayList = new ArrayList<>();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#fafaf9"));
        setupTabIcons();
        restClient = new RestClient();

        parent_bottom_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrationScreen.this, Login.class);
                startActivity(intent);
                finish();
            }
        });
        progressDialog = new ProgressDialog(RegistrationScreen.this);



    }


/*
    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);

    }
*/


    private void setupTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("I am a Car Driver");

        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.group, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("I am the Car Owner");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.group_2, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);


    }



    public void callApi() {
        User user = new User("Tester5", "tester5", "");
        Address address = new Address("123", "mandi road", "maheshwar", 1, 1, 1, 1);
        System.out.println("string User  " + user);
        System.out.println("string Address  " + address);
        RegisterModel registerModel = new RegisterModel("Tester5Chetna", "Jaiswal", "Female", "9685535669", "2018-06-03", "ll123456", address, user);


        restClient.getService().registerUser(registerModel.toString(), new Callback<RegisterModel_a>() {
          /*  @Override
            public void success(Student student, Response response) {
                tvResult.setText("First name: " + student.first + 'n' +
                        "Last name: " + student.last + 'n' +
                        "Age; " + student.age + 'n' +
                        "Sex; " + student.sex + 'n' +
                        "Registered; " + student.registered);
            }*/

            /*@Override
            public void success(RegisterModel registerModel, Response response) {
                System.out.println(registerModel.getAddress());
            }*/

            @Override
            public void success(RegisterModel_a registerModels, Response response) {
                System.out.println("nnnn   -----   " + registerModels.getContact_number());
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(RegistrationScreen.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Registration_TabOne_Fragment(), "ONE");
        adapter.addFragment(new Registration_TabTwo_Fragment(), "TWO");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
//..........................................

/*

    private class GetCountryListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        // ProgressDialog progressDialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
           */
/* progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);*//*

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "countries/", "GET", json2);
                System.out.println("Brand response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                countryListResponseList.clear();
               */
/*//*
/ areaListResponses = null;
                areaListResponses = new AreaListResponse[jsonArray.length()];*//*

                for (int i = 0; i < jsonArray.length(); i++) {
                   */
/* areaListResponseList.add(new AreaListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getString("pincode"), jsonArray.getJSONObject(i).getInt("city"), jsonArray.getJSONObject(i).getInt("state")));
*//*


                    countryListResponseList.add(new CountryListResponse(jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code")));
                   */
/* areaListResponses[i] = new AreaListResponse();
                    areaListResponses[i].setCity(jsonArray.getJSONObject(i).getInt("city"));
                    areaListResponses[i].setCode(jsonArray.getJSONObject(i).getString("code"));
                    areaListResponses[i].setId(jsonArray.getJSONObject(i).getInt("id"));
                    areaListResponses[i].setName(jsonArray.getJSONObject(i).getString("name"));
                    areaListResponses[i].setPincode(jsonArray.getJSONObject(i).getString("pincode"));
                    areaListResponses[i].setState(jsonArray.getJSONObject(i).getInt("state"));
                    areaListResponses[i].setUrl(jsonArray.getJSONObject(i).getString("url"));*//*


                }
              //  countrySpinnerArrayAdapter.notifyDataSetChanged();
                if (Global_Data.checkInternetConnection(RegistrationScreen.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetStateListTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetStateListTask().execute();
                    }

                } else {

                    Toast.makeText(RegistrationScreen.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                // progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();

            }


        }
    }

    private class GetStateListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        // ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           */
/* progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);*//*

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "state/", "GET", json2);
                System.out.println("Brand response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                stateListResponseList.clear();
               */
/*//*
/ areaListResponses = null;
                areaListResponses = new AreaListResponse[jsonArray.length()];*//*

                for (int i = 0; i < jsonArray.length(); i++) {
                   */
/* areaListResponseList.add(new AreaListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getString("pincode"), jsonArray.getJSONObject(i).getInt("city"), jsonArray.getJSONObject(i).getInt("state")));
*//*

                  //  if (String.valueOf(jsonArray.getJSONObject(i).getInt("country")).trim().equals(countryListResponseList.get(spinnerViewList.get(3).getSelectedItemPosition()).getId().toString().trim())) {

                        stateListResponseList.add(new StateListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                                jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getInt("country")));
                   */
/* areaListResponses[i] = new AreaListResponse();
                    areaListResponses[i].setCity(jsonArray.getJSONObject(i).getInt("city"));
                    areaListResponses[i].setCode(jsonArray.getJSONObject(i).getString("code"));
                    areaListResponses[i].setId(jsonArray.getJSONObject(i).getInt("id"));
                    areaListResponses[i].setName(jsonArray.getJSONObject(i).getString("name"));
                    areaListResponses[i].setPincode(jsonArray.getJSONObject(i).getString("pincode"));
                    areaListResponses[i].setState(jsonArray.getJSONObject(i).getInt("state"));
                    areaListResponses[i].setUrl(jsonArray.getJSONObject(i).getString("url"));*//*


                    //}
                }
               // stateSpinnerArrayAdapter.notifyDataSetChanged();
                if (Global_Data.checkInternetConnection(RegistrationScreen.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetCityListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {

                        new GetCityListTask().execute();

                    }

                } else {

                    Toast.makeText(RegistrationScreen.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                //   progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                if (Global_Data.checkInternetConnection(RegistrationScreen.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetStateListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {

                        new GetStateListTask().execute();

                    }

                } else {

                    Toast.makeText(RegistrationScreen.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }


        }
    }

    private class GetCityListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        */
/*ProgressDialog progressDialog = new ProgressDialog(getActivity());
*//*


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           */
/* progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();*//*

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "city/", "GET", json2);
                System.out.println("Brand response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                cityListResponseList.clear();
               */
/*//*
/ areaListResponses = null;
                areaListResponses = new AreaListResponse[jsonArray.length()];*//*

                for (int i = 0; i < jsonArray.length(); i++) {
                   */
/* areaListResponseList.add(new AreaListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getString("pincode"), jsonArray.getJSONObject(i).getInt("city"), jsonArray.getJSONObject(i).getInt("state")));
*//*

                   // if (String.valueOf(jsonArray.getJSONObject(i).getInt("state")).trim().equals(stateListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).getId().toString().trim())) {

                        cityListResponseList.add(new CityListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                                jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getInt("state")));
                   */
/* areaListResponses[i] = new AreaListResponse();
                    areaListResponses[i].setCity(jsonArray.getJSONObject(i).getInt("city"));
                    areaListResponses[i].setCode(jsonArray.getJSONObject(i).getString("code"));
                    areaListResponses[i].setId(jsonArray.getJSONObject(i).getInt("id"));
                    areaListResponses[i].setName(jsonArray.getJSONObject(i).getString("name"));
                    areaListResponses[i].setPincode(jsonArray.getJSONObject(i).getString("pincode"));
                    areaListResponses[i].setState(jsonArray.getJSONObject(i).getInt("state"));
                    areaListResponses[i].setUrl(jsonArray.getJSONObject(i).getString("url"));*//*


                 //   }
                }
             //   citySpinnerArrayAdapter.notifyDataSetChanged();
                if (Global_Data.checkInternetConnection(RegistrationScreen.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetAreaListTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetAreaListTask().execute();
                    }

                } else {

                    Toast.makeText(RegistrationScreen.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                //   progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                if (Global_Data.checkInternetConnection(RegistrationScreen.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetCityListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {

                        new GetCityListTask().execute();

                    }

                } else {

                    Toast.makeText(RegistrationScreen.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }


        }
    }

    private class GetAreaListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "area/", "GET", json2);
                System.out.println("Brand response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                areaListResponseArrayList.clear();
               */
/*//*
/ areaListResponses = null;
                areaListResponses = new AreaListResponse[jsonArray.length()];*//*

                for (int i = 0; i < jsonArray.length(); i++) {
                   */
/* areaListResponseList.add(new AreaListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getString("pincode"), jsonArray.getJSONObject(i).getInt("city"), jsonArray.getJSONObject(i).getInt("state")));
*//*

                  //  if (String.valueOf(jsonArray.getJSONObject(i).getInt("city")).trim().equals(cityListResponseList.get(spinnerViewList.get(4).getSelectedItemPosition()).getId().toString().trim())) {

                        areaListResponseArrayList.add(new AreaListResponse("", jsonArray.getJSONObject(i).getInt("id"),
                                jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getString("pincode"), jsonArray.getJSONObject(i).getInt("city"), jsonArray.getJSONObject(i).getInt("state")));
                   */
/* areaListResponses[i] = new AreaListResponse();
                    areaListResponses[i].setCity(jsonArray.getJSONObject(i).getInt("city"));
                    areaListResponses[i].setCode(jsonArray.getJSONObject(i).getString("code"));
                    areaListResponses[i].setId(jsonArray.getJSONObject(i).getInt("id"));
                    areaListResponses[i].setName(jsonArray.getJSONObject(i).getString("name"));
                    areaListResponses[i].setPincode(jsonArray.getJSONObject(i).getString("pincode"));
                    areaListResponses[i].setState(jsonArray.getJSONObject(i).getInt("state"));
                    areaListResponses[i].setUrl(jsonArray.getJSONObject(i).getString("url"));*//*


                  //  }
                }
               // areaSpinnerArrayAdapter.notifyDataSetChanged();
               */
/* if (Global_Data.checkInternetConnection(getActivity())) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetStateListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {

                        new GetStateListTask().execute();

                    }

                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }*//*

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();

                if (Global_Data.checkInternetConnection(RegistrationScreen.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new GetAreaListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {
                        new GetAreaListTask().execute();

                    }

                } else {

                    Toast.makeText(RegistrationScreen.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }


        }
    }
*/

    public class GetNewsFeed extends AsyncTask<String, String, String> {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1 = new JSONObject();
        JSONObject jsonObject2 = new JSONObject();
        JSONObject jsonObject3 = new JSONObject();
        JSONObject jsonObject4 = new JSONObject();
        JSONObject _getnewsFeedJson = null;
        String _mId = "", _getNewsFeedStatus = "", json1 = "", json2 = "", json3 = "", json4 = "";
        ProgressDialog progressDialog = new ProgressDialog(RegistrationScreen.this);


        public GetNewsFeed() {

        }

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Loading Peaks");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

              /*  "user": {
                    "username": "",
                            "password": "",
                            "role": null
                },
                "license_number": "",
                        "address": {
                    "house": "",
                            "address1": "",
                            "nearby": "",
                            "area": null,
                            "city": null,
                            "state": null,
                            "country": null
                }

                */
                jsonObject.accumulate("firstname", "Tester6");
                jsonObject.accumulate("lastname", "Jaiswal");
                jsonObject.accumulate("gender", "Female");
                jsonObject.accumulate("contact_number", "9685536550");
                jsonObject.accumulate("dob", "2018-06-03");
                jsonObject.accumulate("license_number", "tl12345");

                //jsonObject.a


                jsonObject1.accumulate("username", "tester65");
                jsonObject1.accumulate("password", "12345687");
                jsonObject1.accumulate("role", "");
                json2 = jsonObject1.toString();
                System.out.println("json.....   2   " + json2);
                jsonObject.accumulate("user", json2);

                // jsonObject2.put("user", json2);
                jsonObject3.accumulate("house", "");
                jsonObject3.accumulate("address1", "");
                jsonObject3.accumulate("nearby", "");
                jsonObject3.accumulate("area", "");
                jsonObject3.accumulate("city", "");
                jsonObject3.accumulate("state", "");
                jsonObject3.accumulate("country", "");
                json3 = jsonObject3.toString();
                //  jsonObject4.put("address", jsonObject3);
                jsonObject.accumulate("address", json3);

                json1 = jsonObject.toString();

               /* json3=jsonObject4.toString();
                json4=json1+""+json2+""+json3;*/
                JSONParser jsonParser = new JSONParser();

                System.out.println("json....   1   " + json1);
                _getnewsFeedJson = jsonParser.makeHttpRequest("https://rdrprojectapi.herokuapp.com/api/v1/drivers/", "POST", json1);
              /*   System.out.println(" _getMyNewsfeedJson  ----> " + _globalData._url + "api/v1/webservices/getMyNewsFeedCheckinswithPagesLimit" + json2 + "  --->   " + _getnewsFeedJson);
               */ /*if (_getnewsFeedJson != null) {
                    _getNewsFeedStatus = _getnewsFeedJson.getString("status");

                }*/
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            try {

                // System.out.println("json array--   "+_getnewsFeedJson);
                JSONArray array = new JSONArray(_getnewsFeedJson);
                // System.out.println(".....   "+_getnewsFeedJson.getJSONObject("firstname"));
            } catch (JSONException e1) {
                e1.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();
            }

            progressDialog.dismiss();

            super.onPostExecute(s);
        }
    }

}
