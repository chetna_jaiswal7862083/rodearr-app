package rodearr.app.com.rodearrapp.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Category_ implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;

    public Category_(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}


