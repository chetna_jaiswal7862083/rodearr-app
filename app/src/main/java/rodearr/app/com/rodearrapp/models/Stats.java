package rodearr.app.com.rodearrapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Stats implements Serializable {


    @SerializedName("driver")
    @Expose
    private Integer driver;
    @SerializedName("daily")
    @Expose
    private Integer daily;
    @SerializedName("weekly")
    @Expose
    private Integer weekly;
    @SerializedName("monthly")
    @Expose
    private Integer monthly;
    @SerializedName("yearly")
    @Expose
    private Integer yearly;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("lattitude")
    @Expose
    private String lattitude;

    public Stats(Integer driver, Integer daily, Integer weekly, Integer monthly, Integer yearly, Integer total, String longitude, String lattitude) {
        this.driver = driver;
        this.daily = daily;
        this.weekly = weekly;
        this.monthly = monthly;
        this.yearly = yearly;
        this.total = total;
        this.longitude = longitude;
        this.lattitude = lattitude;
    }

    public Integer getDriver() {
        return driver;
    }

    public void setDriver(Integer driver) {
        this.driver = driver;
    }

    public Integer getDaily() {
        return daily;
    }

    public void setDaily(Integer daily) {
        this.daily = daily;
    }

    public Integer getWeekly() {
        return weekly;
    }

    public void setWeekly(Integer weekly) {
        this.weekly = weekly;
    }

    public Integer getMonthly() {
        return monthly;
    }

    public void setMonthly(Integer monthly) {
        this.monthly = monthly;
    }

    public Integer getYearly() {
        return yearly;
    }

    public void setYearly(Integer yearly) {
        this.yearly = yearly;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }
}
