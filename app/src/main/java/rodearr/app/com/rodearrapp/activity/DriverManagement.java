package rodearr.app.com.rodearrapp.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudrail.si.CloudRail;
import com.cloudrail.si.interfaces.SMS;
import com.cloudrail.si.services.Nexmo;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.models.Driver;
import rodearr.app.com.rodearrapp.models.Stats;
import rodearr.app.com.rodearrapp.models.Vdtoken;
import rodearr.app.com.rodearrapp.models.VehiclesDriverManagement;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;


public class DriverManagement extends AppCompatActivity {
    @BindViews({R.id.name, R.id.phone})
    List<EditText> editText;

    @BindView(R.id.add_driver_btn)
    Button add_driver_btn;
    SharedPref _sharedPref;
    SMS service;
    @BindView(R.id.vehicle_list)
    Spinner spinnerView;
    ArrayList<VehiclesDriverManagement> vehicleListResponseList;

    private ArrayAdapter<VehiclesDriverManagement> driverListResponseArrayAdapter;
    Dialog dialog;
    TextView yes, no, ok, msg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_management);
        ButterKnife.bind(this);

        _sharedPref = new SharedPref(DriverManagement.this);
        vehicleListResponseList = new ArrayList<VehiclesDriverManagement>();
        CloudRail.setAppKey("5b6ad8d170a77f7d86b19f80");

        service = new Nexmo(DriverManagement.this, "9b158af7", "PxiysJNzhPBhx2Bv");
        dialog = new Dialog(DriverManagement.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.no_vehicle);
        msg = (TextView) dialog.findViewById(R.id.message);
        ok = (TextView) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
                finish();
            }
        });
        msg.setText("This driver already associated with a vehicle please invite any other driver");
        dialog.setCancelable(false);
        if (Global_Data.checkInternetConnection(DriverManagement.this)) {
            if (Build.VERSION.SDK_INT > 11) {
                      new UserLoginTask().executeOnExecutor(Global_Data.sExecutor);
            } else {
                 new UserLoginTask().execute();
            }
        } else {

            Toast.makeText(DriverManagement.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }
        add_driver_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.get(0).getText().toString().length() == 0) {
                    editText.get(0).setBackground(ContextCompat.getDrawable(DriverManagement.this, R.drawable.round_corner_whitered_bg));
                    Toast.makeText(DriverManagement.this, "Please enter name.",
                            Toast.LENGTH_SHORT).show();
                } else if (editText.get(1).getText().toString().length() == 0) {
                    editText.get(1).setBackground(ContextCompat.getDrawable(DriverManagement.this, R.drawable.round_corner_whitered_bg));
                    Toast.makeText(DriverManagement.this, "Please enter mobile number.",
                            Toast.LENGTH_SHORT).show();
                } else if (editText.get(1).getText().toString().length() > 10 || editText.get(1).getText().toString().length() < 10) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        editText.get(1).setBackground(ContextCompat.getDrawable(DriverManagement.this, R.drawable.round_corner_whitered_bg));
                    }

                    Toast.makeText(DriverManagement.this, "Please enter 10 digit mobile number",
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (Global_Data.checkInternetConnection(DriverManagement.this)) {
                        if (Build.VERSION.SDK_INT > 11) {
                            new LoginTask().executeOnExecutor(Global_Data.sExecutor);
                            new UserLoginTask().executeOnExecutor(Global_Data.sExecutor);
                        } else {
                            new LoginTask().execute();
                            new UserLoginTask().execute();
                        }
                    } else {

                        Toast.makeText(DriverManagement.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        driverListResponseArrayAdapter = new ArrayAdapter<VehiclesDriverManagement>
                (DriverManagement.this, android.R.layout.simple_spinner_item,
                        vehicleListResponseList); //selected item will look like a spinner set from XML
        driverListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        spinnerView.setAdapter(driverListResponseArrayAdapter);
        if (Global_Data.checkInternetConnection(DriverManagement.this)) {
            if (Build.VERSION.SDK_INT > 11) {

                new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
            } else {

                new GetVehicleListOfOwner().execute();
            }

        } else {

            Toast.makeText(DriverManagement.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }


    }

    long vId = 0;

    private class LoginTask extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        JSONObject _json;
        ProgressDialog progressDialog = new ProgressDialog(DriverManagement.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _json = jsonParser.makeHttpRequest(Global_Data._url+"drivers/?contact_number=" + editText.get(1).getText().toString(), "GET", json2);
                System.out.println("contact number exist---    " + _json);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                vId = 0;
                if (_json != JSONObject.NULL) {
                    long vId = _json.getJSONObject("vehicle").getInt("id");

                    if (vId != 0) {
                        if (Global_Data.checkInternetConnection(DriverManagement.this)) {
                            if (Build.VERSION.SDK_INT > 11) {
                                new SendSMSTask1().executeOnExecutor(Global_Data.sExecutor);
                            } else {
                                new SendSMSTask1().execute();
                            }
                        } else {

                            Toast.makeText(DriverManagement.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
              /*  Toast.makeText(DriverManagement.this, "Error is-  " + e,
                        Toast.LENGTH_SHORT).show();
               */

                if (Global_Data.checkInternetConnection(DriverManagement.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new SendSMSTask1().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new SendSMSTask1().execute();
                    }
                } else {

                    Toast.makeText(DriverManagement.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }

                progressDialog.dismiss();

            }
        }
    }

    //............................................................................................

    private class GetVehicleListOfOwner extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfOwnerJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(DriverManagement.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);

                //Uncomment it when code is running properly.
                // _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + , "GET", json2);

                System.out.println("Owner's vehicle list in listof vehicles------   " + _vehicleListOfOwnerJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                vehicleListResponseList.clear();

                // JSONArray array = json.optJSONArray("vehicles");
                if (_vehicleListOfOwnerJSON != null) {
                    // for(int i=0;i<_vehicleListOfOwnerJSON.getJSONArray("vehicles").length();i++) {
                    if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                        System.out.println("Inside not null");
                        jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                        _sharedPref.setStringData(Global_Data.vehicleId, "");
                        _sharedPref.setStringData(Global_Data.vehicleId, "");

                        // vehicleList= (List<Vehicle>) jsonArray;
                        // }
                        //   if (jsonArray.length() != 0 || jsonArray != null) {
                        System.out.println("Check vehicle json array---   " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").length());


                        for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {


                            try {
                                System.out.println("value of i---->..   " + i);

                                if (_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).isNull("driver")) {

                                    System.out.println("Vehicle token---   " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("vdtoken").getString("vehicle_driver_token"));
                                    _sharedPref.setStringData(Global_Data.vdToken, _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("vdtoken").getString("vehicle_driver_token"));
                                    _sharedPref.setStringData(Global_Data.vehicleId, _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("id"));
                                    vehicleListResponseList.add(new VehiclesDriverManagement(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(111, "", "", "", "", "", "", "", new Stats(0, 0, 0, 0, 0, 0, "", "")), new Vdtoken(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("vdtoken").getString("vehicle_driver_token"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("vdtoken").getString("vehicle_driver_token"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("vdtoken").getString("vehicle_driver_token"))));

                                }

                            } catch (Exception e) {
                                System.out.println("value of i---->..   " + i);


                                e.printStackTrace();
                            }

                        }

                        driverListResponseArrayAdapter.notifyDataSetChanged();

                    } else {
                        System.out.println("Inside null");
                    }
                } else {

                    Toast.makeText(DriverManagement.this, _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                }
                //Do things with array


                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();

                //   Toast.makeText(DriverManagement.this, "Vehicles are not associated for this owner.", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }


        }
    }

//........................................

    private class SendSMSTask extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _sendOTPJSON;
        ProgressDialog progressDialog = new ProgressDialog(DriverManagement.this);
        String mobileNumberOTP = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                System.out.println("------ inside another try catch");


            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                mobileNumberOTP = vehicleListResponseList.get(spinnerView.getSelectedItemPosition()).getVdtoken().getVehicle_driver_token();


                //  mobileNumberOTP = _sharedPref.getStringData(Global_Data.vdToken);
                String json2 = editText.get(1).getText().toString() + "/" + mobileNumberOTP;
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _sendOTPJSON = jsonParser.makeHttpRequest("https://2factor.in/API/V1/4a668095-46be-11e8-a895-0200cd936042/SMS/" + json2, "POST", "");
                System.out.println(_sendOTPJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jo1 = null;
                if (_sendOTPJSON.has("Status")) {
                    //Toast.makeText(OnboardingOTPVerification.this, "SMS sent--  " + _sendOTPJSON.getString("Status"), Toast.LENGTH_SHORT).show();

                    _sharedPref.setStringData(Global_Data.mobile_number_otp, mobileNumberOTP);


                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    //............................................


    //........................................

    private class SendSMSTask1 extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _sendOTPJSON1,_sendOTPJSON;
        String mobileNumberOTP = "";
        Response response;
        String senderNumber = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONParser jsonParser = new JSONParser();
                String json2 = "", mn = "";
                if (editText.get(1).getText().toString().startsWith("+")) {
                    mn = editText.get(1).getText().toString();
                } else {
                    mn = "+" + editText.get(1).getText().toString();
                }
                JSONObject mainJson = new JSONObject();




                String q = "Download the app from%20" + "%20##" + vehicleListResponseList.get(spinnerView.getSelectedItemPosition()).getVdtoken().getVehicle_driver_token() + "##" + "&sender=Rodearr&mobile=" + mn + "&otp=" + vehicleListResponseList.get(spinnerView.getSelectedItemPosition()).getVdtoken().getVehicle_driver_token();
                System.out.println("QQQ==   " + q);
                      json2 = "";

                String p = vehicleListResponseList.get(spinnerView.getSelectedItemPosition()).getVdtoken().getVehicle_driver_token();
                System.out.println("Before--   " + p);

                System.out.println("After--   " + p);
                String msg = p + "is your one time password (OTP) for Rodearr driver-operator/owner http://bit.ly/2PLgawn";
                msg = URLEncoder.encode(msg, "UTF-8");


                senderNumber = editText.get(1).getText().toString();

                System.out.println("After--  sender number   " + p+"     //   "+msg+"    //  "+senderNumber);

                _sendOTPJSON = jsonParser.makeHttpRequest("http://api.msg91.com/api/sendotp.php?authkey=233400AIB8GlSxIoE95b7ef9cc&mobile=" + senderNumber.trim() + "&message=" + msg + "&sender=Roderr&otp=" + p, "GET", json2);


            } catch (Exception e) {
                e.printStackTrace();
                try {
                    _sendOTPJSON1 = new JSONObject(_sendOTPJSON.toString().substring(_sendOTPJSON.toString().indexOf("{"), _sendOTPJSON.toString().lastIndexOf("}") + 1));
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }


            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
              //  System.out.println("Message output---   " + _sendOTPJSON1.toString());

                System.out.println("Message output---   " + _sendOTPJSON.toString());
                if (_sendOTPJSON.getString("type").equals("success")) {

                    msg.setText("Invitaton sent to "+senderNumber);
                    dialog.show();
                }
               /* System.out.println("Message output---   " + _sendOTPJSON.toString());
                if (Global_Data.checkInternetConnection(DriverManagement.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new SendSMSTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new SendSMSTask().execute();
                    }
                } else {

                    Toast.makeText(DriverManagement.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }*/
                /*  finish();*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    //................................


    private class UserLoginTask extends AsyncTask<String, String, String> {
        String message, accessToken;
        boolean flag;
        String responce;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            HashMap<String, Object> data = new HashMap<>();
            data.put("first_name", "rahul");
            data.put("input_password", "rahul");
           /* data.put("authkey", "231742ANhtdxc9ytq15b72c31d");
            try {
                data.put("message", URLEncoder.encode("scascas  sa ##123456##", "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            data.put("sender", "OTPSMS");
            data.put("mobile", "9174737139");
            data.put("otp", "91747");
            data.put("otp_length", "6");*/

            try {
             ///   URL url = new URL("http://control.msg91.com/api/sendotp.php");
                URL url = new URL("http://www.finaljustice.in/codeigniter/web_service.php/");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                System.setProperty("http.keepAlive", "false");
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                StringBuilder result = new StringBuilder();
                boolean first = true;
                for (Map.Entry<String, Object> entry : data.entrySet()) {
                    if (first)
                        first = false;
                    else
                        result.append("&");

                    result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                    result.append("=");
                    result.append(URLEncoder.encode(String.valueOf(entry.getValue()), "UTF-8"));
                }
                writer.write(result.toString());

                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder stringBuffer = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        stringBuffer.append(line);
                    }
                    responce = stringBuffer.toString();
                } else {
                    responce = "Error In Connecting";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responce;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {

                System.out.println("Response in testing ---   " + responce);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
