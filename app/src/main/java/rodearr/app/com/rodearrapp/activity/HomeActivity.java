package rodearr.app.com.rodearrapp.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.LocationUpdaterService;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.fragment.BlankFragment;
import rodearr.app.com.rodearrapp.fragment.Campaign_Driver;
import rodearr.app.com.rodearrapp.fragment.Campaign_Owner;
import rodearr.app.com.rodearrapp.fragment.Dashboard;
import rodearr.app.com.rodearrapp.fragment.ListOfVehicles;
import rodearr.app.com.rodearrapp.fragment.ListOfVehiclesOnDashboard;
import rodearr.app.com.rodearrapp.fragment.Settings;
import rodearr.app.com.rodearrapp.fragment.VehicleSetting;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;

public class HomeActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private AppBarLayout appBarLayout;
    @BindView(R.id.frame_layout)
    FrameLayout frame_layout;
    private int[] tabIcons = {
            R.drawable.dashboard,
            R.drawable.campaigns,
            R.drawable.messages, R.drawable.faq_s, R.drawable.vehicle
    };
    private static final int TWO_MINUTES = 1000 * 60 * 2;
    public LocationManager locationManager;
    public MyLocationListener listener;
    public Location previousBestLocation = null;
    Criteria myCriteria;
    double latitude = 0.0, longitude = 0.0;
    boolean v;
    SharedPref _sharedPref;

    @BindView(R.id.setting)
    ImageView setting;
    @BindView(R.id.add)
    ImageView add;
    @BindView(R.id.logout)
    ImageView logout;

    Dialog dialog, dialog1;
    TextView yes, no, username, ok;
    private static final int PERMISSION_REQUEST_CODE = 200;

    boolean reselectTab;
    private static final String TAG = HomeActivity.class.getSimpleName();

    /**
     * Code used in requesting runtime permissions.
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;


    private boolean mAlreadyStartedService = false;

    private PopupMenu popupMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        _sharedPref = new SharedPref(HomeActivity.this);
        myCriteria = new Criteria();
        myCriteria.setAccuracy(Criteria.ACCURACY_FINE);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        listener = new MyLocationListener();

        LocationRequest mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)
                .setFastestInterval(1 * 1000);

        LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        settingsBuilder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(this)
                .checkLocationSettings(settingsBuilder.build());

        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response =
                            task.getResult(ApiException.class);
                } catch (ApiException ex) {
                    switch (ex.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                               /* try {
                                    ResolvableApiException resolvableApiException =
                                            (ResolvableApiException) ex;
                                    resolvableApiException
                                            .startResolutionForResult(HomeActivity.this,
                                                    LOCATION_SETTINGS_REQUEST);
                                } catch (IntentSender.SendIntentException e) {

                                }*/
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                            break;
                    }
                }
            }
        });
        dialog1 = new Dialog(HomeActivity.this);
        // Include dialog.xml file
        dialog1.setContentView(R.layout.location_popup);
        dialog1.setCancelable(false);
        ok = (TextView) dialog1.findViewById(R.id.ok);
        // no = (TextView) dialog.findViewById(R.id.no);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.hide();
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        });
        try {
            if (!checkPermission()) {

                requestPermission();
            }
         /*   ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            // int a = 111;
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                dialog1.hide();
            } else {
                // Toast.makeText(HomeActivity.this, "permission false", Toast.LENGTH_SHORT).show();
                dialog1.hide();
            }*/

          /*  ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
            // int a = 111;
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            }else{
                // Toast.makeText(HomeActivity.this, "permission false", Toast.LENGTH_SHORT).show();

            }*/
            LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            boolean gps_enabled = false;
            boolean network_enabled = false;

            try {
                gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch (Exception ex) {
            }

            try {
                network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } catch (Exception ex) {
            }

            if (!gps_enabled && !network_enabled) {
                // notify user
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setMessage("Please enable location of your device");
                dialog.setCancelable(false);
                dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub
                        Intent myIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                        //get gps
                    }
                });
               /* dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub

                    }
                });*/
                dialog.show();
            }
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !v) {
                //  buildAlertMessageNoGps();
               /* Intent intent1 = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
              */
                v = true;
            }

            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, listener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, listener);
            System.out.println("Inside of location service");
            try {
                if (getLastKnownLocation() != null) {
                    latitude = getLastKnownLocation().getLatitude();
                    longitude = getLastKnownLocation().getLongitude();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        username = (TextView) findViewById(R.id.top_title);


        String str = _sharedPref.getStringData(Global_Data.username).toString().trim();
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
            builder.append(cap + " ");
        }
        // TextView textView = (TextView) findViewById(R.id.textView);
        username.setText(builder.toString());

        // username.setText(_sharedPref.getStringData(Global_Data.username));
        // Create custom dialog object
        dialog = new Dialog(HomeActivity.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.exit_layout);
        yes = (TextView) dialog.findViewById(R.id.yes);
        no = (TextView) dialog.findViewById(R.id.no);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
            }
        });
        dialog.setCancelable(false);
        Intent i = new Intent(this, LocationUpdaterService.class);
        startService(i);


        viewPager = (ViewPager) findViewById(R.id.viewPagerHome);
        setupViewPager(viewPager);
        appBarLayout = (AppBarLayout) findViewById(R.id.appBar);
        tabLayout = (TabLayout) findViewById(R.id.bottomNavigation);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#389BCB"));
        tabLayout.setTabTextColors(Color.parseColor("#E1872E"), Color.parseColor("#389BCB"));

        setupTabIcons();
        int tabIconColor = ContextCompat.getColor(HomeActivity.this, R.color.colorPrimaryDark);
        int tabIconColor1 = ContextCompat.getColor(HomeActivity.this, R.color.colorAccent);
        System.out.println("view pager----    "+viewPager.getCurrentItem()+"   , "+viewPager.getAdapter().getCount());
        for (int j=0;j< viewPager.getAdapter().getCount();j++){

            if (j==viewPager.getCurrentItem()){
                tabLayout.getTabAt(viewPager.getCurrentItem()).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

            }else {
                tabLayout.getTabAt(j).getIcon().setColorFilter(tabIconColor1, PorterDuff.Mode.SRC_IN);

            }
        }


        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                viewPager.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMenu = new PopupMenu(HomeActivity.this, v);
                popupMenu.setOnDismissListener(new OnDismissListener());
                popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener());
                popupMenu.inflate(R.menu.popup_menu);
              /*  if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {

                    MenuItem item = popupMenu.getMenu().findItem(R.id.lang_android);
                    item.setVisible(false);
                }*/
                popupMenu.show();
             /*   viewPager.setVisibility(View.GONE);
                frame_layout.setVisibility(View.VISIBLE);
                tabLayout.setSelected(false);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, new Settings());
                transaction.commit();
                reselectTab = true;*/
            }
        });
        if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {
            add.setVisibility(View.GONE);
            if (Global_Data.checkInternetConnection(HomeActivity.this)) {
                if (Build.VERSION.SDK_INT > 11) {

                    new GetVehicleListOfDriver().executeOnExecutor(Global_Data.sExecutor);
                } else {

                    new GetVehicleListOfDriver().execute();
                }

            }
        } else {

            if (Global_Data.checkInternetConnection(HomeActivity.this)) {
                if (Build.VERSION.SDK_INT > 11) {

                    new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
                } else {

                    new GetVehicleListOfOwner().execute();
                }

            }
        }
        add.setVisibility(View.GONE);
        logout.setVisibility(View.GONE);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(HomeActivity.this, DriverManagement.class);
                startActivity(i2);
              /*  viewPager.setVisibility(View.GONE);
                frame_layout.setVisibility(View.VISIBLE);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, new AddVehicle());
                transaction.commit();*/
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _sharedPref.setStringData(Global_Data.username, "");
                _sharedPref.setStringData(Global_Data.token, "");
                _sharedPref.setStringData(Global_Data.user_role, "");
                _sharedPref.setStringData(Global_Data.id, "");
                _sharedPref.setStringData(Global_Data.owner_driver_id, "");
                _sharedPref.setStringData(Global_Data.ass_vehicle_id,"");
                _sharedPref.setStringData(Global_Data.ass_campaign_id,"");
                Intent intent = new Intent(HomeActivity.this, IntroScreen.class);
                startActivity(intent);
                finish();

            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setVisibility(View.VISIBLE);
                frame_layout.setVisibility(View.GONE);
                //   viewPager.setCurrentItem(0);

                System.out.println("---> Tabs---   " + tab.getPosition());

                /*   if(tab.getPosition()==1){

                 *//*
                    Intent i1= new Intent(HomeActivity.this, Login.class);
                    startActivity(i1);*//*
                }
                if(tab.getPosition()==0){

                    Intent i1= new Intent(HomeActivity.this, Login.class);
                    startActivity(i1);
                }*/
                int tabIconColor = ContextCompat.getColor(HomeActivity.this, R.color.colorPrimaryDark);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(HomeActivity.this, R.color.colorAccent);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (reselectTab) {
                    viewPager.setVisibility(View.VISIBLE);
                    frame_layout.setVisibility(View.GONE);
                    reselectTab = false;
                }
            }
        });


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {

                    if (Global_Data.checkInternetConnection(HomeActivity.this)) {
                        if (Build.VERSION.SDK_INT > 11) {
                            new SendGeoLocation().executeOnExecutor(Global_Data.sExecutor);
                        } else {
                            new SendGeoLocation().execute();
                        }

                    } else {

                        Toast.makeText(HomeActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }, 2000);
    }

    @Override
    public void onBackPressed() {

        // dialog.show();
        HomeActivity.this.moveTaskToBack(true);

    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            checkPermission();
           /* if (!checkPermission()) {

              //  requestPermission();
            }*/
           /* if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }*/
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 12000, 0, listener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 12000, 0, listener);
//            System.out.println("Inside of location service");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION, CAMERA}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    // if (locationAccepted && cameraAccepted)
                    if (locationAccepted){

                    }
                     //   Snackbar.make(findViewById(R.id.clayout), "Permission Granted, Now you can access location data.", Snackbar.LENGTH_LONG).show();
                    else {

                        Snackbar.make(findViewById(R.id.clayout), "Permission Denied, You cannot access location data.", Snackbar.LENGTH_LONG).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{ACCESS_FINE_LOCATION, CAMERA},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }


                break;
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(HomeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        //tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        // tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(2).setIcon(tabIcons[4]);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {

            adapter.addFragment(new Dashboard(), "Dashboard");

        } else {
            adapter.addFragment(new ListOfVehiclesOnDashboard(), "Dashboard");

        }

        if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {

            adapter.addFragment(new Campaign_Driver(), "Campaign");


        } else {
            adapter.addFragment(new Campaign_Owner(), "Campaign");


        }
       // adapter.addFragment(new BlankFragment(), "Messages");
        // adapter.addFragment(new BlankFragment(), "FAQ");
        if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {

            adapter.addFragment(new VehicleSetting(), "Vehicle");

        } else {
            adapter.addFragment(new ListOfVehicles(), "Vehicle");

        }
        viewPager.setAdapter(adapter);
        viewPager.getAdapter().notifyDataSetChanged();
        /* the ViewPager requires a minimum of 1 as OffscreenPageLimit */
        int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);
        adapter.notifyDataSetChanged();
        // viewPager.setOffscreenPageLimit(limit);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {

                if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {

                    fragment = new Dashboard();

                } else {
                    fragment = new ListOfVehiclesOnDashboard();
                    //fragment = new  Dashboard();

                }

            } else if (position == 1) {
                if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {

                    fragment = new Campaign_Driver();

                } else {
                    fragment = new Campaign_Owner();

                }


            } /*else if (position == 2) {
                fragment = new BlankFragment();
            }*//* else if (position == 3) {
                fragment = new BlankFragment();
            }*/ else if (position == 2) {
                if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {
                    fragment = new VehicleSetting();
                    // adapter.addFragment(new VehicleSetting(), "Vehicle");

                } else {
                    //  adapter.addFragment(new ListOfVehicles(), "Vehicle");
                    fragment = new ListOfVehicles();
                }
                // fragment = new BlankFragment();
            }
            return fragment;
            /*return mFragmentList.get(position);*/
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

 /*   public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        System.out.println("dnkjdsdsjdddddddd    " + requestCode + "  ..   " + grantResults.length);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    // dialog1.hide();
                    System.out.println("Granted");
                } else {
                    System.out.println("Granted not");

                    //    dialog1.show();

     *//*   no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
            }
        });*//*

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case 123: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // dialog1.hide();
                } else {
                    //   dialog1.show();

     *//*   no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
            }
        });*//*

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
*/

    private void buildAlertMessageNoGps() {
//        System.out.println("inside alert box");
        final AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext(), R.style.AppTheme);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }


    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    LocationManager mLocationManager;

    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return null;
            }
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {

                bestLocation = l;
            }
        }
        return bestLocation;
    }

    public class MyLocationListener implements LocationListener {

        @SuppressLint("LongLogTag")
        public void onLocationChanged(final Location loc) {

            //  Toast.makeText(getApplicationContext(), "OnChanged location....   ", Toast.LENGTH_SHORT).show();
            Log.i("**************************************", "Location changed");
            if (isBetterLocation(loc, previousBestLocation)) {
          /*      Toast.makeText(getApplicationContext(), "Accuracy....  " + loc.getAccuracy(), Toast.LENGTH_SHORT).show();
                if (loc.getAccuracy() < 8) {
          */
                latitude = loc.getLatitude();
                longitude = loc.getLongitude();
                //   Toast.makeText(getApplicationContext(), "Location changed in ----   " + latitude + "   ...   " + longitude, Toast.LENGTH_SHORT).show();
                Intent smsIntent = new Intent("location");
                smsIntent.putExtra("lat", latitude);
                smsIntent.putExtra("lng", longitude);

                LocalBroadcastManager.getInstance(HomeActivity.this).sendBroadcast(smsIntent);


                if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }


                loc.getLatitude();
                loc.getLongitude();

            }
        }

        public void onProviderDisabled(String provider) {
            Toast.makeText(getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT).show();

        }


        public void onProviderEnabled(String provider) {
            Toast.makeText(getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();

        }


        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

    }

    //...........................................

    //............................................................................................

    private class SendGeoLocation extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        JSONObject _updateUserProfileOutputJSON, _updateUserProfileInput;

        ProgressDialog progressDialog = new ProgressDialog(HomeActivity.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  progressDialog.setMessage("Please wait....");
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Calendar c = Calendar.getInstance();
                System.out.println("Current time => " + c.getTime());

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                String formattedDate = df.format(c.getTime());
                _updateUserProfileInput = new JSONObject();

                _updateUserProfileInput.put("reading_date", formattedDate.substring(0, 10));
                _updateUserProfileInput.put("reading_time", formattedDate.substring(11, 19));
                _updateUserProfileInput.put("lattitude", latitude);
                _updateUserProfileInput.put("longitude", longitude);
                _updateUserProfileInput.put("driver", _sharedPref.getStringData(Global_Data.id));
                _updateUserProfileInput.put("owner", _sharedPref.getStringData(Global_Data.ass_owner_id));
                _updateUserProfileInput.put("vehicle", _sharedPref.getStringData(Global_Data.ass_vehicle_id));
                _updateUserProfileInput.put("campaign_details", JSONObject.NULL);

                String json2 = "";
                json2 = _updateUserProfileInput.toString();
                System.out.println("SendGeoLocation  ...   " + json2);
                JSONParser jsonParser = new JSONParser();
                _updateUserProfileOutputJSON = jsonParser.makeHttpRequest(Global_Data._url + "geolocation/", "POST", json2);
                System.out.println("Geo Location---   " + _updateUserProfileOutputJSON.getString("lattitude"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            try {
                locationManager.removeUpdates(listener);
                locationManager = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (locationManager != null) {
            try {
                locationManager.removeUpdates(listener);
                locationManager = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

//.......................................


    private class OnDismissListener implements PopupMenu.OnDismissListener {

        @Override
        public void onDismiss(PopupMenu menu) {
            // TODO Auto-generated method stub

        }

    }

    private class OnMenuItemClickListener implements
            PopupMenu.OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            // TODO Auto-generated method stub

            switch (item.getItemId()) {
                case R.id.lang_java:

                    viewPager.setVisibility(View.GONE);
                    frame_layout.setVisibility(View.VISIBLE);
                    tabLayout.setSelected(false);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_layout, new Settings());
                    transaction.commit();
                    reselectTab = true;
                    return true;

              /*  case R.id.lang_android:

                    Intent i2 = new Intent(HomeActivity.this, DriverManagement.class);
                    startActivity(i2);
                    return true;*/
               /* case R.id.lang_python:
                    Toast.makeText(getApplicationContext(), "Python got clicked",
                            Toast.LENGTH_SHORT).show();
                    return true;*/
                case R.id.lang_ruby:
                    _sharedPref.setStringData(Global_Data.username, "");
                    _sharedPref.setStringData(Global_Data.token, "");
                    _sharedPref.setStringData(Global_Data.user_role, "");
                    _sharedPref.setStringData(Global_Data.id, "");
                    Intent intent = new Intent(HomeActivity.this, IntroScreen.class);
                    startActivity(intent);
                    finish();

                    return true;

            }
            return false;
        }
    }

//...........................


    //............................................................................................

    private class GetVehicleListOfOwner extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfOwnerJSON;
        JSONArray jsonArray;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);

                //Uncomment it when code is running properly.       _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + 2, "GET", json2);

                System.out.println("Owner's vehicle list in listof vehicles------   " + _vehicleListOfOwnerJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (_vehicleListOfOwnerJSON != null) {
                    // for(int i=0;i<_vehicleListOfOwnerJSON.getJSONArray("vehicles").length();i++) {
                    if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                        System.out.println("Inside not null");
                        jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                        // vehicleList= (List<Vehicle>) jsonArray;
                        // }
                        //   if (jsonArray.length() != 0 || jsonArray != null) {
                        // System.out.println("Check vehicle json array---   " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").length());


                    } else {
                        System.out.println("Inside null");
                    }
                } else {
                    viewPager.setCurrentItem(3);
                    int tabIconColor = ContextCompat.getColor(HomeActivity.this, R.color.colorPrimaryDark);
                    tabLayout.getTabAt(viewPager.getCurrentItem()).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);


                }

            } catch (Exception e) {
                e.printStackTrace();
                viewPager.setCurrentItem(3);
                int tabIconColor = ContextCompat.getColor(HomeActivity.this, R.color.colorPrimaryDark);
                tabLayout.getTabAt(viewPager.getCurrentItem()).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

            }


        }
    }

    //.......................................


    //............................................................................................

    private class GetVehicleListOfDriver extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfDriverJSON;
        JSONArray jsonArray;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfDriverJSON = jsonParser.makeHttpRequest(Global_Data._url + "drivers/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);

                //  _vehicleListOfDriverJSON = jsonParser.makeHttpRequest(Global_Data._url + "drivers/" + 1, "GET", json2);

                System.out.println("Driver's vehicle list------   " + _vehicleListOfDriverJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (!_vehicleListOfDriverJSON.isNull("vehicle")) {
                    _sharedPref.setStringData(Global_Data.ass_vehicle_id, _vehicleListOfDriverJSON.getJSONObject("vehicle").getString("id"));

                } else {


                    viewPager.setCurrentItem(3);
                    int tabIconColor = ContextCompat.getColor(HomeActivity.this, R.color.colorPrimaryDark);
                    tabLayout.getTabAt(viewPager.getCurrentItem()).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

                }

            } catch (Exception e) {
                viewPager.setCurrentItem(3);
                e.printStackTrace();

            }


        }
    }
}
