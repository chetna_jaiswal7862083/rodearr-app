package rodearr.app.com.rodearrapp.models;

import java.io.Serializable;

public class IncentivisedLocations implements Serializable{

    String city,location;
    double latitude, longitude;

    public IncentivisedLocations(String city, String location, double latitude, double longitude) {
        this.city = city;
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
