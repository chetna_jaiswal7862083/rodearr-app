package rodearr.app.com.rodearrapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by wel come on 25-07-2018.
 */

public class GetAllModels {


    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("brand")
    @Expose
    private GetBrandsModel brand;

    @SerializedName("category")
    @Expose
    private GetCategoryModel category;

    public GetAllModels(Integer id, String name, GetBrandsModel brand, GetCategoryModel category) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GetBrandsModel getBrand() {
        return brand;
    }

    public void setBrand(GetBrandsModel brand) {
        this.brand = brand;
    }

    public GetCategoryModel getCategory() {
        return category;
    }

    public void setCategory(GetCategoryModel category) {
        this.category = category;
    }
}
