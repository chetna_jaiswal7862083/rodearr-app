package rodearr.app.com.rodearrapp.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.jacksonandroidnetworking.JacksonParserFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;

public class Login extends AppCompatActivity {
    @BindViews({R.id.uname, R.id.password})
    List<EditText> editText;

    @BindView(R.id.login_btn)
    Button login_btn;

    @BindView(R.id.forgot_pass_text)
    TextView forgot_pass_text;

    String uname, password;
    SharedPref _sharedPref;
    Dialog dialog;
    TextView ok, msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        _sharedPref = new SharedPref(Login.this);
        // Then set the JacksonParserFactory like below
        AndroidNetworking.setParserFactory(new JacksonParserFactory());

        editText.get(0).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editText.get(0).setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editText.get(1).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editText.get(1).setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialog = new Dialog(Login.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.no_vehicle);
        msg = (TextView) dialog.findViewById(R.id.message);
        msg.setText("No Internet connection, Click OK to enable internet.");
        ok = (TextView) dialog.findViewById(R.id.ok);
        // no = (TextView) dialog.findViewById(R.id.no);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog.hide();
                    Global_Data.enableWIFI(Login.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        dialog.setCancelable(false);

        if (!Global_Data.checkInternetConnection(Login.this)) {
            dialog.show();
        }
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (editText.get(0).getText().toString().length() == 0) {
                    editText.get(0).setBackground(ContextCompat.getDrawable(Login.this, R.drawable.round_corner_whitered_bg));

                    editText.get(0).setError("Invalid Username");
                    Toast.makeText(Login.this, "Invalid Username",
                            Toast.LENGTH_SHORT).show();
                } else if (editText.get(1).getText().toString().length() == 0) {
                    editText.get(1).setBackground(ContextCompat.getDrawable(Login.this, R.drawable.round_corner_whitered_bg));
                    editText.get(1).setError("Invalid Password");
                    Toast.makeText(Login.this, "Invalid Password",
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (Global_Data.checkInternetConnection(Login.this)) {
                        if (Build.VERSION.SDK_INT > 11) {
                            new LoginTask().executeOnExecutor(Global_Data.sExecutor);
                        } else {
                            new LoginTask().execute();
                        }
                    } else {
                        dialog.show();
                        Toast.makeText(Login.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        forgot_pass_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, ForgotPassword.class);
                startActivity(intent);
                finish();
            }
        });

    }

    public void setMobileDataState(boolean mobileDataEnabled) {
        try {
            TelephonyManager telephonyService = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            Method setMobileDataEnabledMethod = telephonyService.getClass().getDeclaredMethod("setDataEnabled", boolean.class);

            if (null != setMobileDataEnabledMethod) {
                setMobileDataEnabledMethod.invoke(telephonyService, mobileDataEnabled);
            }
        } catch (Exception ex) {
            Log.e("EROR-----", "Error setting mobile data state", ex);
        }
    }

    private class LoginTask extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        JSONObject _loginJson;
        ProgressDialog progressDialog = new ProgressDialog(Login.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

             /*   AndroidNetworking.post(Global_Data._url + "login/")
                        .addBodyParameter("username", editText.get(0).getText().toString())
                        .addBodyParameter("password", editText.get(1).getText().toString())
                        .setTag("test")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // do anything with response
                                System.out.println("Login Json---   "+response);
                                _loginJson=response;

                                try {
                                    if (_loginJson.has("username") && _loginJson.has("token")) {
                                        _sharedPref.setStringData(Global_Data.username, _loginJson.getString("username"));
                                        _sharedPref.setStringData(Global_Data.token, _loginJson.getString("token"));
                                        _sharedPref.setStringData(Global_Data.user_role, _loginJson.getString("role"));
                                        _sharedPref.setStringData(Global_Data.id, _loginJson.getString("id"));
                                        if (_loginJson.has("selfdriverId")) {

                                            _sharedPref.setStringData(Global_Data.owner_driver_id, _loginJson.getString("selfdriverId"));
                                        } else {
                                            _sharedPref.setStringData(Global_Data.owner_driver_id, "");

                                        }
                                        if (_sharedPref.getStringData(Global_Data.user_role).equalsIgnoreCase("Vehicle_Driver")) {
                                            if (Global_Data.checkInternetConnection(Login.this)) {
                                                if (Build.VERSION.SDK_INT > 11) {
                                                    new GetDriverDetails().executeOnExecutor(Global_Data.sExecutor);
                                                } else {
                                                    new GetDriverDetails().execute();
                                                }
                                            } else {
                                                //  Toast.makeText(Login.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();

                                                Snackbar.make(findViewById(R.id.clogin_layout), "Please check internet connection.", Snackbar.LENGTH_LONG).show();

                                            }
                                        }


                                        if(!_sharedPref.getStringData(Global_Data.owner_driver_id).equals("")){

                                            if (Global_Data.checkInternetConnection(Login.this)) {
                                                if (Build.VERSION.SDK_INT > 11) {
                                                    new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
                                                } else {
                                                    new GetVehicleListOfOwner().execute();
                                                }
                                            } else {
                                                //  Toast.makeText(Login.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();

                                                Snackbar.make(findViewById(R.id.clogin_layout), "Please check internet connection.", Snackbar.LENGTH_LONG).show();

                                            }
                                        }
                                        Intent intent = new Intent(Login.this, HomeActivity.class);
                                        startActivity(intent);
                                        finish();

                                    } else {
                                        if (_loginJson.getJSONArray("non_field_errors").get(0).equals("Incorrect credentials. Please try again.")) {

                                            System.out.println("checl incorrection---   " + _loginJson.getJSONArray("non_field_errors").get(0));
                                            Toast.makeText(Login.this, "" + _loginJson.getJSONArray("non_field_errors").get(0),
                                                    Toast.LENGTH_SHORT).show();
                                        } else if (_loginJson.getJSONArray("non_field_errors").get(0).equals("This username is not valid.")) {
                                            Toast.makeText(Login.this, "This username is not valid.",
                                                    Toast.LENGTH_SHORT).show();
                                        }


                                    }


                                    progressDialog.dismiss();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(Login.this, "Error is-  " + e,
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                                System.out.println("Login JsonANError error---   "+error.getErrorBody());
                            }
                        });*/
                String json2 = generateLoginjson().toString();
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _loginJson = jsonParser.makeHttpRequest(Global_Data._url + "login/", "POST", json2);
                System.out.println("Login response----   " + _loginJson);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_loginJson.has("username") && _loginJson.has("token")) {
                    _sharedPref.setStringData(Global_Data.username, _loginJson.getString("username"));
                    _sharedPref.setStringData(Global_Data.token, _loginJson.getString("token"));
                    _sharedPref.setStringData(Global_Data.user_role, _loginJson.getString("role"));
                    _sharedPref.setStringData(Global_Data.id, _loginJson.getString("id"));
                    if (_loginJson.has("selfdriverId")) {

                        _sharedPref.setStringData(Global_Data.owner_driver_id, _loginJson.getString("selfdriverId"));
                    } else {
                        _sharedPref.setStringData(Global_Data.owner_driver_id, "");

                    }
                    if (_sharedPref.getStringData(Global_Data.user_role).equalsIgnoreCase("Vehicle_Driver")) {
                        if (Global_Data.checkInternetConnection(Login.this)) {
                            if (Build.VERSION.SDK_INT > 11) {
                                new GetDriverDetails().executeOnExecutor(Global_Data.sExecutor);
                            } else {
                                new GetDriverDetails().execute();
                            }
                        } else {
                            //  Toast.makeText(Login.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();

                            Snackbar.make(findViewById(R.id.clogin_layout), "Please check internet connection.", Snackbar.LENGTH_LONG).show();

                        }
                    }


                    if (!_sharedPref.getStringData(Global_Data.owner_driver_id).equals("")) {

                        if (Global_Data.checkInternetConnection(Login.this)) {
                            if (Build.VERSION.SDK_INT > 11) {
                                new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
                            } else {
                                new GetVehicleListOfOwner().execute();
                            }
                        } else {
                            //  Toast.makeText(Login.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();

                            Snackbar.make(findViewById(R.id.clogin_layout), "Please check internet connection.", Snackbar.LENGTH_LONG).show();

                        }
                    }
                    Intent intent = new Intent(Login.this, HomeActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    if (_loginJson.getJSONArray("non_field_errors").get(0).equals("Incorrect credentials. Please try again.")) {

                        System.out.println("checl incorrection---   " + _loginJson.getJSONArray("non_field_errors").get(0));
                        Toast.makeText(Login.this, "" + _loginJson.getJSONArray("non_field_errors").get(0),
                                Toast.LENGTH_SHORT).show();
                    } else if (_loginJson.getJSONArray("non_field_errors").get(0).equals("This username is not valid.")) {
                        Toast.makeText(Login.this, "This username is not valid.",
                                Toast.LENGTH_SHORT).show();
                    }


                }


                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(Login.this, "Error is-  " + e,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }


    public JSONObject generateLoginjson() {
        JSONObject mainJson = new JSONObject();
        try {
            mainJson.put("username", editText.get(0).getText().toString());
            mainJson.put("password", editText.get(1).getText().toString());

            System.out.println("string LoginModel  " + mainJson.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJson;
    }


    //................................................................


    private class GetDriverDetails extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        JSONObject _driverJson;
        ProgressDialog progressDialog = new ProgressDialog(Login.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = generateLoginjson().toString();
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _driverJson = jsonParser.makeHttpRequest(Global_Data._url + "drivers/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);
                System.out.println("In login    " + _driverJson);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_driverJson.has("owner") && _driverJson.getString("owner").equals(null)) {
                    System.out.println("if if if");
                    _sharedPref.setStringData(Global_Data.ass_owner_id, "null");

                } else {
                    System.out.println("else else else");
                    _sharedPref.setStringData(Global_Data.ass_owner_id, _driverJson.getJSONObject("owner").getString("id"));
                    System.out.println("check what persist----   " + _sharedPref.getStringData(Global_Data.ass_owner_id));
                }
                _sharedPref.setStringData(Global_Data.ass_vehicle_id, _driverJson.getJSONObject("vehicle").getString("id"));


                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(Login.this, "Error is-  " + e,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
//..................................

    private class GetVehicleListOfOwner extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfOwnerJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(Login.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            //  progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);

                //Uncomment it when code is running properly.       _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + 2, "GET", json2);

                System.out.println("Owner's vehicle list in listof vehicles------   " + _vehicleListOfOwnerJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (_vehicleListOfOwnerJSON != null) {
                    if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                        System.out.println("Inside not null");
                        jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");

                        for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {
                            if (_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).has("driver")) {

                                JSONObject dataObject = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).optJSONObject("driver");

                                if (dataObject != null) {
                                    System.out.println("dataObject ----->>>>     " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id") + "   -   " + _sharedPref.getStringData(Global_Data.owner_driver_id));
                                    if (String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id")).equals(_sharedPref.getStringData(Global_Data.owner_driver_id))) {
                                        _sharedPref.setStringData(Global_Data.ass_vehicle_id, String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id")));
                                        System.out.println("dataObject ----->>>>inside     " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"));
                                    }   //Do things with object.

                                }
                            }


                        }
                        _sharedPref.setStringData(Global_Data.ass_owner_id, String.valueOf(_vehicleListOfOwnerJSON.getInt("id")));

                    }
                } else {


                }


            } catch (Exception e) {
                e.printStackTrace();


            }


        }
    }

}
