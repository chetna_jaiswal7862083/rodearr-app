package rodearr.app.com.rodearrapp.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.adapter.VehicleListCampaignAssAdapter;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.models.Brand;
import rodearr.app.com.rodearrapp.models.Category;
import rodearr.app.com.rodearrapp.models.Driver;
import rodearr.app.com.rodearrapp.models.Model;
import rodearr.app.com.rodearrapp.models.Stats;
import rodearr.app.com.rodearrapp.models.VehicleALL;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;

public class Vehicle_Campaign_Association extends AppCompatActivity {
    private RecyclerView campaign_vehicle_LV;
    ArrayList<VehicleALL> vehicleListResponseList;
    @BindView(R.id.no_record_layout)
    LinearLayout no_record_layout;

    @BindView(R.id.great_layout)
    RelativeLayout great_layout;

    Button associate;
    public static int selectedVehicleId = -1;
    SharedPref _sharedPref;
    VehicleListCampaignAssAdapter vehicleListCampaignAssAdapter;
    String owner_id = "", vehicle_id = "0", driver_id = "";
    String advertiser, campaign, campaign_per_city, owner, vehicle;
    Bundle bundle;

    @BindViews({R.id.go_back, R.id.okay_message})
    List<ImageView> imageViewList;

    public static List<Integer> vehicleIds;
    @BindViews({R.id.top_title})
    List<TextView> textViewList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle__campaign__association);
        ButterKnife.bind(this);

        _sharedPref = new SharedPref(Vehicle_Campaign_Association.this);
        associate = (Button) findViewById(R.id.associate);

        campaign_vehicle_LV = (RecyclerView) findViewById(R.id.campaign_vehicle_LV);
        vehicleListResponseList = new ArrayList<VehicleALL>();
        bundle = getIntent().getExtras();
        advertiser = getIntent().getStringExtra("advertiser");
        campaign = getIntent().getStringExtra("campaign");
        campaign_per_city = getIntent().getStringExtra("campaign_per_city");

        String str = _sharedPref.getStringData(Global_Data.username).toString().trim();
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
            builder.append(cap + " ");
        }
        textViewList.get(0).setText(builder.toString());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Vehicle_Campaign_Association.this);
        campaign_vehicle_LV.setLayoutManager(mLayoutManager);
        campaign_vehicle_LV.setItemAnimator(new DefaultItemAnimator());
        campaign_vehicle_LV.setNestedScrollingEnabled(false);
        campaign_vehicle_LV.setAdapter(vehicleListCampaignAssAdapter);
        campaign_vehicle_LV.setNestedScrollingEnabled(false);
        associate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("ssss----ssss    " + advertiser);
                if (vehicleIds.size() == 0) {

                    Toast.makeText(Vehicle_Campaign_Association.this, "Please select any vehicle to associate", Toast.LENGTH_SHORT).show();
                } else {
                    if (Global_Data.checkInternetConnection(Vehicle_Campaign_Association.this)) {
                        if (Build.VERSION.SDK_INT > 11) {

                            new AssociateVehicleCampaign().executeOnExecutor(Global_Data.sExecutor);
                        } else {

                            new AssociateVehicleCampaign().execute();
                        }

                    } else {

                        Toast.makeText(Vehicle_Campaign_Association.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        imageViewList.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imageViewList.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Vehicle_Campaign_Association.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });

        vehicleIds = new ArrayList<Integer>();
    }


    //............................................................................................
    boolean isStatsAvailable;

    private class GetVehicleListOfOwner extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfOwnerJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(Vehicle_Campaign_Association.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                AndroidNetworking.get(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id))
                        /*    .addBodyParameter("firstname", "Amit")
                            .addBodyParameter("lastname", "Shekhar")
                            .setTag("test")*/
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // do anything with response
                                _vehicleListOfOwnerJSON = response;

                                System.out.println("Owner's vehicle list in Vehicle campaign association------   " + _vehicleListOfOwnerJSON);

                                try {
                                    vehicleListResponseList.clear();
                                    if (_vehicleListOfOwnerJSON != null) {
                                        // for(int i=0;i<_vehicleListOfOwnerJSON.getJSONArray("vehicles").length();i++) {
                                        if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                                            System.out.println("Inside not null");
                                            jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                                            if (jsonArray.length() > 0) {
                                                for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {


                                                    try {
                                                        System.out.println("value of i---->..   " + i);
                                                        JSONObject driverObj = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).optJSONObject("driver");
                                                        JSONObject campaignObj = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).optJSONObject("campaign");
                                                        if (driverObj != null && campaignObj == null) {
                                                            if (_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").has("stats")) {

                                                                JSONObject dataObject = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").optJSONObject("stats");

                                                                if (dataObject != null) {

                                                                    //Do things with object.
                                                                    isStatsAvailable = true;
                                                                } else {

                                                                    isStatsAvailable = false;
                                                                    //Do things with array
                                                                }
                                                            }


                                                            if (isStatsAvailable) {
                                                                vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getJSONObject("location").getString("longitude"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getJSONObject("location").getString("lattitude")))));

                                                            } else {
                                                                vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                                            }
                                                        } else {


                                                        }


                                                    } catch (Exception e) {
                                                        System.out.println("value of i---->..   " + i);

                                                        //No driver   vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(111, "", "", "", "", "", "", "", new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                                        e.printStackTrace();
                                                    }

                                                }

                                                vehicle_id = String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(0).getInt("id"));
                                                driver_id = vehicle_id;
                                                //   vehicleListResponseArrayAdapter.notifyDataSetChanged();
                                                campaign_vehicle_LV.setVisibility(View.VISIBLE);
                                                associate.setVisibility(View.VISIBLE);
                                                vehicleListCampaignAssAdapter = new VehicleListCampaignAssAdapter(vehicleListResponseList, selectedVehicleId, Vehicle_Campaign_Association.this);

                                                campaign_vehicle_LV.setAdapter(vehicleListCampaignAssAdapter);
                                                if (vehicleListResponseList.size() == 0) {
                                                    no_record_layout.setVisibility(View.VISIBLE);
                                                    campaign_vehicle_LV.setVisibility(View.GONE);
                                                    associate.setVisibility(View.GONE);
                                                }

                                            } else {
                                                no_record_layout.setVisibility(View.VISIBLE);
                                                campaign_vehicle_LV.setVisibility(View.GONE);
                                                associate.setVisibility(View.GONE);
                                            }
                       /* vehicleListAdapter = new VehicleListAdapter(vehicleListResponseList, getActivity());
                        vehicleListAdapter.notifyDataSetChanged();*/
                                        } else {
                                            System.out.println("Inside null");
                                        }
                                    } else {

                                        Toast.makeText(Vehicle_Campaign_Association.this, _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                                    }
                                    progressDialog.dismiss();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    no_record_layout.setVisibility(View.VISIBLE);
                                    campaign_vehicle_LV.setVisibility(View.GONE);

                                    //    Toast.makeText(getActivity(), "Vehicles are not associated for this owner.", Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();

                                }
                            }

                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });

            /*    String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);

                //Uncomment it when code is running properly.       _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + 2, "GET", json2);

                System.out.println("Owner's vehicle list in Vehicle campaign association------   " + _vehicleListOfOwnerJSON);
          */
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

           /* try {
                vehicleListResponseList.clear();
                if (_vehicleListOfOwnerJSON != null) {
                    // for(int i=0;i<_vehicleListOfOwnerJSON.getJSONArray("vehicles").length();i++) {
                    if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                        System.out.println("Inside not null");
                        jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {


                                try {
                                    System.out.println("value of i---->..   " + i);
                                    JSONObject driverObj = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).optJSONObject("driver");
                                    JSONObject campaignObj=_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).optJSONObject("campaign");
                                    if (driverObj != null && campaignObj==null) {
                                        if (_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").has("stats")) {

                                            JSONObject dataObject = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").optJSONObject("stats");

                                            if (dataObject != null) {

                                                //Do things with object.
                                                isStatsAvailable = true;
                                            } else {

                                                isStatsAvailable = false;
                                                //Do things with array
                                            }
                                        }


                                        if (isStatsAvailable) {
                                            vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("longitude"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("lattitude")))));

                                        } else {
                                            vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                        }
                                    } else {


                                    }


                                } catch (Exception e) {
                                    System.out.println("value of i---->..   " + i);

                                    //No driver   vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(111, "", "", "", "", "", "", "", new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                    e.printStackTrace();
                                }

                            }

                            vehicle_id = String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(0).getInt("id"));
                            driver_id = vehicle_id;
                            //   vehicleListResponseArrayAdapter.notifyDataSetChanged();
                            campaign_vehicle_LV.setVisibility(View.VISIBLE);
                            associate.setVisibility(View.VISIBLE);
                            vehicleListCampaignAssAdapter = new VehicleListCampaignAssAdapter(vehicleListResponseList, selectedVehicleId, Vehicle_Campaign_Association.this);

                            campaign_vehicle_LV.setAdapter(vehicleListCampaignAssAdapter);
                            if (vehicleListResponseList.size() == 0) {
                                no_record_layout.setVisibility(View.VISIBLE);
                                campaign_vehicle_LV.setVisibility(View.GONE);
                                associate.setVisibility(View.GONE);
                            }

                        } else {
                            no_record_layout.setVisibility(View.VISIBLE);
                            campaign_vehicle_LV.setVisibility(View.GONE);
                            associate.setVisibility(View.GONE);
                        }
                       *//* vehicleListAdapter = new VehicleListAdapter(vehicleListResponseList, getActivity());
                        vehicleListAdapter.notifyDataSetChanged();*//*
                    } else {
                        System.out.println("Inside null");
                    }
                } else {

                    Toast.makeText(Vehicle_Campaign_Association.this, _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                no_record_layout.setVisibility(View.VISIBLE);
                campaign_vehicle_LV.setVisibility(View.GONE);

                //    Toast.makeText(getActivity(), "Vehicles are not associated for this owner.", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }*/


        }
    }

    @Override
    public void onResume() {
        super.onResume();

        no_record_layout.setVisibility(View.GONE);
        if (Global_Data.checkInternetConnection(Vehicle_Campaign_Association.this)) {
            if (Build.VERSION.SDK_INT > 11) {

                new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
            } else {

                new GetVehicleListOfOwner().execute();
            }

        } else {

            Toast.makeText(Vehicle_Campaign_Association.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }
    }


    //..............................................................................................

    private class AssociateVehicleCampaign extends AsyncTask<String, String, String> {
        String message = "", id = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONArray jsonArray;
        JSONArray _fetchUserProfileJsonArray, _updateUserProfileOutputJSON;
        JSONObject _updateUserProfileInput;
        ProgressDialog progressDialog = new ProgressDialog(Vehicle_Campaign_Association.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                id = _sharedPref.getStringData(Global_Data.id);

                jsonArray = new JSONArray();
                //  _updateUserProfileInput.put("id", 1);
              /*  _updateUserProfileInput.put("advertiser", Integer.parseInt(advertiser));

                _updateUserProfileInput.put("campaign", Integer.parseInt(campaign));
                _updateUserProfileInput.put("campaign_per_city", Integer.parseInt(campaign_per_city));

                //      _updateUserProfileInput.put("campaign_per_city", 2);

                _updateUserProfileInput.put("owner", Integer.parseInt(_sharedPref.getStringData(Global_Data.id)));
                _updateUserProfileInput.put("vehicle", selectedVehicleId);
*/
                for (int i = 0; i < vehicleIds.size(); i++) {
                    _updateUserProfileInput = new JSONObject();
                    _updateUserProfileInput.put("advertiser", Integer.parseInt(advertiser));

                    _updateUserProfileInput.put("campaign", Integer.parseInt(campaign));
                    _updateUserProfileInput.put("campaign_per_city", Integer.parseInt(campaign_per_city));
                    _updateUserProfileInput.put("owner", Integer.parseInt(_sharedPref.getStringData(Global_Data.id)));
                    _updateUserProfileInput.put("vehicle", vehicleIds.get(i));
                    System.out.println("campaign/vehicles/---   value of vehicle id    " + vehicleIds.get(i));
                    jsonArray.put(_updateUserProfileInput);
                }


                String json2 = jsonArray.toString();
                JSONParser jsonParser = new JSONParser();
                System.out.println("campaign/vehicles/---   Input  ...   " + json2);
                _updateUserProfileOutputJSON = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "campaign/vehicles/", "POST", json2);
                System.out.println("campaign/vehicles/---      " + _updateUserProfileOutputJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
           try {
                if (_updateUserProfileOutputJSON.length() != 0) {
                    if (_updateUserProfileOutputJSON.getJSONObject(0).has("id")) {
                        no_record_layout.setVisibility(View.GONE);
                        campaign_vehicle_LV.setVisibility(View.GONE);
                        associate.setVisibility(View.GONE);
                        great_layout.setVisibility(View.VISIBLE);
                        selectedVehicleId = -1;
                    }
                } else {

                    Toast.makeText(Vehicle_Campaign_Association.this, "Failed to associate.", Toast.LENGTH_SHORT).show();

                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }

            progressDialog.dismiss();
        }


    }
}
