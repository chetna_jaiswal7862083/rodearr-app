package rodearr.app.com.rodearrapp.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.classes.WorkaroundMapFragment;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.models.Brand;
import rodearr.app.com.rodearrapp.models.Category;
import rodearr.app.com.rodearrapp.models.Driver;
import rodearr.app.com.rodearrapp.models.Model;
import rodearr.app.com.rodearrapp.models.Stats;
import rodearr.app.com.rodearrapp.models.VehicleALL;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;


public class Dashboard extends Fragment implements OnMapReadyCallback {
    View view;
    MapView mMapView;
    private GoogleMap googleMap;
    private ScrollView mScrollView;

    VehicleALL vehicleALL;
    String username = "", vehicle_number = "";
    @BindView(R.id.car_image)
    ImageView car_image;

    @BindViews({R.id.driving_through_textview, R.id.you_drove_kms, R.id.you_drove_week_kmsvalue, R.id.you_drove_month_kmsvalue, R.id.you_drove_lastyear_kmsvalue})
    List<TextView> textViewList;
    SharedPref _sharedPref;
    Dialog dialog;
    TextView yes, no, ok;
boolean loaded;

    private static final int TWO_MINUTES = 1000 * 60 * 2;
    public LocationManager locationManager;
    public MyLocationListener listener;
    public Location previousBestLocation = null;
    Criteria myCriteria;
    double latitude = 0.0, longitude = 0.0;
    boolean v, driver_exist, first_time = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);


        // Inflate the layout for this fragment
      /*  String first = "Your're driving through  ";
        String next = "<font color='#389BCB'>"+first+"</font>";
        String last = "Be happy that you will earn more!";
        textViewList.get(0).setText(Html.fromHtml(first + next + "" + last));
     */
        _sharedPref = new SharedPref(getActivity());

        dialog = new Dialog(getActivity());
        // Include dialog.xml file
        dialog.setContentView(R.layout.no_vehicle);
        ok = (TextView) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
            }
        });

        dialog.setCancelable(false);

        ((WorkaroundMapFragment) this.getChildFragmentManager().findFragmentById(R.id.fragment_map)).getMapAsync(this);
        mScrollView = (ScrollView) view.findViewById(R.id.sv_container);

        ((WorkaroundMapFragment) this.getChildFragmentManager().findFragmentById(R.id.fragment_map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                mScrollView.requestDisallowInterceptTouchEvent(true);
            }
        });


        myCriteria = new Criteria();
        myCriteria.setAccuracy(Criteria.ACCURACY_FINE);
        locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        listener = new MyLocationListener();
        try {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            // int a = 111;
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            }
            LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            boolean gps_enabled = false;
            boolean network_enabled = false;

            try {
                gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch (Exception ex) {
            }

            try {
                network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } catch (Exception ex) {
            }

            if (!gps_enabled && !network_enabled) {
                // notify user
                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setMessage("Please enable location of your device");
                dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub
                        Intent myIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                        //get gps
                    }
                });
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub

                    }
                });
                //  dialog.show();
            }
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !v) {
               /* buildAlertMessageNoGps();
                Intent intent1 = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
             */
                v = true;
            }

            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, listener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, listener);
            System.out.println("Inside of location service    " + getLastKnownLocation().getLatitude());
            try {
                if (getLastKnownLocation() != null) {
                    latitude = getLastKnownLocation().getLatitude();
                    longitude = getLastKnownLocation().getLongitude();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        getAddress1();

        if (Global_Data.checkInternetConnection(getActivity())) {
            loaded=false;
            if (Build.VERSION.SDK_INT > 11) {

                new GetVehicleListDriver().executeOnExecutor(Global_Data.sExecutor);
            } else {

                new GetVehicleListDriver().execute();
            }

        } else {

            Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                if (Global_Data.checkInternetConnection(getActivity())) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetVehicleListDriver().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetVehicleListDriver().execute();
                    }

                } else {

                    //   Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                handler.postDelayed(this, 60000);
                // handler.postDelayed(this, 7000);
            }
        }, 60000);
        /*
        *
        * String first = "This word is ";
String next = "<font color='#EE0000'>red</font>";
t.setText(Html.fromHtml(first + next));   */
        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public boolean getUserVisibleHint() {
        System.out.println("getUserVisibleHint");


        return super.getUserVisibleHint();


    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;

        //setUpMap();
    }



    public void setUpMap() {

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity
            //
            //
            // Compat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        // googleMap.setMyLocationEnabled(true);
        googleMap.setTrafficEnabled(true);

        googleMap.setIndoorEnabled(true);
        googleMap.setBuildingsEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        // latitude and longitude
        double latitude1 = 17.385044;
        double longitude1 = 78.486671;

       /* // create marker
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(latitude, longitude)).title("Hello Maps");

        // Changing marker icon
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
*/
        // adding marker
        // googleMap.addMarker(marker);
        System.out.println("check coordinates----   " + vehicleALL.getDriver().getStats().getLattitude() + "   ...   " + vehicleALL.getDriver().getStats().getLongitude());
        //   if ((!vehicleALL.getDriver().getStats().getLattitude().equals("") && !vehicleALL.getDriver().getStats().getLongitude().equals("")) || (!vehicleALL.getDriver().getStats().getLattitude().trim().equals(null) && !vehicleALL.getDriver().getStats().getLongitude().trim().equals(null))  ||  ((!vehicleALL.getDriver().getStats().getLattitude().trim().equals("null")) && (!vehicleALL.getDriver().getStats().getLattitude().trim().equals("null"))) || ((vehicleALL.getDriver().getStats().getLongitude()!=null) && (vehicleALL.getDriver().getStats().getLattitude()!=null)) || ((!vehicleALL.getDriver().getStats().getLattitude().trim().equals("0")) && (!vehicleALL.getDriver().getStats().getLattitude().trim().equals(0)))) {
        if ((!vehicleALL.getDriver().getStats().getLattitude().trim().equals("0")) && (!vehicleALL.getDriver().getStats().getLattitude().trim().equals(0))) {

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(vehicleALL.getDriver().getStats().getLattitude()), Double.parseDouble(vehicleALL.getDriver().getStats().getLongitude()))).zoom(12).build();
            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

            googleMap.clear();
            createMarker(Double.parseDouble(vehicleALL.getDriver().getStats().getLattitude()), Double.parseDouble(vehicleALL.getDriver().getStats().getLongitude()), username, vehicle_number, R.drawable.yc);
            System.out.println("Check location in setUp map---   " + vehicleALL.getDriver().getStats().getLattitude());
            latitude = Double.parseDouble(vehicleALL.getDriver().getStats().getLattitude());
            longitude = Double.parseDouble(vehicleALL.getDriver().getStats().getLongitude());
            String first = "Your're driving through  ";
            String next = "<font color='#389BCB'>" + getAddress() + "</font>";
            String last = "Be happy that you will earn more!";
            textViewList.get(0).setText(Html.fromHtml(first + next + "" + last));


        }
    }

    protected Marker createMarker(double latitude, double longitude, String title, String snippet, int iconResID) {

        return googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(snippet)
                // .icon(BitmapDescriptorFactory.i
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
        //.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));

    }

/*
    public void setUpMap() {

        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.setTrafficEnabled(true);

        googleMap.setIndoorEnabled(true);
        googleMap.setBuildingsEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        // latitude and longitude
        double latitude = 17.385044;
        double longitude = 78.486671;

        // create marker
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(latitude, longitude)).title("Hello Maps");

        // Changing marker icon
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

        // adding marker
        googleMap.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(17.385044, 78.486671)).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

    }
*/


   /* @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }*/


    @Override
    public void onResume() {
        super.onResume();

        try {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 12000, 0, listener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 12000, 0, listener);
//            System.out.println("Inside of location service");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    //............................................................................................

    private class GetVehicleListDriver extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfOwnerJSON, jsonObject;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            if (!first_time) {

                progressDialog.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                AndroidNetworking.get(Global_Data._url + "drivers/" + _sharedPref.getStringData(Global_Data.id))
                        /*    .addBodyParameter("firstname", "Amit")
                            .addBodyParameter("lastname", "Shekhar")
                            .setTag("test")*/
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // do anything with response
                                _vehicleListOfOwnerJSON = response;
                                System.out.println("Driver's vehicle list in listof vehicles------   " + _vehicleListOfOwnerJSON);

                                try {
                                    if (_vehicleListOfOwnerJSON != null) {
                                        if (!_vehicleListOfOwnerJSON.isNull("vehicle")) {
                                            System.out.println("Inside not null");
                                            jsonObject = _vehicleListOfOwnerJSON.getJSONObject("vehicle");
                                            System.out.println("Check vehicle json object---   " + _vehicleListOfOwnerJSON.getJSONObject("vehicle"));

                                            vehicleALL = new VehicleALL(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getString("vehicle_number"), new Driver(111, "", "", "", "", "", "", "", new Stats(_vehicleListOfOwnerJSON.getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONObject("stats").getJSONObject("location").getString("longitude"), _vehicleListOfOwnerJSON.getJSONObject("stats").getJSONObject("location").getString("lattitude"))));
                                            username = _vehicleListOfOwnerJSON.getJSONObject("user").getString("username");
                                            vehicle_number = _vehicleListOfOwnerJSON.getJSONObject("vehicle").getString("vehicle_number");

                                            driver_exist = true;
                                            textViewList.get(1).setText(String.valueOf(_vehicleListOfOwnerJSON.getJSONObject("stats").getInt("daily")));
                                            textViewList.get(2).setText(String.valueOf(_vehicleListOfOwnerJSON.getJSONObject("stats").getInt("weekly")));

                                            textViewList.get(3).setText(String.valueOf(_vehicleListOfOwnerJSON.getJSONObject("stats").getInt("monthly")));
                                            textViewList.get(4).setText(String.valueOf(_vehicleListOfOwnerJSON.getJSONObject("stats").getInt("yearly")));
// Inflate the layout for this fragment
                                            JSONObject campaignJSON = _vehicleListOfOwnerJSON.optJSONObject("campaign");
                                            if (campaignJSON != null && !loaded) {
                                                Glide.with(getActivity()).load(_vehicleListOfOwnerJSON.getJSONObject("campaign").getString("campaign_logo_image"))
                                                        .thumbnail(0.5f)
                                                        .crossFade()
                                                        //.placeholder(R.drawable.rc_book_front)

                                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                        .into(car_image);
                                                loaded=true;
                                            }else {
                                                if(!loaded){
                                                   car_image.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.soch_a));
                                                }
                                            }
                                            setUpMap();


                                        } else {
                                            System.out.println("herekksksks");
                                            if (!first_time) {
                                                dialog.show();
                                            }
                                            try {
                                                textViewList.get(1).setText("0");
                                                textViewList.get(2).setText("0");
                                                textViewList.get(3).setText("0");
                                                textViewList.get(4).setText("0");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            System.out.println("Inside null");
                                        }
                                    } else {
                                        Toast.makeText(getActivity(), _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                                    }
                                    if (!first_time) {
                                        first_time = true;
                                        progressDialog.dismiss();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
               /* no_record_layout.setVisibility(View.VISIBLE);
                rv.setVisibility(View.GONE);
*/
                                    driver_exist = false;
                                    textViewList.get(1).setText("0");
                                    textViewList.get(2).setText("0");
                                    textViewList.get(3).setText("0");
                                    textViewList.get(4).setText("0");

                                    //  dialog.show();
                                    if (!first_time) {
                                        first_time = true;
                                        progressDialog.dismiss();
                                    }
                                }

                            }

                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });



               /* String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "drivers/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);
                //Uncomment it when code is running properly.
                // _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "drivers/" + 1, "GET", json2);
*/
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

      /*      try {
                if (_vehicleListOfOwnerJSON != null) {
                    // for(int i=0;i<_vehicleListOfOwnerJSON.getJSONArray("vehicles").length();i++) {
                    if (!_vehicleListOfOwnerJSON.isNull("vehicle")) {
                        System.out.println("Inside not null");
                        jsonObject = _vehicleListOfOwnerJSON.getJSONObject("vehicle");
                        // vehicleList= (List<Vehicle>) jsonArray;
                        // }
                        //   if (jsonArray.length() != 0 || jsonArray != null) {
                        System.out.println("Check vehicle json object---   " + _vehicleListOfOwnerJSON.getJSONObject("vehicle"));

//vehicleALL= new VehicleALL(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getJSONObject("stats").getString("longitude"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("driver").getJSONObject("stats").getString("lattitude"))));

                        vehicleALL = new VehicleALL(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONObject("vehicle").getString("vehicle_number"), new Driver(111, "", "", "", "", "", "", "", new Stats(_vehicleListOfOwnerJSON.getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONObject("stats").getString("longitude"), _vehicleListOfOwnerJSON.getJSONObject("stats").getString("lattitude"))));
                        username = _vehicleListOfOwnerJSON.getJSONObject("user").getString("username");
                        vehicle_number = _vehicleListOfOwnerJSON.getJSONObject("vehicle").getString("vehicle_number");

                        driver_exist = true;
                        textViewList.get(1).setText(String.valueOf(_vehicleListOfOwnerJSON.getJSONObject("stats").getInt("daily")));
                        textViewList.get(2).setText(String.valueOf(_vehicleListOfOwnerJSON.getJSONObject("stats").getInt("weekly")));

                        textViewList.get(3).setText(String.valueOf(_vehicleListOfOwnerJSON.getJSONObject("stats").getInt("monthly")));
                        textViewList.get(4).setText(String.valueOf(_vehicleListOfOwnerJSON.getJSONObject("stats").getInt("yearly")));
// Inflate the layout for this fragment

                          *//*  System.out.println("---->>>    " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"));
                            System.out.println("---->>>    " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"));
                         *//*

                        setUpMap();


                     *//*   vehicle_id = String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(0).getInt("id"));
                        driver_id = vehicle_id;
                        //   vehicleListResponseArrayAdapter.notifyDataSetChanged();
                        rv.setVisibility(View.VISIBLE);
                        vehicleListAdapter.notifyDataSetChanged();*//*
                    } else {
                        System.out.println("herekksksks");
                        if (!first_time) {
                            dialog.show();
                            //  Toast.makeText(getActivity(), "Vehicles are not associated for this owner.", Toast.LENGTH_SHORT).show();
                        }
                        try {
                            textViewList.get(1).setText("0");
                            textViewList.get(2).setText("0");
                            textViewList.get(3).setText("0");
                            textViewList.get(4).setText("0");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        System.out.println("Inside null");
                    }
                } else {
                    Toast.makeText(getActivity(), _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                }
                if (!first_time) {
                    first_time = true;
                    progressDialog.dismiss();
                }

            } catch (Exception e) {
                e.printStackTrace();
               *//* no_record_layout.setVisibility(View.VISIBLE);
                rv.setVisibility(View.GONE);
*//*
                driver_exist = false;
                textViewList.get(1).setText("0");
                textViewList.get(2).setText("0");
                textViewList.get(3).setText("0");
                textViewList.get(4).setText("0");

                //  dialog.show();
                if (!first_time) {
                    first_time = true;
                    progressDialog.dismiss();
                }
            }*/


        }
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void buildAlertMessageNoGps() {
//        System.out.println("inside alert box");
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }


    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    LocationManager mLocationManager;

    //...
    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {

           /* if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return null;
            }*/
            @SuppressLint("MissingPermission") Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {

                bestLocation = l;
            }
        }
        return bestLocation;
    }

    public class MyLocationListener implements LocationListener {

        @SuppressLint("LongLogTag")
        public void onLocationChanged(final Location loc) {
            try {
                //  Toast.makeText(getApplicationContext(), "OnChanged location....   ", Toast.LENGTH_SHORT).show();
                Log.i("**************************************", "Location changed");
                if (isBetterLocation(loc, previousBestLocation)) {
          /*      Toast.makeText(getApplicationContext(), "Accuracy....  " + loc.getAccuracy(), Toast.LENGTH_SHORT).show();
                if (loc.getAccuracy() < 8) {
          */


                    latitude = loc.getLatitude();
                    longitude = loc.getLongitude();
                    // Toast.makeText(getActivity(), "Location changed in ----   " + latitude + "   ...   " + longitude, Toast.LENGTH_SHORT).show();
                    if (driver_exist) {
                        googleMap.clear();
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(latitude, longitude)).zoom(15).build();
                        googleMap.animateCamera(CameraUpdateFactory
                                .newCameraPosition(cameraPosition));
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        // googleMap.setMyLocationEnabled(true);

                        createMarker(loc.getLatitude(), loc.getLongitude(), username, vehicle_number, R.drawable.yc);
                    }

                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }

                    // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 17));
                    //      }
                    loc.getLatitude();
                    loc.getLongitude();

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onProviderDisabled(String provider) {
            Toast.makeText(getActivity(), "Gps Disabled", Toast.LENGTH_SHORT).show();
            //   notifyprovider = "off";
            // new GPSStatus().execute();
        }


        public void onProviderEnabled(String provider) {
            Toast.makeText(getActivity(), "Gps Enabled", Toast.LENGTH_SHORT).show();
            ///notifyprovider = "on";
            // new GPSStatus().execute();

        }


        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            try {
                locationManager.removeUpdates(listener);
                locationManager = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (dialog != null && dialog.isShowing()) {
            dialog.cancel();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (locationManager != null) {
            try {
                locationManager.removeUpdates(listener);
                locationManager = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (dialog != null && dialog.isShowing()) {
            dialog.cancel();
        }
    }


    public String getAddress() {
        String address = "", city = "", state = "", country = "", postalCode = "", knownName = "", s = "";
        try {
            Geocoder geocoder;
            List<android.location.Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());


            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            postalCode = addresses.get(0).getPostalCode();
            knownName = addresses.get(0).getFeatureName();
            s = addresses.get(0).getThoroughfare() + "," + city + " ";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;

    }


    public String getAddress1() {
        // String address = "", city = "", state = "", country = "", postalCode = "", knownName = "", s = "";
        try {
            Geocoder geocoder;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            String result = null;

            /*List<android.location.Address> addresses;


            addresses = geocoder.getFromLocation(22.2139, 75.4723, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
          */
            List<Address> addressList = geocoder.getFromLocation(22.2139, 75.4723, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)); //.append("\n");
                }
                sb.append(address.getLocality()).append("\n");
                sb.append(address.getPostalCode()).append("\n");
                sb.append(address.getCountryName());
                result = sb.toString();

                System.out.println("Result is for address--   " + result + "   " + addressList.get(0).getThoroughfare());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";

    }

}
