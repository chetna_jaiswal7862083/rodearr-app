package rodearr.app.com.rodearrapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.models.CampaignDetailListModel;

public class Campaign_Details extends AppCompatActivity {
    List<CampaignDetailListModel> campaign_detailsList;
    @BindViews({R.id.join_this_campaign_layout, R.id.incentivised_location_layout})
    List<LinearLayout> linearLayoutList;

    @BindViews({R.id.join_this_campaign_btn, R.id.incentivised_btn})
    List<Button> buttonList;
    int pos;
    String bundlePos;

    @BindViews({R.id.go_back, R.id.campaigns_image})
    List<ImageView> imageViews;
    @BindViews({R.id.start_date_value, R.id.end_date_value, R.id.earn_value, R.id.top_title, R.id.advertise_value, R.id.dailycap_value, R.id.name_value, R.id.location_value})
    List<TextView> valueTVList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaign__details);
        ButterKnife.bind(this);
        try {


            campaign_detailsList = (ArrayList<CampaignDetailListModel>) getIntent().getSerializableExtra(
                    "CAMPAIGN LIST");
            Bundle bundle = getIntent().getExtras();

            bundlePos = bundle.getString("POS");
            pos = Integer.parseInt(bundlePos);
            String sDate = campaign_detailsList.get(pos).getStartDate();
            String[] separated = sDate.split("-");
            valueTVList.get(0).setText(separated[2]+"-"+separated[1]+"-"+separated[0]);
            String eDate =campaign_detailsList.get(pos).getEndDate();
            String[] separated1 = eDate.split("-");
            valueTVList.get(1).setText( separated1[2]+"-"+separated1[1]+"-"+separated1[0]);
         /*   valueTVList.get(1).setText(campaign_detailsList.get(pos).getEndDate());
         */   valueTVList.get(2).setText(campaign_detailsList.get(pos).getMinEarning() + "-" + campaign_detailsList.get(pos).getMaxEarning() + "");
            valueTVList.get(3).setText(campaign_detailsList.get(pos).getCampaign().getName());
            valueTVList.get(4).setText(campaign_detailsList.get(pos).getAdvertiser().getFirstname() + " " + campaign_detailsList.get(pos).getAdvertiser().getLastname());

            valueTVList.get(7).setText(campaign_detailsList.get(pos).getCity().getName());
            valueTVList.get(6).setText(campaign_detailsList.get(pos).getCampaign().getName());

            String s1 = campaign_detailsList.get(pos).getMinKm().toString() + "-" + campaign_detailsList.get(pos).getMaxKm();
            String next = "<font color='#389BCB'>" + s1 + "</font>";
            String last = " KMS";
            valueTVList.get(5).setText(Html.fromHtml(next + "" + last));

            imageViews.get(0).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            if (!campaign_detailsList.get(pos).getCampaign_logo_image().equals(null)) {
                Glide.with(Campaign_Details.this).load(campaign_detailsList.get(pos).getCampaign_logo_image())
                        .thumbnail(0.5f)
                        .crossFade()
                        //.placeholder(R.drawable.rightsideviewwithnp)

                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageViews.get(1));
            }
            linearLayoutList.get(0).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("callVehicleCampaignAssociation.putExtra---    " + campaign_detailsList.get(pos).getAdvertiser().getId());
                    Intent callVehicleCampaignAssociation = new Intent(Campaign_Details.this, Vehicle_Campaign_Association.class);
                    callVehicleCampaignAssociation.putExtra("advertiser", String.valueOf(campaign_detailsList.get(pos).getAdvertiser().getId()));
                    callVehicleCampaignAssociation.putExtra("campaign", String.valueOf(campaign_detailsList.get(pos).getCampaign().getId()));
                    callVehicleCampaignAssociation.putExtra("campaign_per_city", String.valueOf(campaign_detailsList.get(pos).getId()));
                    // callVehicleCampaignAssociation.putExtra( "vehicle", campaign_detailsList.get(pos).getId());
                    startActivity(callVehicleCampaignAssociation);
                }
            });

            buttonList.get(0).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("callVehicleCampaignAssociation.putExtra---    " + campaign_detailsList.get(pos).getAdvertiser().getId());
                    Intent callVehicleCampaignAssociation = new Intent(Campaign_Details.this, Vehicle_Campaign_Association.class);
                    callVehicleCampaignAssociation.putExtra("advertiser", String.valueOf(campaign_detailsList.get(pos).getAdvertiser().getId()));
                    callVehicleCampaignAssociation.putExtra("campaign", String.valueOf(campaign_detailsList.get(pos).getCampaign().getId()));
                    callVehicleCampaignAssociation.putExtra("campaign_per_city", String.valueOf(campaign_detailsList.get(pos).getId()));
                    // callVehicleCampaignAssociation.putExtra( "vehicle", campaign_detailsList.get(pos).getId());
                    startActivity(callVehicleCampaignAssociation);
                }
            });
            buttonList.get(1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callVehicleCampaignAssociation = new Intent(Campaign_Details.this, Incentivised_Location.class);
                    callVehicleCampaignAssociation.putExtra("campaign", String.valueOf(campaign_detailsList.get(pos).getCampaign().getName()));

                    startActivity(callVehicleCampaignAssociation);
                }
            });
            linearLayoutList.get(1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callVehicleCampaignAssociation = new Intent(Campaign_Details.this, Incentivised_Location.class);
                    callVehicleCampaignAssociation.putExtra("campaign", String.valueOf(campaign_detailsList.get(pos).getCampaign().getName()));

                    startActivity(callVehicleCampaignAssociation);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
