package rodearr.app.com.rodearrapp.models;
import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class CampaignDetailListModel implements Serializable {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("city")
    @Expose
    private CityListResponse city;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("no_of_vehicles")
    @Expose
    private Integer noOfVehicles;
    @SerializedName("min_earning")
    @Expose
    private Integer minEarning;
    @SerializedName("max_earning")
    @Expose
    private Integer maxEarning;
    @SerializedName("min_km")
    @Expose
    private Integer minKm;
    @SerializedName("max_km")
    @Expose
    private Integer maxKm;
    @SerializedName("incentivised_locations")
    @Expose
    private Object incentivisedLocations;
    @SerializedName("campaign")
    @Expose
    private Campaign campaign;
    @SerializedName("advertiser")
    @Expose
    private Advertiser advertiser;
    @SerializedName("campaign_logo_image")
    @Expose
    private String campaign_logo_image;

    @SerializedName("vehicles")
    @Expose
    private List<Campaign_Vehicle> vehicles = null;

    public CampaignDetailListModel(Integer id, String status, CityListResponse city, String startDate, String endDate, Integer noOfVehicles, Integer minEarning, Integer maxEarning, Integer minKm, Integer maxKm, Object incentivisedLocations, Campaign campaign, Advertiser advertiser, List<Campaign_Vehicle> vehicles,String campaign_logo_image) {
        this.id = id;
        this.status = status;
        this.city = city;
        this.startDate = startDate;
        this.endDate = endDate;
        this.noOfVehicles = noOfVehicles;
        this.minEarning = minEarning;
        this.maxEarning = maxEarning;
        this.minKm = minKm;
        this.maxKm = maxKm;
        this.incentivisedLocations = incentivisedLocations;
        this.campaign = campaign;
        this.advertiser = advertiser;
        this.vehicles = vehicles;
        this.campaign_logo_image=campaign_logo_image;
    }

    public String getCampaign_logo_image() {
        return campaign_logo_image;
    }

    public void setCampaign_logo_image(String campaign_logo_image) {
        this.campaign_logo_image = campaign_logo_image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CityListResponse getCity() {
        return city;
    }

    public void setCity(CityListResponse city) {
        this.city = city;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getNoOfVehicles() {
        return noOfVehicles;
    }

    public void setNoOfVehicles(Integer noOfVehicles) {
        this.noOfVehicles = noOfVehicles;
    }

    public Integer getMinEarning() {
        return minEarning;
    }

    public void setMinEarning(Integer minEarning) {
        this.minEarning = minEarning;
    }

    public Integer getMaxEarning() {
        return maxEarning;
    }

    public void setMaxEarning(Integer maxEarning) {
        this.maxEarning = maxEarning;
    }

    public Integer getMinKm() {
        return minKm;
    }

    public void setMinKm(Integer minKm) {
        this.minKm = minKm;
    }

    public Integer getMaxKm() {
        return maxKm;
    }

    public void setMaxKm(Integer maxKm) {
        this.maxKm = maxKm;
    }

    public Object getIncentivisedLocations() {
        return incentivisedLocations;
    }

    public void setIncentivisedLocations(Object incentivisedLocations) {
        this.incentivisedLocations = incentivisedLocations;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    public Advertiser getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(Advertiser advertiser) {
        this.advertiser = advertiser;
    }

    public List<Campaign_Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Campaign_Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

}
//.....................