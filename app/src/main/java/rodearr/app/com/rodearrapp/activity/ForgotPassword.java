package rodearr.app.com.rodearrapp.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;

public class ForgotPassword extends AppCompatActivity {

    @BindViews({R.id.provide_detail_layout, R.id.forgot_pass_layout, R.id.set_new_password_layout})
    List<ConstraintLayout> constraintLayoutViewList;

    @BindViews({R.id.provide_detail_next_btn, R.id.next_otp_btn, R.id.save_new_password_btn})
    List<Button> buttonViewList;

    @BindView(R.id.enter_mobile_number)
    EditText enter_mobile_number;

    @BindViews({R.id.otp1, R.id.otp2, R.id.otp3, R.id.otp4, R.id.otp5, R.id.otp6, R.id.new_pass, R.id.confirm_pass, R.id.username_forgotpass})
    List<EditText> otpEditTextViews;

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    SharedPref _sharedPref;
    private String accessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        _sharedPref = new SharedPref(ForgotPassword.this);
        constraintLayoutViewList.get(0).setVisibility(View.VISIBLE);
        if (checkAndRequestPermissions()) {
            // carry on the normal flow, as the case of  permissions  granted.
            init();
        } else {
            init();
        }

    }

    private void init() {
        buttonViewList.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enter_mobile_number.getText().toString().length() == 0) {

                    Toast.makeText(ForgotPassword.this, "Please enter mobile number",
                            Toast.LENGTH_SHORT).show();
                } else if (enter_mobile_number.getText().toString().length() < 10) {

                    Toast.makeText(ForgotPassword.this, "Please enter 10 digit mobile number",
                            Toast.LENGTH_SHORT).show();
                } else if (enter_mobile_number.getText().toString().length() > 10) {

                    Toast.makeText(ForgotPassword.this, "Please enter 10 digit mobile number",
                            Toast.LENGTH_SHORT).show();
                } else {
                   /* String otp = String.valueOf(generateRandomNumber());
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(enter_mobile_number.getText().toString(), null, otp, null, null);
                  */

                    if (Global_Data.checkInternetConnection(ForgotPassword.this)) {
                        if (Build.VERSION.SDK_INT > 11) {

                            new SendSMSTask().executeOnExecutor(Global_Data.sExecutor);


                        } else {

                            new SendSMSTask().execute();
                        }

                    } else {

                        Toast.makeText(ForgotPassword.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }

                    constraintLayoutViewList.get(1).setVisibility(View.VISIBLE);
                    constraintLayoutViewList.get(0).setVisibility(View.GONE);
                    constraintLayoutViewList.get(2).setVisibility(View.GONE);
                }
            }
        });
        buttonViewList.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String s = "";
                for (int i = 0; i < otpEditTextViews.size(); i++) {
                    s += otpEditTextViews.get(i).getText().toString();
                    System.out.println("String OTP--   " + s);
                }
                if (s.equals(_sharedPref.getStringData(Global_Data.otp))) {
                    constraintLayoutViewList.get(2).setVisibility(View.VISIBLE);
                    constraintLayoutViewList.get(1).setVisibility(View.GONE);
                    constraintLayoutViewList.get(0).setVisibility(View.GONE);
                } else {
                    Toast.makeText(ForgotPassword.this, "Incorrect OTP.  ",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

        buttonViewList.get(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otpEditTextViews.get(6).getText().toString().length() == 0) {
                    Toast.makeText(ForgotPassword.this, "Please enter new password.  ",
                            Toast.LENGTH_SHORT).show();
                } else if (otpEditTextViews.get(7).getText().toString().length() == 0) {
                    Toast.makeText(ForgotPassword.this, "Please enter confirm password.  ",
                            Toast.LENGTH_SHORT).show();
                } else if (!otpEditTextViews.get(7).getText().toString().equals(otpEditTextViews.get(6).getText().toString())) {
                    Toast.makeText(ForgotPassword.this, "Confirm password do not match.  ",
                            Toast.LENGTH_SHORT).show();
                } else {

                    if (Global_Data.checkInternetConnection(ForgotPassword.this)) {

                        if (Build.VERSION.SDK_INT > 11) {
                            new FindAccessToken().executeOnExecutor(Global_Data.sExecutor);
                        } else {
                            new FindAccessToken().execute();
                        }
                    } else {

                        Toast.makeText(ForgotPassword.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }


                }
                    /*constraintLayoutViewList.get(2).setVisibility(View.VISIBLE);
                    constraintLayoutViewList.get(1).setVisibility(View.GONE);
                    constraintLayoutViewList.get(0).setVisibility(View.GONE);*/
            }
        });
        otpEditTextViews.get(0).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(1).requestFocus();
                    otpEditTextViews.get(1).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(1).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(2).requestFocus();
                    otpEditTextViews.get(2).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(2).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(3).requestFocus();
                    otpEditTextViews.get(3).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(3).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(4).requestFocus();
                    otpEditTextViews.get(4).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(4).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(5).requestFocus();
                    otpEditTextViews.get(5).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpEditTextViews.get(5).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    System.out.println("count   " + count);
                    otpEditTextViews.get(5).requestFocus();
                    otpEditTextViews.get(5).setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    // Create an anonymous implementation of OnClickListener
    private View.OnClickListener editTextClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            // do something when the button is clicked
            // Yes we will handle click here but which button clicked??? We don't know

            // So we will make
            switch (v.getId() /*to get clicked view id**/) {
                case R.id.otp1:
                    otpEditTextViews.get(0).addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() == 1) {
                                System.out.println("count   " + count);
                                otpEditTextViews.get(1).requestFocus();
                                otpEditTextViews.get(1).setCursorVisible(true);

                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    // do something when the corky is clicked
                    break;
                case R.id.otp2:

                    // do something when the corky2 is clicked
                    otpEditTextViews.get(1).addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() == 1) {
                                System.out.println("count   " + count);
                                otpEditTextViews.get(2).requestFocus();
                                otpEditTextViews.get(2).setCursorVisible(true);

                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    break;
                case R.id.otp3:

                    // do something when the corky3 is clicked
                    otpEditTextViews.get(2).addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() == 1) {
                                System.out.println("count   " + count);
                                otpEditTextViews.get(3).requestFocus();
                                otpEditTextViews.get(3).setCursorVisible(true);

                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    break;
                case R.id.otp4:

                    // do something when the corky3 is clicked
                    otpEditTextViews.get(3).addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() == 1) {
                                System.out.println("count   " + count);
                                otpEditTextViews.get(4).requestFocus();
                                otpEditTextViews.get(4).setCursorVisible(true);

                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    break;
                case R.id.otp5:

                    // do something when the corky3 is clicked
                    otpEditTextViews.get(4).addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() == 1) {
                                System.out.println("count   " + count);
                                otpEditTextViews.get(5).requestFocus();
                                otpEditTextViews.get(5).setCursorVisible(true);

                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    break;
                case R.id.otp6:

                    // do something when the corky3 is clicked
                    otpEditTextViews.get(5).addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() == 1) {
                                System.out.println("count   " + count);
                                otpEditTextViews.get(5).requestFocus();
                                otpEditTextViews.get(5).setCursorVisible(true);

                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    break;
                default:
                    break;
            }
        }
    };
    int range = 9;  // to generate a single number with this range, by default its 0..9
    int length = 6; // by default length is 4

    public int generateRandomNumber() {
        int randomNumber;

        SecureRandom secureRandom = new SecureRandom();
        String s = "";
        for (int i = 0; i < length; i++) {
            int number = secureRandom.nextInt(range);
            if (number == 0 && i == 0) { // to prevent the Zero to be the first number as then it will reduce the length of generated pin to three or even more if the second or third number came as zeros
                i = -1;
                continue;
            }
            s = s + number;
        }

        randomNumber = Integer.parseInt(s);

        return randomNumber;
    }


  /* public int generateRandomNumber(){
        int min = 65;
        int max = 80;

        Random r = new Random();
        int i1 = r.nextInt(max - min + 1) + min;
    }*/


    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);
        int receiveSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
        int readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                if (intent.getStringExtra("Sender").equals("IX-TFCTOR")) {
                    /*Intent intent1 = new Intent(OnboardingOTPVerification.this, OnboardingOTPVerification.class);
                    intent1.putExtra("message", message);
                    startActivity(intent1);*/
                    //intent1.putExtra("firstname", editTextViewList.get(0).getText().toString());
                    otpEditTextViews.get(0).setText(_sharedPref.getStringData(Global_Data.otp).substring(0, 1));
                    otpEditTextViews.get(1).setText(_sharedPref.getStringData(Global_Data.otp).substring(1, 2));
                    otpEditTextViews.get(2).setText(_sharedPref.getStringData(Global_Data.otp).substring(2, 3));
                    otpEditTextViews.get(3).setText(_sharedPref.getStringData(Global_Data.otp).substring(3, 4));
                    otpEditTextViews.get(4).setText(_sharedPref.getStringData(Global_Data.otp).substring(4, 5));
                    otpEditTextViews.get(5).setText(_sharedPref.getStringData(Global_Data.otp).substring(5, 6));

                 /*   Intent intent2 = new Intent(OnboardingOTPVerification.this, HomeActivity.class);

                    startActivity(intent2);
                    finish();  */


                }

               /* TextView tv = (TextView) findViewById(R.id.txtview);
                tv.setText(message);
           */
            }
        }
    };

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private class FindAccessToken extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _tokenOutputJSON, _inputTokenJSON;
        ProgressDialog progressDialog = new ProgressDialog(ForgotPassword.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                _inputTokenJSON = new JSONObject();
                _inputTokenJSON.put("username", "rdrappuser");
                _inputTokenJSON.put("password", "app@123");
                String json2 = "";
                json2 = _inputTokenJSON.toString();
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _tokenOutputJSON = jsonParser.makeHttpRequest(Global_Data._url1 + "token/", "POST", json2);
                System.out.println(_tokenOutputJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {

                if (_tokenOutputJSON.has("access")) {

                    accessToken = _tokenOutputJSON.getString("access");
                    if (Global_Data.checkInternetConnection(ForgotPassword.this)) {

                        if (Build.VERSION.SDK_INT > 11) {
                            new ResetPassword().executeOnExecutor(Global_Data.sExecutor);
                        } else {
                            new ResetPassword().execute();
                        }
                    } else {

                        Toast.makeText(ForgotPassword.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(ForgotPassword.this, "Unable to found access token.", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    //..........................................


    private class ResetPassword extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _tokenOutputJSON, _inputTokenJSON;
        ProgressDialog progressDialog = new ProgressDialog(ForgotPassword.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                _inputTokenJSON = new JSONObject();
                _inputTokenJSON.put("username", otpEditTextViews.get(8).getText().toString());
                _inputTokenJSON.put("new_password", otpEditTextViews.get(6).getText().toString());
                String json2 = "";
                json2 = _inputTokenJSON.toString();
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                //  _tokenOutputJSON = jsonParser.makeHttpRequestWithHeader(Global_Data._url + "resetpassword/", "POST", json2, accessToken);
                _tokenOutputJSON = JSONParser.getDataFromWeb(Global_Data._url + "resetpassword/", otpEditTextViews.get(8).getText().toString(), otpEditTextViews.get(6).getText().toString(), accessToken);
                System.out.println(_tokenOutputJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_tokenOutputJSON != null) {
                    if (_tokenOutputJSON.has("status")) {
                        if (_tokenOutputJSON.getString("status").equals("[\"No user found with this username.\"]")) {
                            Toast.makeText(ForgotPassword.this, "Invalid User.", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Intent intent = new Intent(ForgotPassword.this, Login.class);
                        startActivity(intent);
                        finish();
                    }
                    progressDialog.dismiss();
                }else {
                    Toast.makeText(ForgotPassword.this, "Password reset successfully", Toast.LENGTH_LONG).show();

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 900);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    //........................................


    private class SendSMSTask extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _sendOTPJSON;
        ProgressDialog progressDialog = new ProgressDialog(ForgotPassword.this);
        String mobileNumberOTP = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {


                mobileNumberOTP = String.valueOf(generateRandomNumber());
                String json2 = enter_mobile_number.getText().toString() + "/" + mobileNumberOTP;
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _sendOTPJSON = jsonParser.makeHttpRequest("https://2factor.in/API/V1/4a668095-46be-11e8-a895-0200cd936042/SMS/" + json2, "POST", "");
                System.out.println(_sendOTPJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jo1 = null;
                if (_sendOTPJSON.has("Status")) {
                    //  _sharedPref.setStringData(Global_Data.mobile_number_otp, mobileNumberOTP);
                    _sharedPref.setStringData(Global_Data.otp, mobileNumberOTP);


                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }
        }


    }


}
