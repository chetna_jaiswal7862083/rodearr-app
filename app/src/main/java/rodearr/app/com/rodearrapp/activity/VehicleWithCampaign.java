package rodearr.app.com.rodearrapp.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.adapter.VehicleCampaignAdapter;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.models.Brand;
import rodearr.app.com.rodearrapp.models.Category;
import rodearr.app.com.rodearrapp.models.Driver;
import rodearr.app.com.rodearrapp.models.Model;
import rodearr.app.com.rodearrapp.models.Stats;
import rodearr.app.com.rodearrapp.models.VehicleALL;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;

public class VehicleWithCampaign extends AppCompatActivity {
    @BindView(R.id.no_record_layout)
    LinearLayout no_record_layout;
    private RecyclerView vehicle_with_campaign_list;
    SharedPref _sharedPref;
    String owner_id = "", vehicle_id = "0", driver_id = "";
    public static int selectedVehicleId = -1;

    ArrayList<VehicleALL> vehicleListResponseList;
    private VehicleCampaignAdapter vehicleListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_with_campaign);
        ButterKnife.bind(this);
        vehicle_with_campaign_list = (RecyclerView) findViewById(R.id.vehicle_with_campaign_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(VehicleWithCampaign.this);
        vehicle_with_campaign_list.setLayoutManager(mLayoutManager);
        vehicle_with_campaign_list.setItemAnimator(new DefaultItemAnimator());
        vehicle_with_campaign_list.setNestedScrollingEnabled(false);
        vehicleListResponseList = new ArrayList<VehicleALL>();



        _sharedPref = new SharedPref(VehicleWithCampaign.this);



    }

    @Override
    public void onResume() {
        super.onResume();

        no_record_layout.setVisibility(View.GONE);

        if (Global_Data.checkInternetConnection(VehicleWithCampaign.this)) {
            if (Build.VERSION.SDK_INT > 11) {

                new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);

            } else {

                new GetVehicleListOfOwner().execute();

            }

        } else {

            Toast.makeText(VehicleWithCampaign.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }

    }


    //............................................................................................
    boolean isStatsAvailable;

    private class GetVehicleListOfOwner extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfOwnerJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(VehicleWithCampaign.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AndroidNetworking.get(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id))
                        /*    .addBodyParameter("firstname", "Amit")
                            .addBodyParameter("lastname", "Shekhar")
                            .setTag("test")*/
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // do anything with response
                                _vehicleListOfOwnerJSON =response;
                                System.out.println("Owner's vehicle list in Vehicle campaign association------   " + _vehicleListOfOwnerJSON);

                                try {
                                    vehicleListResponseList.clear();
                                    if (_vehicleListOfOwnerJSON != null) {
                                        if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                                            System.out.println("Inside not null");
                                            jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                                            if (jsonArray.length() > 0) {
                                                for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {


                                                    try {
                                                        System.out.println("value of i---->..   " + i);
                                                        JSONObject driverObj = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).optJSONObject("driver");
                                                        JSONObject campaignObj = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).optJSONObject("campaign");
                                                        if (driverObj != null && campaignObj != null) {
                                                            if (_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").has("stats")) {

                                                                JSONObject dataObject = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").optJSONObject("stats");

                                                                if (dataObject != null) {

                                                                    //Do things with object.
                                                                    isStatsAvailable = true;
                                                                } else {

                                                                    isStatsAvailable = false;
                                                                    //Do things with array
                                                                }
                                                            }


                                                            if (isStatsAvailable) {
                                                                vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getJSONObject("location").getString("longitude"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getJSONObject("location").getString("lattitude")))));

                                                            } else {
                                                                vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                                            }
                                                        } else {


                                                        }


                                                    } catch (Exception e) {

                                                        //No driver   vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(111, "", "", "", "", "", "", "", new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                                        e.printStackTrace();
                                                    }

                                                }

                                                vehicle_id = String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(0).getInt("id"));
                                                driver_id = vehicle_id;
                                                //   vehicleListResponseArrayAdapter.notifyDataSetChanged();
                                                vehicle_with_campaign_list.setVisibility(View.VISIBLE);
                                                // associate.setVisibility(View.VISIBLE);

                                                vehicleListAdapter = new VehicleCampaignAdapter(vehicleListResponseList, VehicleWithCampaign.this);

                                                vehicle_with_campaign_list.setAdapter(vehicleListAdapter);


                                                if (vehicleListResponseList.size() == 0) {
                                                    no_record_layout.setVisibility(View.VISIBLE);
                                                    vehicle_with_campaign_list.setVisibility(View.GONE);
                                                    //   associate.setVisibility(View.GONE);
                                                }

                                            } else {
                                                no_record_layout.setVisibility(View.VISIBLE);
                                                vehicle_with_campaign_list.setVisibility(View.GONE);
                                                // associate.setVisibility(View.GONE);
                                            }

                                        } else {
                                            System.out.println("Inside null");
                                        }
                                    } else {

                                        Toast.makeText(VehicleWithCampaign.this, _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                                    }
                                    progressDialog.dismiss();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    no_record_layout.setVisibility(View.VISIBLE);
                                    vehicle_with_campaign_list.setVisibility(View.GONE);
                                    progressDialog.dismiss();

                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });



              /*  String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);

               */ //Uncomment it when code is running properly.       _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + 2, "GET", json2);

                   } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            /*try {
                vehicleListResponseList.clear();
                if (_vehicleListOfOwnerJSON != null) {
                    if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                        System.out.println("Inside not null");
                        jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {


                                try {
                                    System.out.println("value of i---->..   " + i);
                                    JSONObject driverObj = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).optJSONObject("driver");
                                    JSONObject campaignObj = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).optJSONObject("campaign");
                                    if (driverObj != null && campaignObj != null) {
                                        if (_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").has("stats")) {

                                            JSONObject dataObject = _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").optJSONObject("stats");

                                            if (dataObject != null) {

                                                //Do things with object.
                                                isStatsAvailable = true;
                                            } else {

                                                isStatsAvailable = false;
                                                //Do things with array
                                            }
                                        }


                                        if (isStatsAvailable) {
                                            vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("longitude"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("lattitude")))));

                                        } else {
                                            vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                        }
                                    } else {


                                    }


                                } catch (Exception e) {

                                    //No driver   vehicleListResponseList.add(new VehicleALL(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), new Brand(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("brand").getString("code")), new Category(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("name"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("category").getString("desc")), new Model(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("model").getString("name")), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(111, "", "", "", "", "", "", "", new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                    e.printStackTrace();
                                }

                            }

                            vehicle_id = String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(0).getInt("id"));
                            driver_id = vehicle_id;
                            //   vehicleListResponseArrayAdapter.notifyDataSetChanged();
                            vehicle_with_campaign_list.setVisibility(View.VISIBLE);
                            // associate.setVisibility(View.VISIBLE);

                            vehicleListAdapter = new VehicleCampaignAdapter(vehicleListResponseList, VehicleWithCampaign.this);

                            vehicle_with_campaign_list.setAdapter(vehicleListAdapter);


                            if (vehicleListResponseList.size() == 0) {
                                no_record_layout.setVisibility(View.VISIBLE);
                                vehicle_with_campaign_list.setVisibility(View.GONE);
                                //   associate.setVisibility(View.GONE);
                            }

                        } else {
                            no_record_layout.setVisibility(View.VISIBLE);
                            vehicle_with_campaign_list.setVisibility(View.GONE);
                            // associate.setVisibility(View.GONE);
                        }

                    } else {
                        System.out.println("Inside null");
                    }
                } else {

                    Toast.makeText(VehicleWithCampaign.this, _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                no_record_layout.setVisibility(View.VISIBLE);
                vehicle_with_campaign_list.setVisibility(View.GONE);
                progressDialog.dismiss();

            }*/


        }
    }
}
