package rodearr.app.com.rodearrapp.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cloudrail.si.CloudRail;
import com.cloudrail.si.interfaces.SMS;
import com.cloudrail.si.services.Twilio;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.models.BrandListResponse;
import rodearr.app.com.rodearrapp.models.CategoryListResponse;
import rodearr.app.com.rodearrapp.models.GetAllModels;
import rodearr.app.com.rodearrapp.models.GetBrandsModel;
import rodearr.app.com.rodearrapp.models.GetCategoryModel;
import rodearr.app.com.rodearrapp.models.OwnerResponse;
import rodearr.app.com.rodearrapp.models.User_a;
import rodearr.app.com.rodearrapp.models.VehicleListResponse;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;
import rodearr.app.com.rodearrapp.utils.BitmapUtils;
import rodearr.app.com.rodearrapp.utils.ImageUtils;

public class UpdateVehicle extends AppCompatActivity {

    @BindViews({R.id.brand, R.id.model, R.id.category, R.id.vehicle_number, R.id.owner_list, R.id.driver_list})
    List<Spinner> spinnerViewList;
    List<BrandListResponse> brandListResponseList;
    List<BrandListResponse.VehicleModel> vehicleModelList;
    List<BrandListResponse.Category> categoryList;
    List<CategoryListResponse> categoryListResponseList;
    List<CategoryListResponse> modelListResponseList;
    List<GetAllModels> allmodelListResponseList;
    List<OwnerResponse> ownerResponseList;
    List<OwnerResponse> driverResponseList;
    //  List<CategoryListResponse> modelListResponseList;
    @BindView(R.id.carNumberEditText)
    EditText carNumberEditText;
    @BindViews({R.id.change_frontsideview_np_layout, R.id.change_backsideview_np_layout, R.id.change_leftsideview_np_layout, R.id.upload_leftsideview_layout, R.id.upload_vehicle_tax_doc_frontview_layout,
            R.id.upload_vehicle_tax_doc_back_layout, R.id.upload_vehicle_rc_card_front_layout, R.id.upload_vehicle_rc_card_back_layout, R.id.vehicle_list_layout})
    List<LinearLayout> linearLayoutsViewList;

    @BindViews({R.id.change_frontsideview_np, R.id.change_backsideview_np, R.id.change_leftsideview_np, R.id.upload_leftsideview, R.id.upload_rightsideview, R.id.upload_vehicle_tax_doc_back,
            R.id.upload_vehicle_rc_card_front, R.id.upload_vehicle_rc_card_back})
    List<Button> btnViewList;

    @BindViews({R.id.frontsideviewnp_imageview, R.id.backsideviewnp_imageview, R.id.leftsideview_imageview, R.id.rightsideview_imageview, R.id.Vehicle_tax_doc_frontview_imageview, R.id.vehicle_tax_doc_back_imageview, R.id.vehicle_rc_card_front_imageview, R.id.vehicle_rc_card_back_imageview,R.id.back})
    List<ImageView> imageViewList;
    private ArrayAdapter<BrandListResponse> brandListResponseArrayAdapter;
    private ArrayAdapter<CategoryListResponse> categoryListResponseArrayAdapter;
    private ArrayAdapter<CategoryListResponse> modelListResponseArrayAdapter;
    private ArrayAdapter<VehicleListResponse> vehicleListResponseArrayAdapter;
    private ArrayAdapter<OwnerResponse> ownerListResponseArrayAdapter;
    private ArrayAdapter<OwnerResponse> driverListResponseArrayAdapter;
    private View view;

    private ArrayList<String> modelList;
    private ArrayAdapter<String> modelListAdapter;
    private int PICK_IMAGE_REQUEST = 100;
    private int imageType = 9;
    private Uri filePath;
    public static final String TAG = "Upload Image";
    private Bitmap bitmap = null, frontsideviewnp_imageview_bitmap = null, backsideviewnp_imageview_bitmap = null, leftsideview_imageview_bitmap = null, rightsideview_imageview_bitmap = null, Vehicle_tax_doc_frontview_imageview_bitmap = null,
            vehicle_tax_doc_back_imageview_bitmap = null, vehicle_rc_card_front_imageview_bitmap = null, vehicle_rc_card_back_imageview_bitmap = null;

    ImageView imgView, frontsideviewnp_imageview, backsideviewnp_imageview, leftsideviewnp_imageview, leftsideview_imageview, rightsideview_imageview, rcbook_imageview, vehicle_taxpaper_imageview, vehicle_travel_permit_imageview;
    SharedPref _sharedPref;

    String owner_id = "", vehicle_id = "0", driver_id = "", o_id = "", v_id = "", d_id = "", vehicle_token = "";

    private String selectedFilePath;

    @BindView(R.id.save_vehicle_settings)
    Button save_vehicle_settings;

    @BindViews({R.id.select_vehicle_list_textview, R.id.dNameTextView, R.id.driverNumberTextView})
    List<TextView> TextViewList;

    @BindViews({R.id.driver_nameEditText, R.id.driverNumberEditText})
    List<EditText> editTextsViewList;


    List<VehicleListResponse> vehicleListResponseList;
    Intent intent;
    Dialog dialog;
    TextView yes, no;
    SMS service;
    @BindView(R.id.add_self_update)
    CheckBox add_self_update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_vehicle);
        ButterKnife.bind(this);
        _sharedPref = new SharedPref(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this);
        brandListResponseList = new ArrayList<>();
        vehicleModelList = new ArrayList<>();
        categoryList = new ArrayList<>();
        categoryListResponseList = new ArrayList<>();
        modelListResponseList = new ArrayList<>();
        vehicleListResponseList = new ArrayList<>();
        ownerResponseList = new ArrayList<>();
        driverResponseList = new ArrayList<>();

        allmodelListResponseList = new ArrayList<>();

        CloudRail.setAppKey("5b6ad8d170a77f7d86b19f80");
        //   service = new Twilio(this, "AC44405c9244688604fcd9d8373e9b3bbf", "1dd8a16c7f4eb4601372f3f673c02fa9");
        service = new Twilio(this, "AC4f371b28f463d9b3a4dd12a2e0eff380", "4642f8626a59264ed33fb5c2347d682f");

        intent = getIntent();


        v_id = intent.getStringExtra("id");
        v_id = String.valueOf(_sharedPref.getIntegerData(Global_Data.vid));
        System.out.println("Vehicle id---   " + v_id);
        brandListResponseArrayAdapter = new ArrayAdapter<BrandListResponse>
                (rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, android.R.layout.simple_spinner_item,
                        brandListResponseList); //selected item will look like a spinner set from XML
        brandListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(0).setAdapter(brandListResponseArrayAdapter);

        categoryListResponseArrayAdapter = new ArrayAdapter<CategoryListResponse>
                (rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, android.R.layout.simple_spinner_item,
                        categoryListResponseList); //selected item will look like a spinner set from XML
        categoryListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(2).setAdapter(categoryListResponseArrayAdapter);
        modelList = new ArrayList<>();

        modelListResponseArrayAdapter = new ArrayAdapter<CategoryListResponse>
                (rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, android.R.layout.simple_spinner_item,
                        modelListResponseList); //selected item will look like a spinner set from XML
        modelListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        spinnerViewList.get(1).setAdapter(modelListResponseArrayAdapter);
        vehicleListResponseArrayAdapter = new ArrayAdapter<VehicleListResponse>
                (rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, android.R.layout.simple_spinner_item,
                        vehicleListResponseList); //selected item will look like a spinner set from XML
        vehicleListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        spinnerViewList.get(3).setAdapter(vehicleListResponseArrayAdapter);

        ownerListResponseArrayAdapter = new ArrayAdapter<OwnerResponse>
                (rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, android.R.layout.simple_spinner_item,
                        ownerResponseList); //selected item will look like a spinner set from XML
        ownerListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        spinnerViewList.get(4).setAdapter(ownerListResponseArrayAdapter);

        driverListResponseArrayAdapter = new ArrayAdapter<OwnerResponse>
                (rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, android.R.layout.simple_spinner_item,
                        driverResponseList); //selected item will look like a spinner set from XML
        driverListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        spinnerViewList.get(5).setAdapter(driverListResponseArrayAdapter);

        spinnerViewList.get(2).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("Item selecting.....   ");
                if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new GetCorrectModel().executeOnExecutor(Global_Data.sExecutor);

                        //recently chaged        new GetVehicleModel().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new GetCorrectModel().execute();
                        //recently chaged     new GetVehicleModel().execute();
                    }

                } else {

                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }

                spinnerViewList.get(2).setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerViewList.get(0).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("Item selecting.....   ");
                if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new GetCategory().executeOnExecutor(Global_Data.sExecutor);
//                        new GetCorrectModel().executeOnExecutor(Global_Data.sExecutor);

                        //recently chaged new GetVehicleModel().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new GetCategory().execute();
                        //recently chaged      new GetVehicleModel().execute();
                        //          new GetCorrectModel().execute();
                    }

                } else {

                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                spinnerViewList.get(0).setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerViewList.get(3).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("Item selecting.....   ");
                vehicle_id = String.valueOf(vehicleListResponseList.get(spinnerViewList.get(3).getSelectedItemPosition()).getId());
                if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new GetVehicleDetail().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new GetVehicleDetail().execute();

                    }

                } else {

                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        dialog = new Dialog(UpdateVehicle.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.send_invitation);
        yes = (TextView) dialog.findViewById(R.id.yes);
        no = (TextView) dialog.findViewById(R.id.no);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Global_Data.checkInternetConnection(UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new SendSMSTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new SendSMSTask().execute();
                    }
                } else {

                    Toast.makeText(UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
            }
        });
        dialog.setCancelable(false);

        spinnerViewList.get(5).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position > 0)
                    dialog.show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        imageViewList.get(9).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        save_vehicle_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new UpdateVehicleInfo().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new UpdateVehicleInfo().execute();
                    }

                } else {

                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {
            if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                if (Build.VERSION.SDK_INT > 11) {

                    new GetVehicleListOfDriver().executeOnExecutor(Global_Data.sExecutor);
                } else {

                    new GetVehicleListOfDriver().execute();
                }

            } else {

                Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
            }
        } else {


            /*UUCC */
            if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                if (Build.VERSION.SDK_INT > 11) {

                    new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
                } else {

                    new GetVehicleListOfOwner().execute();
                }

            } else {

                Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
            }


        }

        if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Owner")) {
            linearLayoutsViewList.get(0).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    imageType = 0;
                    getPermissionToStorage();

                }
            });

            linearLayoutsViewList.get(1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 1;
                    getPermissionToStorage();

                }
            });

            linearLayoutsViewList.get(2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 2;
                    getPermissionToStorage();

                }
            });

            linearLayoutsViewList.get(3).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 3;
                    getPermissionToStorage();

                }
            });

            linearLayoutsViewList.get(4).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 4;
                    getPermissionToStorage();

                }
            });

            linearLayoutsViewList.get(5).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 5;
                    getPermissionToStorage();

                }
            });

            linearLayoutsViewList.get(6).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 6;
                    getPermissionToStorage();

                }
            });

            linearLayoutsViewList.get(7).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 7;
                    getPermissionToStorage();
                }
            });
//....................................................................

            btnViewList.get(0).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 0;
                  /*  Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 1;
               /*     Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 2;
                   /* Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(3).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 3;
                   /* Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(4).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 4;
                 /*   Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(5).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 5;
                 /*   Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(6).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 6;
               /*     Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);*/
                    getPermissionToStorage();
                }
            });

            btnViewList.get(7).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageType = 7;
                   /* Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
*/
                    getPermissionToStorage();
                }
            });

            owner_id = _sharedPref.getStringData(Global_Data.id);
        } else {

        }

    }

    private class GetBrandList extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AndroidNetworking.get(Global_Data._url + "brands/")
                     /*   .addPathParameter("pageNumber", "0")
                        .addQueryParameter("limit", "3")
                        .addHeaders("token", "1234")*/
                        .setTag("test")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                // do anything with response
                                jsonArray =response;
                                try {
                                    brandListResponseList.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        brandListResponseList.add(new BrandListResponse(jsonArray.getJSONObject(i).getInt("id"),
                                                jsonArray.getJSONObject(i).getString("name").trim(), jsonArray.getJSONObject(i).getString("name"), vehicleModelList));

                                    }

                                    brandListResponseArrayAdapter.notifyDataSetChanged();


                                    for (int i = 0; i < brandListResponseList.size(); i++) {
                                        //  System.out.println("check brand for spinner--   " + brandListResponseList.get(i).gecontains("Maruti Suzuki"));
                                        if (brandListResponseList.get(i).getName().trim().equals(brandName.trim())) {

                                            // pos1 = _vehicleDetailJSON.getJSONObject("brand").getInt("id");
                                            pos1 = i;
                                        }
                                    }

                                    spinnerViewList.get(0).setSelection(pos1);


                                    if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                                        if (Build.VERSION.SDK_INT > 11) {
                                            new GetCategory().executeOnExecutor(Global_Data.sExecutor);
                                        } else {
                                            new GetCategory().execute();
                                        }

                                    }
              /*  if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetVehicleListOfOwner().execute();
                    }

                } else {

                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }*/

                                    progressDialog.dismiss();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });


             /*   String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "brands/", "GET", json2);
           */     System.out.println("Brand response in vehicle---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

          /*  try {
                brandListResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    brandListResponseList.add(new BrandListResponse(jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name").trim(), jsonArray.getJSONObject(i).getString("name"), vehicleModelList));

                }

                brandListResponseArrayAdapter.notifyDataSetChanged();


                for (int i = 0; i < brandListResponseList.size(); i++) {
                    //  System.out.println("check brand for spinner--   " + brandListResponseList.get(i).gecontains("Maruti Suzuki"));
                    if (brandListResponseList.get(i).getName().trim().equals(brandName.trim())) {

                        // pos1 = _vehicleDetailJSON.getJSONObject("brand").getInt("id");
                        pos1 = i;
                    }
                }

                spinnerViewList.get(0).setSelection(pos1);


                if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new GetCategory().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new GetCategory().execute();
                    }

                }
              *//*  if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetVehicleListOfOwner().execute();
                    }

                } else {

                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }*//*

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }*/


        }
    }

    //............................................................................................

    private class GetCategory extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                AndroidNetworking.get(Global_Data._url + "categories/")
                     /*   .addPathParameter("pageNumber", "0")
                        .addQueryParameter("limit", "3")
                        .addHeaders("token", "1234")*/
                        .setTag("test")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                // do anything with response
                                jsonArray =response;




                                try {
                                    categoryListResponseList.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        for (int j = 0; j < jsonArray.getJSONObject(i).getJSONArray("brands").length(); j++) {
                                            if (brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONArray("brands").getJSONObject(j).getString("name").trim())) {
                                                categoryListResponseList.add(new CategoryListResponse(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("desc")));

                                            }
                                        }

                                    }

                                    categoryListResponseArrayAdapter.notifyDataSetChanged();
                                    progressDialog.dismiss();
                                    for (int j = 0; j < categoryListResponseList.size(); j++) {
                                        System.out.println("====>>>   " + categoryListResponseList.get(j).getName().trim());
                                        if (categoryListResponseList.get(j).getName().trim().equals(catName.trim())) {
                                            pos2 = j;
                                        }
                                    }

                                    spinnerViewList.get(2).setSelection(pos2);

                                    if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                                        if (Build.VERSION.SDK_INT > 11) {

                                            new GetCorrectModel().executeOnExecutor(Global_Data.sExecutor);
                                        } else {

                                            new GetCorrectModel().execute();
                                        }

                                    } else {

                                        Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                                    }


                                } catch (Exception e) {
                                    progressDialog.dismiss();
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });


               /* String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "categories/", "GET", json2);
         */       System.out.println("Category response in vehicle---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
/*
            try {
                categoryListResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    for (int j = 0; j < jsonArray.getJSONObject(i).getJSONArray("brands").length(); j++) {
                        if (brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONArray("brands").getJSONObject(j).getString("name").trim())) {
                            categoryListResponseList.add(new CategoryListResponse(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("desc")));

                        }
                    }

                }

                categoryListResponseArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();
                for (int j = 0; j < categoryListResponseList.size(); j++) {
                    System.out.println("====>>>   " + categoryListResponseList.get(j).getName().trim());
                    if (categoryListResponseList.get(j).getName().trim().equals(catName.trim())) {
                        pos2 = j;
                    }
                }

                spinnerViewList.get(2).setSelection(pos2);

                if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetCorrectModel().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetCorrectModel().execute();
                    }

                } else {

                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }


            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }*/


        }
    }



    //............................................................................................

    private class GetDriverList extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "drivers/", "GET", json2);
                System.out.println("Driver list response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                driverResponseList.clear();
                driverResponseList.add(new OwnerResponse(122112, "Select Driver", "", "", new User_a("", "")));
                for (int i = 0; i < jsonArray.length(); i++) {
                    // if (jsonArray.getJSONObject(i).isNull("vehicle")) {
                    driverResponseList.add(new OwnerResponse(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("firstname"), jsonArray.getJSONObject(i).getString("lastname"), jsonArray.getJSONObject(i).getString("contact_number"), new User_a(jsonArray.getJSONObject(i).getJSONObject("user").getString("username"), jsonArray.getJSONObject(i).getJSONObject("user").getString("role"))));
                    //}
                }

                driverListResponseArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();

            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }


        }
    }

    //............................................................................................

    private class GetAllVehicleModel extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "models/", "GET", json2);
                System.out.println("Model response in vehicle---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                allmodelListResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    System.out.println("Check values----   " + jsonArray.getJSONObject(i).getJSONObject("brand").getString("name") + "   ///   " + jsonArray.getJSONObject(i).getJSONObject("brand").getString("code"));

                    allmodelListResponseList.add(new GetAllModels(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("name"), new GetBrandsModel(Integer.parseInt(jsonArray.getJSONObject(i).getJSONObject("brand").getString("id")), jsonArray.getJSONObject(i).getJSONObject("brand").getString("name"), jsonArray.getJSONObject(i).getJSONObject("brand").getString("code")), new GetCategoryModel(Integer.parseInt(jsonArray.getJSONObject(i).getJSONObject("category").getString("id")), jsonArray.getJSONObject(i).getJSONObject("category").getString("name"), jsonArray.getJSONObject(i).getJSONObject("category").getString("desc"))));


                }
                System.out.println("Get All Models----   " + allmodelListResponseList.size());

                if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        //recently changed           new GetVehicleModel().executeOnExecutor(Global_Data.sExecutor);
                        new GetCorrectModel().executeOnExecutor(Global_Data.sExecutor);
                    } else {


                        //recently changed            new GetVehicleModel().execute();
                        new GetCorrectModel().execute();
                    }

                } else {

                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }


        }
    }


    //...........................

    //............................................................................................

    private class GetCorrectModel extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AndroidNetworking.get(Global_Data._url + "models/")
                        /*.addPathParameter("pageNumber", "0")
                        .addQueryParameter("limit", "3")*/
                        .addHeaders("Content-type", "application/json")
                        .setTag("test")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                // do anything with response
                                jsonArray=response;

                                try {
                                    modelListResponseList.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        if (brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONObject("brand").getString("name")) && categoryListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONObject("category").getString("name"))) {
                                            modelListResponseList.add(new CategoryListResponse(jsonArray.getJSONObject(i).getInt("id"), jsonArray.getJSONObject(i).getString("name"), ""));

                                        }
                                        System.out.println("Check Correct values----   " + jsonArray.getJSONObject(i).getJSONObject("brand").getString("name") + "   ///   " + jsonArray.getJSONObject(i).getJSONObject("brand").getString("code"));

                                        //   allmodelListResponseList.add(new GetAllModels(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("name"), new GetBrandsModel(Integer.parseInt(jsonArray.getJSONObject(i).getJSONObject("brand").getString("id")), jsonArray.getJSONObject(i).getJSONObject("brand").getString("name"), jsonArray.getJSONObject(i).getJSONObject("brand").getString("code")), new GetCategoryModel(Integer.parseInt(jsonArray.getJSONObject(i).getJSONObject("category").getString("id")), jsonArray.getJSONObject(i).getJSONObject("category").getString("name"), jsonArray.getJSONObject(i).getJSONObject("category").getString("desc"))));

                                    }
                                    System.out.println("Get All Models- in correct vehicle---   " + allmodelListResponseList.size());

                                    modelListResponseArrayAdapter.notifyDataSetChanged();

                                    for (int k = 0; k < modelListResponseList.size(); k++) {
                                        System.out.println("====>>> outside   ");
                                        if (modelListResponseList.get(k).getName().trim().equals(modelName.trim())) {
                                            System.out.println("====>>>   " + modelListResponseList.get(k).getName().trim());

                                            pos3 = k;
                                        }
                                    }

                                    spinnerViewList.get(1).setSelection(pos3);

               /* if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetVehicleListOfOwner().execute();
                    }

                } else {

                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }*/
                                    progressDialog.dismiss();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    progressDialog.dismiss();
                                }

                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });

               /* String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "models/", "GET", json2);*/
                System.out.println("Model response in vehicle---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);


          /*  try {
                modelListResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONObject("brand").getString("name")) && categoryListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONObject("category").getString("name"))) {
                        modelListResponseList.add(new CategoryListResponse(jsonArray.getJSONObject(i).getInt("id"), jsonArray.getJSONObject(i).getString("name"), ""));

                    }
                    System.out.println("Check Correct values----   " + jsonArray.getJSONObject(i).getJSONObject("brand").getString("name") + "   ///   " + jsonArray.getJSONObject(i).getJSONObject("brand").getString("code"));

                    //   allmodelListResponseList.add(new GetAllModels(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("name"), new GetBrandsModel(Integer.parseInt(jsonArray.getJSONObject(i).getJSONObject("brand").getString("id")), jsonArray.getJSONObject(i).getJSONObject("brand").getString("name"), jsonArray.getJSONObject(i).getJSONObject("brand").getString("code")), new GetCategoryModel(Integer.parseInt(jsonArray.getJSONObject(i).getJSONObject("category").getString("id")), jsonArray.getJSONObject(i).getJSONObject("category").getString("name"), jsonArray.getJSONObject(i).getJSONObject("category").getString("desc"))));

                }
                System.out.println("Get All Models- in correct vehicle---   " + allmodelListResponseList.size());

                modelListResponseArrayAdapter.notifyDataSetChanged();

                for (int k = 0; k < modelListResponseList.size(); k++) {
                    System.out.println("====>>> outside   ");
                    if (modelListResponseList.get(k).getName().trim().equals(modelName.trim())) {
                        System.out.println("====>>>   " + modelListResponseList.get(k).getName().trim());

                        pos3 = k;
                    }
                }

                spinnerViewList.get(1).setSelection(pos3);

               *//* if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetVehicleListOfOwner().execute();
                    }

                } else {

                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }*//*
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }
*/

        }
    }


    private class GetVehicleModel extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                modelListResponseList.clear();
                for (int i = 0; i < allmodelListResponseList.size(); i++) {
                    if (brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).toString().trim().equals(allmodelListResponseList.get(i).getBrand().getName().trim()) && categoryListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).toString().trim().equals(allmodelListResponseList.get(i).getCategory().getName().trim())) {
                        modelListResponseList.add(new CategoryListResponse(allmodelListResponseList.get(i).getId(), allmodelListResponseList.get(i).getName(), ""));

                    }

                }

                modelListResponseArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }

            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
              /*  spinnerViewList.get(0).setSelection(pos1);
                spinnerViewList.get(2).setSelection(pos2);

                spinnerViewList.get(1).setSelection(pos3);
*/
                //   spinnerViewList.get(1).setSelection(pos3);
                if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetVehicleListOfOwner().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetVehicleListOfOwner().execute();
                    }

                } else {

                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                System.out.println(",,,,,,,,   ---  " + modelListResponseList.size());
                //  modelListResponseArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }


        }
    }


    //..............................................................................................

    private class UpdateVehicleInfo extends AsyncTask<String, String, String> {
        String message = "", id = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONArray _fetchUserProfileJsonArray;
        JSONObject _updateUserProfileOutputJSON, _updateUserProfileInput;
        ProgressDialog progressDialog = new ProgressDialog(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {


                id = _sharedPref.getStringData(Global_Data.id);
                _updateUserProfileInput = new JSONObject();
                _updateUserProfileInput.put("id", v_id);

                _updateUserProfileInput.put("brand", brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).getId());
                _updateUserProfileInput.put("category", categoryListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).getId());
                try {
                    _updateUserProfileInput.put("model", modelListResponseList.get(spinnerViewList.get(1).getSelectedItemPosition()).getId());
                } catch (Exception e) {
                    e.printStackTrace();
                    _updateUserProfileInput.put("model", JSONObject.NULL);

                }
                _updateUserProfileInput.put("vehicle_number", carNumberEditText.getText().toString());
                _updateUserProfileInput.put("owner", _sharedPref.getStringData(Global_Data.id));

                if (add_self_update.isChecked()) {
                    _updateUserProfileInput.put("addSelf", true);
                } else {
                    _updateUserProfileInput.put("addSelf", false);
                }



                if (frontsideviewnp_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_front_image", encodeToBase64(frontsideviewnp_imageview_bitmap));
                }
                if (backsideviewnp_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_back_image", encodeToBase64(backsideviewnp_imageview_bitmap));
                }
                if (leftsideview_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_left_image", encodeToBase64(leftsideview_imageview_bitmap));
                }
                if (rightsideview_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_right_image", encodeToBase64(rightsideview_imageview_bitmap));
                }
                if (Vehicle_tax_doc_frontview_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_tax_doc_front_image", encodeToBase64(Vehicle_tax_doc_frontview_imageview_bitmap));
                }
                if (vehicle_tax_doc_back_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_tax_doc_back_image", encodeToBase64(vehicle_tax_doc_back_imageview_bitmap));
                }
                if (vehicle_rc_card_front_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_rc_card_front_image", encodeToBase64(vehicle_rc_card_front_imageview_bitmap));
                }
                if (vehicle_rc_card_back_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_rc_card_back_image", encodeToBase64(vehicle_rc_card_back_imageview_bitmap));
                }
                String json2 = _updateUserProfileInput.toString();
                System.out.println("Driver id---   " + _sharedPref.getStringData(Global_Data.driver_id));

                JSONParser jsonParser = new JSONParser();
                System.out.println("_updateVehicleInfoInput  ...   " + json2 + "   ????    " + v_id);
                _updateUserProfileOutputJSON = jsonParser.makeHttpRequest(Global_Data._url + "vehicles/" + v_id + "/", "PATCH", json2);
                System.out.println("_updateVehicleInfoOutput----   " + _updateUserProfileOutputJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_updateUserProfileOutputJSON.has("driver") && _updateUserProfileOutputJSON.getString("driver").equals("[\"This field must be unique.\"]")) {
                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Driver must be unique.", Toast.LENGTH_SHORT).show();

                } else if (_updateUserProfileOutputJSON.has("model") && _updateUserProfileOutputJSON.getString("model").equals("[\"This field may not be null.\"]")) {
                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please select vehicle model.", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Vehicle updated successfully", Toast.LENGTH_SHORT).show();
                /*    if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                        if (Build.VERSION.SDK_INT > 11) {

                            new GetSelfDriverIdOfOwner().executeOnExecutor(Global_Data.sExecutor);
                        } else {

                            new GetSelfDriverIdOfOwner().execute();
                        }

                    } else {

                        Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                    }*/
                    finish();
                }

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    public static String encodeToBase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    /**
     * Run time permission for above marshmallow
     * if user deny the permission when install the application then system ask for permission
     * camera & storage
     */
    //  private int PICK_IMAGE_REQUEST = 2;
    public final static int PERMISSIONS_REQUEST = 1;

    @TargetApi(Build.VERSION_CODES.M)
    public void getPermissionToStorage() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            }

            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA},
                    PERMISSIONS_REQUEST);

        } else {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);

        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[2])) {
            } else if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, permissions[1]) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, permissions[2]) == PackageManager.PERMISSION_GRANTED) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
            } else {
                Intent intent1 = new Intent();
                intent1.setType("image/*");
                intent1.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent1, "Select Image"), PICK_IMAGE_REQUEST);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {


            filePath = data.getData();
            String selectedImagePath = BitmapUtils.getFilePathFromUri(this, data.getData());
            Log.i(TAG, " File path : " + filePath);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(UpdateVehicle.this.getContentResolver(), filePath);
                System.out.println("After Compress Dimension     " + bitmap.getWidth() + "-" + bitmap.getHeight() + "    file - " + filePath + "    select--   " + selectedImagePath);

                Bitmap compressedBitmap = ImageUtils.getInstant().getCompressedBitmap(selectedImagePath);

                System.out.println("After Compress Dimension     " + compressedBitmap.getWidth() + "-" + compressedBitmap.getHeight() + "    file - " + filePath + "    select--   " + selectedImagePath);
                persistImage(compressedBitmap, "imagefile");

                if (imageType == 0) {
                    frontsideviewnp_imageview_bitmap = compressedBitmap;
                    imageViewList.get(0).setImageBitmap(compressedBitmap);
                } else if (imageType == 1) {
                    backsideviewnp_imageview_bitmap = compressedBitmap;
                    imageViewList.get(1).setImageBitmap(compressedBitmap);
                } else if (imageType == 2) {
                    leftsideview_imageview_bitmap = compressedBitmap;
                    imageViewList.get(2).setImageBitmap(compressedBitmap);
                } else if (imageType == 3) {
                    rightsideview_imageview_bitmap = compressedBitmap;
                    imageViewList.get(3).setImageBitmap(compressedBitmap);

                } else if (imageType == 4) {
                    Vehicle_tax_doc_frontview_imageview_bitmap = compressedBitmap;
                    imageViewList.get(4).setImageBitmap(compressedBitmap);
                } else if (imageType == 5) {
                    vehicle_tax_doc_back_imageview_bitmap = compressedBitmap;
                    imageViewList.get(5).setImageBitmap(compressedBitmap);
                } else if (imageType == 6) {
                    vehicle_rc_card_front_imageview_bitmap = compressedBitmap;
                    imageViewList.get(6).setImageBitmap(compressedBitmap);
                } else if (imageType == 7) {
                    vehicle_rc_card_back_imageview_bitmap = compressedBitmap;
                    imageViewList.get(7).setImageBitmap(compressedBitmap);
                }
                // imgView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private String persistImage(Bitmap bitmap, String name) {
        File filesDir = getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");
        String file_String = "";
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            System.out.println("OS---   " + imageFile.getName());
            file_String = imageFile.toString();
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return file_String;
    }


    //............................................................................................

    private class GetVehicleListOfOwner extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfOwnerJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                AndroidNetworking.get(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id))
                        /*    .addBodyParameter("firstname", "Amit")
                            .addBodyParameter("lastname", "Shekhar")
                            .setTag("test")*/
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // do anything with response
                                _vehicleListOfOwnerJSON =response;


                                try {
                                    if (_vehicleListOfOwnerJSON != null) {
                                        if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                                            jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                                            System.out.println("Check vehicle json array---   " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").length());

                                            for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {
                                                vehicleListResponseList.add(new VehicleListResponse(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number")));
                                            }
                                            vehicle_id = String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(0).getInt("id"));
                                            driver_id = vehicle_id;
                                            vehicleListResponseArrayAdapter.notifyDataSetChanged();

                                            if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                                                if (Build.VERSION.SDK_INT > 11) {

                                                    new GetVehicleDetail().executeOnExecutor(Global_Data.sExecutor);
                                                } else {

                                                    new GetVehicleDetail().execute();
                                                }

                                            } else {

                                                Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                                            }


                                        }
                                    } else {
                                        Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                                    }
                                    progressDialog.dismiss();

                                } catch (Exception e) {
                                    e.printStackTrace();

                                    progressDialog.dismiss();

                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });


             /*   String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);
             */   System.out.println("Owner's vehicle list------   " + _vehicleListOfOwnerJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

           /* try {
                if (_vehicleListOfOwnerJSON != null) {
                    if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                        jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                        System.out.println("Check vehicle json array---   " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").length());

                        for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {
                            vehicleListResponseList.add(new VehicleListResponse(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number")));
                        }
                        vehicle_id = String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(0).getInt("id"));
                        driver_id = vehicle_id;
                        vehicleListResponseArrayAdapter.notifyDataSetChanged();

                        if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                            if (Build.VERSION.SDK_INT > 11) {

                                new GetVehicleDetail().executeOnExecutor(Global_Data.sExecutor);
                            } else {

                                new GetVehicleDetail().execute();
                            }

                        } else {

                            Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                        }


                    }
                } else {
                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();

                progressDialog.dismiss();

            }*/


        }
    }

    //............................................................................................

    private class GetVehicleListOfDriver extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfDriverJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfDriverJSON = jsonParser.makeHttpRequest(Global_Data._url + "drivers/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);
                System.out.println("Driver's vehicle list------   " + _vehicleListOfDriverJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (_vehicleListOfDriverJSON.getString("vehicle") != null) {
                    vehicle_id = String.valueOf(_vehicleListOfDriverJSON.getJSONObject("vehicle").getInt("id"));
                    driver_id = vehicle_id;

                    if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                        if (Build.VERSION.SDK_INT > 11) {

                            new GetVehicleDetail().executeOnExecutor(Global_Data.sExecutor);
                        } else {

                            new GetVehicleDetail().execute();
                        }
                    }
                } else {

                    Toast.makeText(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }


        }
    }

    //............................................................................................
    int pos1 = -1, pos2 = -1, pos3 = -1;
    String brandName = "", catName = "", modelName = "";

    private class GetVehicleDetail extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleDetailJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                AndroidNetworking.get(Global_Data._url + "vehicles/" + v_id)
                        /*    .addBodyParameter("firstname", "Amit")
                            .addBodyParameter("lastname", "Shekhar")
                            .setTag("test")*/
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // do anything with response
                                _vehicleDetailJSON =response;
                                System.out.println("Vehicle details------   " + _vehicleDetailJSON);
                                try {
                                    brandName = _vehicleDetailJSON.getJSONObject("brand").getString("name").trim();
                                    catName = _vehicleDetailJSON.getJSONObject("category").getString("name").trim();
                                    modelName = _vehicleDetailJSON.getJSONObject("model").getString("name").trim();

                                    if (_vehicleDetailJSON.has("driver")) {

                                        JSONObject dataObject = _vehicleDetailJSON.optJSONObject("driver");

                                        if (dataObject != null) {
                                            TextViewList.get(1).setVisibility(View.VISIBLE);

                                            TextViewList.get(2).setVisibility(View.VISIBLE);

                                            editTextsViewList.get(0).setVisibility(View.VISIBLE);
                                            editTextsViewList.get(1).setVisibility(View.VISIBLE);
                                            //Do things with object.
                                            editTextsViewList.get(0).setText(_vehicleDetailJSON.getJSONObject("driver").getString("firstname") + " " + _vehicleDetailJSON.getJSONObject("driver").getString("lastname"));
                                            editTextsViewList.get(1).setText(_vehicleDetailJSON.getJSONObject("driver").getString("contact_number"));
                                            editTextsViewList.get(0).setEnabled(false);
                                            editTextsViewList.get(1).setEnabled(false);
if(!_sharedPref.getStringData(Global_Data.owner_driver_id).equalsIgnoreCase("")){
    if(_sharedPref.getStringData(Global_Data.owner_driver_id).equals(String.valueOf(_vehicleDetailJSON.getJSONObject("driver").getInt("id")))){
        add_self_update.setChecked(true);
    }else {
        add_self_update.setChecked(false);
    }
}
                                            // if(_sharedPref.getStringData(Global_Data.o)){}
                                        } else {


                                            //Do things with array
                                            TextViewList.get(1).setVisibility(View.GONE);

                                            TextViewList.get(2).setVisibility(View.GONE);

                                            editTextsViewList.get(0).setVisibility(View.GONE);
                                            editTextsViewList.get(1).setVisibility(View.GONE);
                                        }
                                    } else {
                                        // Do nothing or throw exception if "data" is a mandatory field
                                    }

              /*  if (_vehicleDetailJSON.getJSONObject("driver") == JSONObject.NULL || _vehicleDetailJSON.getJSONObject("driver") == null) {
                    TextViewList.get(1).setVisibility(View.GONE);

                    TextViewList.get(2).setVisibility(View.GONE);

                    editTextsViewList.get(0).setVisibility(View.GONE);
                    editTextsViewList.get(1).setVisibility(View.GONE);
                } else {
                    editTextsViewList.get(0).setText(_vehicleDetailJSON.getJSONObject("driver").getString("firstname") + " " + _vehicleDetailJSON.getJSONObject("driver").getString("lastname"));
                    editTextsViewList.get(1).setText(_vehicleDetailJSON.getJSONObject("driver").getString("contact_number"));
                    editTextsViewList.get(0).setEnabled(false);
                    editTextsViewList.get(1).setEnabled(false);
                }*/

            /*    for (int i = 0; i < brandListResponseList.size(); i++) {
                    //  System.out.println("check brand for spinner--   " + brandListResponseList.get(i).gecontains("Maruti Suzuki"));
                    if (brandListResponseList.get(i).getName().trim().equals(_vehicleDetailJSON.getJSONObject("brand").getString("name").trim())) {

                        // pos1 = _vehicleDetailJSON.getJSONObject("brand").getInt("id");
                        pos1 = i;
                    }
                }
                System.out.println("positions----     " + pos1);
                for (int j = 0; j < categoryListResponseList.size(); j++) {
                    System.out.println("====>>>   " + categoryListResponseList.get(j).getName().trim());
                    if (categoryListResponseList.get(j).getName().trim().equals(_vehicleDetailJSON.getJSONObject("category").getString("name").trim())) {
                        pos2 = j;
                    }
                }
                System.out.println("positions----     " + pos2);

                for (int k = 0; k < modelListResponseList.size(); k++) {
                    System.out.println("====>>> outside   ");
                    if (modelListResponseList.get(k).getName().trim().equals(_vehicleDetailJSON.getJSONObject("model").getString("name").trim())) {
                        System.out.println("====>>>   " + modelListResponseList.get(k).getName().trim());

                        pos3 = k;
                    }
                }
                System.out.println("positions----     " + pos3);
*/
                                    if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                                        if (Build.VERSION.SDK_INT > 11) {
                                            new GetBrandList().executeOnExecutor(Global_Data.sExecutor);
             /*   new GetCategory().executeOnExecutor(Global_Data.sExecutor);
                new GetAllVehicleModel().executeOnExecutor(Global_Data.sExecutor);
           */
                                        } else {
                                            new GetBrandList().execute();
             /*   new GetCategory().execute();
                new GetVehicleListOfOwner1().execute();
             */
                                        }

                                    }

                /*final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            spinnerViewList.get(0).setSelection(pos1);
                            spinnerViewList.get(2).setSelection(pos2);
                            spinnerViewList.get(1).setSelection(pos3);


                            System.out.println("Update vehicle");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 4000);*/
              /*  if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new GetCategory().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new GetCategory().execute();
                    }

                }*/

                                    carNumberEditText.setText(_vehicleDetailJSON.getString("vehicle_number"));

                                    vehicle_token = _vehicleDetailJSON.getJSONObject("vdtoken").getString("vehicle_driver_token");
                                    if (_vehicleDetailJSON.has("vehicle_front_image")) {
                                        Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_front_image"))
                                                .thumbnail(0.5f)
                                                .crossFade()
                                                //.placeholder(R.drawable.frontsideviewnp)
                                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                                .into(imageViewList.get(0));
                                    }

                                    if (_vehicleDetailJSON.has("vehicle_back_image")) {
                                        Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_back_image"))
                                                .thumbnail(0.5f)
                                                .crossFade()
                                                //  .placeholder(R.drawable.backsideviewnp)
                                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                                .into(imageViewList.get(1));
                                    }
                                    if (_vehicleDetailJSON.has("vehicle_left_image")) {
                                        Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_left_image"))
                                                .thumbnail(0.5f)
                                                .crossFade()
                                                //.placeholder(R.drawable.rightsideviewwithnp)

                                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                                .into(imageViewList.get(2));
                                    }
                                    if (_vehicleDetailJSON.has("vehicle_right_image")) {
                                        Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_right_image"))
                                                .thumbnail(0.5f)
                                                .crossFade()
                                                // .placeholder(R.drawable.rightsideview)

                                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                                .into(imageViewList.get(3));
                                    }
                                    if (_vehicleDetailJSON.has("vehicle_tax_doc_front_image")) {
                                        Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_tax_doc_front_image"))
                                                .thumbnail(0.5f)
                                                .crossFade()
                                                // .placeholder(R.drawable.rc_book_front)

                                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                                .into(imageViewList.get(4));
                                    }
                                    if (_vehicleDetailJSON.has("vehicle_tax_doc_back_image")) {
                                        Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_tax_doc_back_image"))
                                                .thumbnail(0.5f)
                                                .crossFade()
                                                //.placeholder(R.drawable.rc_book_front)

                                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                                .into(imageViewList.get(5));
                                    }
                                    if (_vehicleDetailJSON.has("vehicle_rc_card_front_image")) {
                                        Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_rc_card_front_image"))
                                                .thumbnail(0.5f)
                                                .crossFade()
                                                //  .placeholder(R.drawable.vehicle_tax_paper)

                                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                                .into(imageViewList.get(6));
                                    }
                                    if (_vehicleDetailJSON.has("vehicle_rc_card_back_image")) {
                                        Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_rc_card_back_image"))
                                                .thumbnail(0.5f)
                                                .crossFade()
                                                //  .placeholder(R.drawable.vehicle_travel_permit)

                                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                                .into(imageViewList.get(7));
                                    }

                                    progressDialog.dismiss();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });




          /*      String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleDetailJSON = jsonParser.makeHttpRequest(Global_Data._url + "vehicles/" + v_id, "GET", json2);
          */        } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

           /* try {
                brandName = _vehicleDetailJSON.getJSONObject("brand").getString("name").trim();
                catName = _vehicleDetailJSON.getJSONObject("category").getString("name").trim();
                modelName = _vehicleDetailJSON.getJSONObject("model").getString("name").trim();

                if (_vehicleDetailJSON.has("driver")) {

                    JSONObject dataObject = _vehicleDetailJSON.optJSONObject("driver");

                    if (dataObject != null) {
                        TextViewList.get(1).setVisibility(View.VISIBLE);

                        TextViewList.get(2).setVisibility(View.VISIBLE);

                        editTextsViewList.get(0).setVisibility(View.VISIBLE);
                        editTextsViewList.get(1).setVisibility(View.VISIBLE);
                        //Do things with object.
                        editTextsViewList.get(0).setText(_vehicleDetailJSON.getJSONObject("driver").getString("firstname") + " " + _vehicleDetailJSON.getJSONObject("driver").getString("lastname"));
                        editTextsViewList.get(1).setText(_vehicleDetailJSON.getJSONObject("driver").getString("contact_number"));
                        editTextsViewList.get(0).setEnabled(false);
                        editTextsViewList.get(1).setEnabled(false);

                        // if(_sharedPref.getStringData(Global_Data.o)){}
                    } else {


                        //Do things with array
                        TextViewList.get(1).setVisibility(View.GONE);

                        TextViewList.get(2).setVisibility(View.GONE);

                        editTextsViewList.get(0).setVisibility(View.GONE);
                        editTextsViewList.get(1).setVisibility(View.GONE);
                    }
                } else {
                    // Do nothing or throw exception if "data" is a mandatory field
                }

              *//*  if (_vehicleDetailJSON.getJSONObject("driver") == JSONObject.NULL || _vehicleDetailJSON.getJSONObject("driver") == null) {
                    TextViewList.get(1).setVisibility(View.GONE);

                    TextViewList.get(2).setVisibility(View.GONE);

                    editTextsViewList.get(0).setVisibility(View.GONE);
                    editTextsViewList.get(1).setVisibility(View.GONE);
                } else {
                    editTextsViewList.get(0).setText(_vehicleDetailJSON.getJSONObject("driver").getString("firstname") + " " + _vehicleDetailJSON.getJSONObject("driver").getString("lastname"));
                    editTextsViewList.get(1).setText(_vehicleDetailJSON.getJSONObject("driver").getString("contact_number"));
                    editTextsViewList.get(0).setEnabled(false);
                    editTextsViewList.get(1).setEnabled(false);
                }*//*

            *//*    for (int i = 0; i < brandListResponseList.size(); i++) {
                    //  System.out.println("check brand for spinner--   " + brandListResponseList.get(i).gecontains("Maruti Suzuki"));
                    if (brandListResponseList.get(i).getName().trim().equals(_vehicleDetailJSON.getJSONObject("brand").getString("name").trim())) {

                        // pos1 = _vehicleDetailJSON.getJSONObject("brand").getInt("id");
                        pos1 = i;
                    }
                }
                System.out.println("positions----     " + pos1);
                for (int j = 0; j < categoryListResponseList.size(); j++) {
                    System.out.println("====>>>   " + categoryListResponseList.get(j).getName().trim());
                    if (categoryListResponseList.get(j).getName().trim().equals(_vehicleDetailJSON.getJSONObject("category").getString("name").trim())) {
                        pos2 = j;
                    }
                }
                System.out.println("positions----     " + pos2);

                for (int k = 0; k < modelListResponseList.size(); k++) {
                    System.out.println("====>>> outside   ");
                    if (modelListResponseList.get(k).getName().trim().equals(_vehicleDetailJSON.getJSONObject("model").getString("name").trim())) {
                        System.out.println("====>>>   " + modelListResponseList.get(k).getName().trim());

                        pos3 = k;
                    }
                }
                System.out.println("positions----     " + pos3);
*//*
                if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new GetBrandList().executeOnExecutor(Global_Data.sExecutor);
             *//*   new GetCategory().executeOnExecutor(Global_Data.sExecutor);
                new GetAllVehicleModel().executeOnExecutor(Global_Data.sExecutor);
           *//*
                    } else {
                        new GetBrandList().execute();
             *//*   new GetCategory().execute();
                new GetVehicleListOfOwner1().execute();
             *//*
                    }

                }

                *//*final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            spinnerViewList.get(0).setSelection(pos1);
                            spinnerViewList.get(2).setSelection(pos2);
                            spinnerViewList.get(1).setSelection(pos3);


                            System.out.println("Update vehicle");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 4000);*//*
              *//*  if (Global_Data.checkInternetConnection(rodearr.app.com.rodearrapp.activity.UpdateVehicle.this)) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new GetCategory().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new GetCategory().execute();
                    }

                }*//*

                carNumberEditText.setText(_vehicleDetailJSON.getString("vehicle_number"));

                vehicle_token = _vehicleDetailJSON.getJSONObject("vdtoken").getString("vehicle_driver_token");
                if (_vehicleDetailJSON.has("vehicle_front_image")) {
                    Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_front_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            //.placeholder(R.drawable.frontsideviewnp)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(0));
                }

                if (_vehicleDetailJSON.has("vehicle_back_image")) {
                    Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_back_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            //  .placeholder(R.drawable.backsideviewnp)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(1));
                }
                if (_vehicleDetailJSON.has("vehicle_left_image")) {
                    Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_left_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            //.placeholder(R.drawable.rightsideviewwithnp)

                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(2));
                }
                if (_vehicleDetailJSON.has("vehicle_right_image")) {
                    Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_right_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            // .placeholder(R.drawable.rightsideview)

                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(3));
                }
                if (_vehicleDetailJSON.has("vehicle_tax_doc_front_image")) {
                    Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_tax_doc_front_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            // .placeholder(R.drawable.rc_book_front)

                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(4));
                }
                if (_vehicleDetailJSON.has("vehicle_tax_doc_back_image")) {
                    Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_tax_doc_back_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            //.placeholder(R.drawable.rc_book_front)

                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(5));
                }
                if (_vehicleDetailJSON.has("vehicle_rc_card_front_image")) {
                    Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_rc_card_front_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            //  .placeholder(R.drawable.vehicle_tax_paper)

                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(6));
                }
                if (_vehicleDetailJSON.has("vehicle_rc_card_back_image")) {
                    Glide.with(UpdateVehicle.this).load(_vehicleDetailJSON.getString("vehicle_rc_card_back_image"))
                            .thumbnail(0.5f)
                            .crossFade()
                            //  .placeholder(R.drawable.vehicle_travel_permit)

                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imageViewList.get(7));
                }

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }*/


        }
    }
//..............


    //........................................

    private class SendSMSTask extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONObject _sendOTPJSON;
        ProgressDialog progressDialog = new ProgressDialog(UpdateVehicle.this);
        String mobileNumberOTP = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                System.out.println("------ inside another try catch");
                service.sendSMS(
                        "+15005550006",
                        "+919174737139",
                        "Download the app from https://play.google.com/store/apps/details?id=com.facebook.katana Please enter " + _sharedPref.getStringData(Global_Data.vdToken) + " when associate with new vehicle."
                );
             /*   service.sendSMS(
                        "Patrick",
                        "+91 9174737139",
                        "Hey, how are you?"
                );*/

            } catch (Exception e) {
                e.printStackTrace();
            }
            try {

                mobileNumberOTP = _sharedPref.getStringData(Global_Data.vdToken);
                String json2 = driverResponseList.get(spinnerViewList.get(5).getSelectedItemPosition()).getContact_number().toString() + "/" + mobileNumberOTP;
                System.out.println("input from update vehicle--  " + json2);
                JSONParser jsonParser = new JSONParser();
                _sendOTPJSON = jsonParser.makeHttpRequest("https://2factor.in/API/V1/4a668095-46be-11e8-a895-0200cd936042/SMS/" + json2, "POST", "");
                System.out.println(_sendOTPJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jo1 = null;
                if (_sendOTPJSON.has("Status")) {
                    //Toast.makeText(OnboardingOTPVerification.this, "SMS sent--  " + _sendOTPJSON.getString("Status"), Toast.LENGTH_SHORT).show();

                    //  _sharedPref.setStringData(Global_Data.mobile_number_otp, mobileNumberOTP);


                }
                dialog.hide();
                progressDialog.dismiss();

            } catch (Exception e) {
                dialog.hide();
                progressDialog.dismiss();
                e.printStackTrace();
            }
        }


    }

    //..........................................


    //............................................................................................

    private class GetVehicleListOfOwner1 extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfOwnerJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(UpdateVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);

                //Uncomment it when code is running properly.
                // _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + , "GET", json2);

                System.out.println("Owner's 1111 vehicle list in listof vehicles------   " + _vehicleListOfOwnerJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                // vehicleListResponseList.clear();

                // JSONArray array = json.optJSONArray("vehicles");
                if (_vehicleListOfOwnerJSON != null) {
                    // for(int i=0;i<_vehicleListOfOwnerJSON.getJSONArray("vehicles").length();i++) {
                    if (_vehicleListOfOwnerJSON.getJSONArray("vehicles") != null) {
                        System.out.println("Inside not null");
                        jsonArray = _vehicleListOfOwnerJSON.getJSONArray("vehicles");
                        _sharedPref.setStringData(Global_Data.vehicleId, "");
                        _sharedPref.setStringData(Global_Data.vehicleId, "");

                        // vehicleList= (List<Vehicle>) jsonArray;
                        // }
                        //   if (jsonArray.length() != 0 || jsonArray != null) {
                        System.out.println("Check vehicle json array---   " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").length());


                        for (int i = 0; i < _vehicleListOfOwnerJSON.getJSONArray("vehicles").length(); i++) {


                            try {
                                System.out.println("value of i---->..   " + i);

                                if (_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).isNull("driver")) {

                                    System.out.println("Vehicle token---   " + _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("vdtoken").getString("vehicle_driver_token"));
                                    _sharedPref.setStringData(Global_Data.vdToken, _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("vdtoken").getString("vehicle_driver_token"));
                                    _sharedPref.setStringData(Global_Data.vehicleId, _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("id"));
                                    break;
                                }
                                //         vehicleListResponseList.add(new VehiclesDriverManagement(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("firstname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("lastname"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("gender"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("contact_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dob"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("license_number"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getString("dl_img_front"), new Stats(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("driver"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("daily"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("weekly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("monthly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("yearly"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getInt("total"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("longitude"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("lattitude")),new Vdtoken(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("lattitude"),_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("lattitude"),_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getJSONObject("driver").getJSONObject("stats").getString("lattitude")))));

                            } catch (Exception e) {
                                System.out.println("value of i---->..   " + i);

                                //       vehicleListResponseList.add(new VehiclesDriverManagement(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getInt("id"), _vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(i).getString("vehicle_number"), new Driver(111, "", "", "", "", "", "", "", new Stats(0, 0, 0, 0, 0, 0, "", ""))));

                                e.printStackTrace();
                            }

                        }
                          /*  vehicle_id = String.valueOf(_vehicleListOfOwnerJSON.getJSONArray("vehicles").getJSONObject(0).getInt("id"));
                            driver_id = vehicle_id;
*/
                    } else {
                        System.out.println("Inside null");
                    }
                } else {

                    Toast.makeText(UpdateVehicle.this, _vehicleListOfOwnerJSON.toString(), Toast.LENGTH_SHORT).show();

                }
                //Do things with array


                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();

                Toast.makeText(UpdateVehicle.this, "Vehicles are not associated for this owner.", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }


        }
    }




    //----------------------



    private class GetSelfDriverIdOfOwner extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _vehicleListOfOwnerJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(UpdateVehicle.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            //  progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);

                //Uncomment it when code is running properly.       _vehicleListOfOwnerJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + 2, "GET", json2);

                System.out.println("GetSelfDriverIdOfOwner------   " + _vehicleListOfOwnerJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (_vehicleListOfOwnerJSON != null) {
                if(_vehicleListOfOwnerJSON.has("selfdriver")){
                  JSONObject selfDriverIdObj= _vehicleListOfOwnerJSON.optJSONObject("selfdriver");
                    System.out.println("selfDriverIdObj    "+selfDriverIdObj);
                    if (selfDriverIdObj!= null ) {

                        /*if (selfDriverIdObj!= null || !selfDriverIdObj.equals(null)) {
                    */    _sharedPref.setStringData(Global_Data.owner_driver_id,_vehicleListOfOwnerJSON.getString("selfdriver"));

                    } else{

                        _sharedPref.setStringData(Global_Data.owner_driver_id,"");

                    }
                }
                } else {


                }


            } catch (Exception e) {
                e.printStackTrace();


            }


        }
    }
}
